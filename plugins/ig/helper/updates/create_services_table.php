<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_services', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->text('description');
            $table->boolean('status')->default(true);

            $table->timestamps();
            
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_services');
    }
}
