<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAnnouncementsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_announcements', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->text('content');
            
            $table->boolean('status')->default(false);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->string('bg_color')->nullable();
            $table->string('text_color')->nullable();
            $table->integer('font_size')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_announcements');
    }
}
