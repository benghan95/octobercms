<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGalleriesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_galleries', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug')->unique();
            $table->boolean('status')->default(true);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('backend_users');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_galleries');
    }
}
