<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOutletsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_outlets', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->text('address');
            $table->text('location_link');
            $table->integer('sort_order')->nullable()->default(0);
            $table->boolean('status')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_outlets');
    }
}
