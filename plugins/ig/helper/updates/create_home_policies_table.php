<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomePoliciesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_home_policies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('icon')->nullable();
            $table->text('description');
            $table->boolean('status');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_home_policies');
    }
}
