<?php namespace IG\Helper\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactFormsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_helper_contact_forms', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('email');
            $table->string('company')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('subject');
            $table->text('message');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_helper_contact_forms');
    }
}
