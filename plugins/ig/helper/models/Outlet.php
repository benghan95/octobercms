<?php namespace IG\Helper\Models;

use Model;

/**
 * Outlet Model
 */
class Outlet extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_helper_outlets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function active(){
        return static::where('status', true);
    }
}
