<?php namespace IG\Helper\Models;

use Model;
use Carbon\Carbon;

/**
 * Announcement Model
 */
class Announcement extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_helper_announcements';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave() {
        if ($this->start_date)
            $this->start_date = \Carbon\Carbon::parse($this->start_date)->setTime(0, 0, 0);

        if($this->start_date == null)
            $this->start_date = Carbon::now('Asia/Kuala_Lumpur');

        if ($this->end_date)
            $this->end_date = \Carbon\Carbon::parse($this->end_date)->setTime(0, 0, 0);
    }

    public static function displayable(){
        $dt = Carbon::now('Asia/Kuala_Lumpur');
        $dt->addDay(1);

        return static::where('status', true)
                     ->where('start_date', '<=', $dt->toDateString())
                     ->where(function($query) use ($dt){
                        $query->where('end_date', NULL)
                              ->orWhere('end_date', '>=', $dt->toDateString());
                     })
                     ->orderBy('updated_at', 'DESC');
    }
}
