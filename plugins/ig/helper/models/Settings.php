<?php namespace IG\Helper\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'ig_helper_company_info_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
