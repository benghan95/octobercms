<?php namespace IG\Helper\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Outlet Back-end Controller
 */
class Outlet extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.Helper', 'helper', 'outlet');
    }
}
