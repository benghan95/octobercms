<?php namespace IG\Helper;

use Backend;
use System\Classes\PluginBase;

/**
 * Helper Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Helper',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-globe'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            // 'IG\Helper\Components\Gallery'             => 'gallery',
            'IG\Helper\Components\OurPartners'         => 'ourPartners',
            'IG\Helper\Components\OurServices'         => 'ourServices',
            'IG\Helper\Components\OurTestimonials'     => 'ourTestimonials',
            'IG\Helper\Components\HomePolicies'        => 'homePolicies',
            'IG\Helper\Components\AnnouncementMarquee' => 'announcementMarquee',
            'IG\Helper\Components\CompanyInfo'         => 'companyInfo',
            'IG\Helper\Components\BranchList'          => 'branchList',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'IG Premium Main Settings',
                'description' => "Settings on the IG Premium website can be done here.",
                'category'    => 'IG Helper',
                'icon'        => 'icon-cog',
                'class'       => 'IG\Helper\Models\Settings',
                'order'       => -100,
                'permissions' => ['ig.helper.manage_settings']
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'ig.helper.manage_settings' => [
                'tab' => 'Helper',
                'label' => 'Manage Settings'
            ],
            'ig.helper.manage_home_policy' => [
                'tab' => 'Helper',
                'label' => 'Manage Home Policy'
            ],
            'ig.helper.manage_outlets' => [
                'tab' => 'Helper',
                'label' => 'Manage Outlets'
            ],
            'ig.helper.manage_galleries' => [
                'tab' => 'Helper',
                'label' => 'Manage Galleries'
            ],
            'ig.helper.manage_contactforms' => [
                'tab' => 'Helper',
                'label' => 'Manage Contact Forms'
            ],
            'ig.helper.manage_partners' => [
                'tab' => 'Helper',
                'label' => 'Manage Partners'
            ],
            'ig.helper.manage_services' => [
                'tab' => 'Helper',
                'label' => 'Manage Services'
            ],
            'ig.helper.manage_testimonials' => [
                'tab' => 'Helper',
                'label' => 'Manage Testimonials'
            ],
            'ig.helper.manage_announcements' => [
                'tab' => 'Helper',
                'label' => 'Manage Announcements'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'helper' => [
                'label'       => 'Helper',
                'url'         => Backend::url('ig/helper/announcement'),
                'icon'        => 'icon-globe',
                'permissions' => ['ig.helper.*'],
                'order'       => 150,
                'sideMenu'    => [
                    'announcement' => [
                        'label'       => 'Announcements',
                        'url'         => Backend::url('ig/helper/announcement'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_annoucements'],
                        'order'       => 100,
                    ],
                    'outlet' => [
                        'label'       => 'Outlets',
                        'url'         => Backend::url('ig/helper/outlet'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_outlets'],
                        'order'       => 100,
                    ],
                    'homepolicy' => [
                        'label'       => 'Home Policies',
                        'url'         => Backend::url('ig/helper/homepolicy'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_home_policies'],
                        'order'       => 100,
                    ],
                    'contactform' => [
                        'label'       => 'Contact Forms',
                        'url'         => Backend::url('ig/helper/contactform'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_contactforms'],
                        'order'       => 200,
                    ],
                    'partner' => [
                        'label'       => 'Partners',
                        'url'         => Backend::url('ig/helper/partner'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_partners'],
                        'order'       => 300,
                    ],
                    'service' => [
                        'label'       => 'Services',
                        'url'         => Backend::url('ig/helper/service'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_services'],
                        'order'       => 400,
                    ],
                    'testimonial' => [
                        'label'       => 'Testimonials',
                        'url'         => Backend::url('ig/helper/testimonial'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.helper.manage_testimonials'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }
}
