<?php namespace IG\Helper\Components;

use IG\Helper\Models\Announcement;
use Cms\Classes\ComponentBase;

class AnnouncementMarquee extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AnnouncementMarquee Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $announcement = Announcement::displayable()->first();

        if ($announcement)
            $this->page['announcement'] = $announcement;
    }
}
