<?php namespace IG\Helper\Components;

use Cms\Classes\ComponentBase;
use IG\Helper\Models\Partner;

class OurPartners extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'OurPartners Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $partners = Partner::where('status', true)->get();

        $this->page['partners'] = $partners;
    }
}
