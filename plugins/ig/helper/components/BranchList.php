<?php namespace IG\Helper\Components;

use IG\Helper\Models\Outlet;
use Cms\Classes\ComponentBase;

class BranchList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BranchList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $outlets = Outlet::active()->orderBy('sort_order', 'ASC')->get();

        $this->page['outlets'] = $outlets;
    }
}
