<?php namespace IG\Helper\Components;

use Cms\Classes\ComponentBase;
use IG\Helper\Models\Service;

class OurServices extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'OurServices Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $services = Service::where('status', true)->get();

        $this->page['services'] = $services;
    }
}
