<?php namespace IG\Helper\Components;

use Cms\Classes\ComponentBase;
use IG\Helper\Models\Settings;

class CompanyInfo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CompanyInfo Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $company_info = [];

        $company_info['display_contact'] = Settings::get('display_contact');
        $company_info['primary_contact'] = Settings::get('primary_contact');
        $company_info['contact_email']   = Settings::get('contact_email');
        $company_info['operation_hour']  = Settings::get('operation_hour');
        $company_info['company_address']  = Settings::get('company_address');


        $company_info['facebook_url']    = Settings::get('facebook_url');
        $company_info['instagram_url']   = Settings::get('instagram_url');
        $company_info['twitter_url']     = Settings::get('twitter_url');
        $company_info['youtube_url']     = Settings::get('youtube_url');
        $company_info['tumblr_url']      = Settings::get('tumblr_url');
        $company_info['google_plus_url'] = Settings::get('google_plus_url');
        $company_info['linkedin_url']    = Settings::get('linkedin_url');

        
        $company_info['about_company'] = Settings::get('about_company');
        $company_info['copyright']     = Settings::get('copyright');

        $this->page['company_info'] = $company_info;
    }
}
