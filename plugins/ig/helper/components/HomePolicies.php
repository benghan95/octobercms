<?php namespace IG\Helper\Components;

use Cms\Classes\ComponentBase;
use IG\Helper\Models\HomePolicy;

class HomePolicies extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'HomePolicies Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        $policies = HomePolicy::active()->get();

        $this->page['home_policies'] = $policies;
    }
}
