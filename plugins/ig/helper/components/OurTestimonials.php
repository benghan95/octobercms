<?php namespace IG\Helper\Components;

use Cms\Classes\ComponentBase;
use IG\Helper\Models\Testimonial;

class OurTestimonials extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'OurTestimonials Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['testimonials'] = Testimonial::all();
    }
}
