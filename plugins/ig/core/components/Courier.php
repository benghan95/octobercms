<?php namespace IG\Core\Components;

use Cms\Classes\ComponentBase;

class Courier extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Courier Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
