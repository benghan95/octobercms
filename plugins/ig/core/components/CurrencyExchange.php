<?php namespace IG\Core\Components;

use Redirect;
use Session;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use IG\Core\Models\Currency;
use IG\User\Models\User;
use IG\Transact\Models\Order;
use RainLab\User\Facades\Auth;

class CurrencyExchange extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CurrencyExchange Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $this->page['userCurrency'] = Currency::getUserCurrency();

        $currencies = Currency::where('available', true)->get();
        $this->page['currenciesList'] = $currencies;
    }

    public function onChangeCurrency(){
        $data = post();

        $user = Auth::getUser();

        if($user){
            $user = User::find($user->id);
            $user->currency_id = $data['currency'];
            $user->save();
        }
        else{
            Session::put('currency', $data['currency']);
        }

        $order = Order::get_existing_order_or_createnew();

        if ($order){
            foreach ($order->orderitems as $orderitem){
                if ($orderitem->product == null){
                    $orderitem->delete();
                    continue;
                }

                $orderitem->currency_id = $data['currency'];
                $orderitem->unit_price  = $orderitem->itemstocks[0]->option->activePricing['price'];
                $orderitem->gst         = $orderitem->itemstocks[0]->option->activePricing['gst'];
                $orderitem->save();
            }
        }

        return $data;
    }
}
