<?php namespace IG\Core\Components;

use Cms\Classes\ComponentBase;

class SearchInput extends \OFFLINE\SiteSearch\Components\SearchInput
{
    public function defineProperties()
    {
        return [
            'useAutoComplete' => [
                'title'       => 'offline.sitesearch::lang.searchInput.properties.use_auto_complete.title',
                'type'        => 'checkbox',
                'default'     => 1,
            ],
            'autoCompleteResultCount' => [
                'title'             => 'offline.sitesearch::lang.searchInput.properties.auto_complete_result_count.title',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Please enter only numbers',
                'default'           => '5',
            ],
            'showProviderBadge' => [
                'title'       => 'offline.sitesearch::lang.searchResults.properties.provider_badge.title',
                'description' => 'offline.sitesearch::lang.searchResults.properties.provider_badge.description',
                'type'        => 'checkbox',
                'default'     => 1,
            ],
            'searchPage'              => [
                'title'       => 'offline.sitesearch::lang.searchInput.properties.search_page.title',
                'description' => 'offline.sitesearch::lang.searchInput.properties.search_page.description',
                'type'        => 'dropdown',
            ],
            // 'mobile' => [
            //     'title' => 'Is Mobile search',
            //     'description' => 'Unique ID for search',
            //     'type'        => 'checkbox',
            //     'default'     => 1,
            // ]
        ];
    }

    // public function isMobile(){
    //     $isMobile = $this->property('mobile');

    //     if($isMobile){
    //         return "mobile";
    //     }

    //     return "desktop";
    // }
}
