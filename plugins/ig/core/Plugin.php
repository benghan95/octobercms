<?php namespace IG\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IG\Core\Components\CurrencyExchange' => 'currencyExchange',
            'IG\Core\Components\SearchResults'    => 'productSearchResults',
            'IG\Core\Components\SearchInput'      => 'proSearchInput',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Server Type',
                'description' => 'Identify whether the server is in staging or production',
                'category'    => 'IG Core',
                'icon'        => 'icon-cog',
                'class'       => 'IG\Core\Models\Settings',
                'order'       => 550,
                'permissions' => ['ig.core.manage_settings']
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'ig.core.manage_deliveries' => [
                'tab' => 'Core',
                'label' => 'Manage Deliveries'
            ],
            'ig.core.manage_orderstatuses' => [
                'tab' => 'Core',
                'label' => 'Manage Order Statuses'
            ],
            'ig.core.manage_currencies' => [
                'tab' => 'Core',
                'label' => 'Manage Currencies'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'core' => [
                'label'       => 'Core',
                'url'         => Backend::url('ig/core/currency'),
                'icon'        => 'icon-leaf',
                'permissions' => ['ig.core.*'],
                'order'       => 130,
                'sideMenu'    => [
                    'courier' => [
                        'label'       => 'Couriers',
                        'url'         => Backend::url('ig/core/courier'),
                        'icon'        => 'icon-usd',
                        'permissions' => ['ig.core.manage_couriers'],
                        'order'       => 100,
                    ],
                    'currency' => [
                        'label'       => 'Currencies',
                        'url'         => Backend::url('ig/core/currency'),
                        'icon'        => 'icon-usd',
                        'permissions' => ['ig.core.manage_currencies'],
                        'order'       => 100,
                    ],
                    'delivery' => [
                        'label'       => 'Deliveries',
                        'url'         => Backend::url('ig/core/delivery'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.core.manage_deliveries'],
                        'order'       => 100,
                    ],
                    'orderstatus' => [
                        'label'       => 'Order Statuses',
                        'url'         => Backend::url('ig/core/orderstatus'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.core.manage_orderstatuses'],
                        'order'       => 200,
                    ],
                ]
            ],
        ];
    }
}
