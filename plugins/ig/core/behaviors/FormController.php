<?php namespace IG\Core\Behaviors;


class FormController extends \Backend\Behaviors\FormController {
    /**
     * Initialize the form configuration against a model and context value.
     * This will process the configuration found in the `$formConfig` property
     * and prepare the Form widget, which is the underlying tool used for
     * actually rendering the form. The model used by this form is passed
     * to this behavior via this method as the first argument.
     *
     * @see Backend\Widgets\Form
     * @param October\Rain\Database\Model $model
     * @param string $context Form context
     * @return void
     */
    public function initForm($model, $context = null)
    {
        if ($context !== null) {
            $this->context = $context;
        }

        $context = $this->formGetContext();

        /*
         * Each page can supply a unique form definition, if desired
         */
        $formFields = $this->config->form;

        if ($context == self::CONTEXT_CREATE) {
            $formFields = $this->getConfig('create[form]', $formFields);
        }
        elseif ($context == self::CONTEXT_UPDATE) {
            $formFields = $this->getConfig('update[form]', $formFields);
        }
        elseif ($context == self::CONTEXT_PREVIEW) {
            $formFields = $this->getConfig('preview[form]', $formFields);
        }

        $config = $this->makeConfig($formFields);
        $config->model = $model;
        $config->arrayName = class_basename($model);
        $config->context = $context;

        /*
         * Form Widget with extensibility
         */
        $this->formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $this->formWidget->bindEvent('form.extendFieldsBefore', function () {
            $this->controller->formExtendFieldsBefore($this->formWidget);
        });

        $this->formWidget->bindEvent('form.extendFields', function ($fields) {
            $this->controller->formExtendFields($this->formWidget, $fields);
        });

        $this->formWidget->bindEvent('form.beforeRefresh', function ($holder) {
            $result = $this->controller->formExtendRefreshData($this->formWidget, $holder->data);
            if (is_array($result)) $holder->data = $result;
        });

        $this->formWidget->bindEvent('form.refreshFields', function ($fields) {
            return $this->controller->formExtendRefreshFields($this->formWidget, $fields);
        });

        $this->formWidget->bindEvent('form.refresh', function ($result) {
            return $this->controller->formExtendRefreshResults($this->formWidget, $result);
        });

        $this->formWidget->bindToController();

        /*
         * Detected Relation controller behavior
         */
        if ($this->controller->isClassExtendedWith('Backend.Behaviors.RelationController') ||
            $this->controller->isClassExtendedWith('IG.Core.Behaviors.RelationController')
        ) {
            $this->controller->initRelation($model);
        }

        $this->prepareVars($model);
        $this->model = $model;
    }
}