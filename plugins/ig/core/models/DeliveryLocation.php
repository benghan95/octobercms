<?php namespace IG\Core\Models;

use Model;
use IG\Core\Models\Country;
use IG\Core\Models\State;

/**
 * DeliveryLocation Model
 */
class DeliveryLocation extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_delivery_locations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'delivery' => ['IG\Core\Models\Delivery', 'key' => 'delivery_id'],
        'country' => ['IG\Core\Models\Country', 'key' => 'country_id'],
        'state' => ['IG\Core\Models\State', 'key' => 'state_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave() {
        if (!$this->primary_weight) {
            $this->primary_weight = 0;
        }
        if (!$this->primary_price) {
            $this->primary_price = 0;
        }
        if (!$this->consec_weight) {
            $this->consec_weight = 0;
        }
        if (!$this->consec_price) {
            $this->consec_price = 0;
        }
    }

    public function getCountryIdOptions(){
        $countries = Country::all();
        $options = [];
        $options[""] = "--- Select a Country ---";
        foreach ($countries as $country){
            $options[$country->id] = $country->name;
        }

        return $options;
    }

    public function getStateIdOptions(){
        if($this->country_id){
            $country = Country::find($this->country_id);

            if ($country){
                $states = $country->states;
                $options = [
                    '' => '--- Select a State ---'
                ];
                // $options[""] = "--- Select Subcategory ---";

                foreach ($states as $state){
                    $options[$state->id] = $state->name;
                }

                if (!empty($options))
                    return $options;
                else
                    return ["" => "No State Found"];
            }
        }

        return ["" => "--- Select Country First ---"];
    }
}
