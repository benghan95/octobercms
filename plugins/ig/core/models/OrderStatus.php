<?php namespace IG\Core\Models;

use Model;

/**
 * OrderStatus Model
 */
class OrderStatus extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_order_statuses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function guest(){
        return OrderStatus::where('slug', 'guest')->first();
    }

    public static function order_created(){
        return OrderStatus::where('slug', 'order-created')->first();
    }

    public static function pending(){
        return OrderStatus::where('slug', 'pending')->first();
    }

    public static function updated_address(){
        return OrderStatus::where('slug', 'updated-address')->first();
    }

    /**
     * @return mixed
     *
     *
     * Below is where we started tracking process, sort by descending order (function)
     *
     *
     */
    public static function pending_payment(){
        return OrderStatus::where('slug', 'pending-payment')->first();
    }

    /**
     * @return mixed
     *
     * iPay
     *
     */
    public static function attempt_ipay88(){
        return OrderStatus::where('slug', 'attempt-payment')->first();
    }

    public static function ipay_failed(){
        return OrderStatus::where('slug', 'payment-failed')->first();
    }

    public static function ipay_signature_unmatched(){
        return OrderStatus::where('slug', 'payment-signature-unmatched')->first();
    }

    public static function ipay_successful(){
        return OrderStatus::where('slug', 'payment-success')->first();
    }

    public static function processing(){
        return OrderStatus::where('slug', 'processing')->first();
    }

    public static function order_received(){
        return OrderStatus::where('slug', 'order-received')->first();
    }

    public static function completed(){
        return OrderStatus::where('slug', 'completed')->first();
    }

    public static function order_failed(){
        return OrderStatus::where('slug', 'order-failed')->first();
    }

    public static function shipped(){
        return OrderStatus::where('slug', 'shipped')->first();
    }
}
