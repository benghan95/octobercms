<?php namespace IG\Core\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'ig_core_server_env_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
