<?php namespace IG\Core\Models;

use Model;

/**
 * Courier Model
 */
class Courier extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_couriers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
