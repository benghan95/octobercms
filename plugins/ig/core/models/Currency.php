<?php namespace IG\Core\Models;

use Model;
use Session;
use IG\Core\Models\Country;
use IG\User\Models\User;
use RainLab\User\Facades\Auth;

/**
 * Currency Model
 */
class Currency extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_currencies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeDefaultCurrency($query){
        return $query->where('unit', 'MYR')->first();
    }

    public static function getUserCurrency(){
        $user = Auth::getUser();

        if($user){
            $user = User::find($user->id);

            if ($user->currency)
                return $user->currency;
            else
                return Currency::defaultCurrency();
        }
        else{
            $sessionCurrency = Session::get('currency');
            if ($sessionCurrency)
                return Currency::find($sessionCurrency);
            else
                return Currency::defaultCurrency();
        }
    }

    public static function getCurrencyUnit($unit){
        return static::where('unit', $unit)->first();
    }
}
