<?php namespace IG\Core\Models;

use Model;
use IG\Core\Models\Zone;

/**
 * Delivery Model
 */
class Delivery extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_deliveries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'locations' => ['IG\Core\Models\DeliveryLocation', 'key' => 'delivery_id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $jsonable = [
        'primary_price',
        'cont_price'
    ];

    public function beforeSave(){
        if ($this->min == null)
            $this->min = 0;

        if ($this->amount_waive == null)
            $this->amount_waive = 99999;
    }
}
