<?php namespace IG\Core\Models;

use Model;

/**
 * Country Model
 */
class Country extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_core_countries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'delivery' => ['IG\Core\Models\DeliveryLocation', 'key' => 'country_id'],
        'states' => ['IG\Core\Models\State', 'key' => 'countries_id'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeActive($query){
        return $query->where('active', 1);
    }

    public function scopeCountryCode($query, $code){
        return $query->where('iso_code_2', $code);
    }

    public static function hasDelivery(){
        return static::whereHas('delivery');
    }
}
