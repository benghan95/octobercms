<?php namespace IG\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Order Status Back-end Controller
 */
class OrderStatus extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.Core', 'core', 'orderstatus');
    }
}
