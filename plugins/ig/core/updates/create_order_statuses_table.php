<?php namespace IG\Core\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderStatusesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_core_order_statuses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug')->unique();
            $table->string('description')->nullable();
            $table->boolean('notifiable')->default(false);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_core_order_statuses');
    }
}
