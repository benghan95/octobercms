<?php namespace IG\Core\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_core_currencies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title')->nullable();
            $table->string('unit')->unique();
            $table->string('country_code')->nullable();
            $table->float('rate', 10, 4);
            $table->boolean('available')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_core_currencies');
    }
}
