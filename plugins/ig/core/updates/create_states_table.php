<?php namespace IG\Core\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStatesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_core_states', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('countries_id')->unsigned();
            $table->foreign('countries_id')->references('id')->on('ig_core_countries')->onDelete('cascade');

            $table->string('code');
            $table->string('name');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_core_states');
    }
}
