<?php namespace IG\Core\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveriesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_core_deliveries', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->text('description')->nullable();
            $table->text('more_info')->nullable();
            $table->float('min', 10, 2)->defaul(0.00);
            $table->float('amount_waive', 10, 2)->defaul(0.00);

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_core_deliveries');
    }
}
