<?php namespace IG\Core\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDeliveryLocationsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_core_delivery_locations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('delivery_id')->unsigned();
            $table->foreign('delivery_id')->references('id')->on('ig_core_deliveries');

            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('ig_core_countries');

            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('ig_core_states');

            $table->float('primary_weight', 10, 3)->default(0.000);
            $table->float('primary_price', 10, 2)->default(0.00);

            $table->float('consec_weight', 10, 3)->default(0.000);
            $table->float('consec_price', 10, 2)->default(0.00);

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_core_delivery_locations');
    }
}
