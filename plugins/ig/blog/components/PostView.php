<?php namespace IG\Blog\Components;

use Cms\Classes\ComponentBase;
use IG\Blog\Models\Post;

class PostView extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PostView Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        if ($slug = $this->param('slug') != null) {
            $post = Post::where('slug', $this->param('slug'))
                        ->active()
                        ->first();

            if( !$post ) {
                $this->setStatusCode(404);
                return $this->controller->run('404');
            }

            $this->page['post'] = $post;

            $this->recentPost($post);
        }
    }

    public function recentPost($post) {

    }
}
