<?php namespace IG\Blog\Components;

use Cms\Classes\ComponentBase;
use IG\Blog\Models\Post;

class PostList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PostList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        $this->page['posts'] = Post::active()->orderBy('published_at', 'DESC')->get();;
    }
}
