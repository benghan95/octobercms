<?php namespace IG\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_blog_posts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content');
            $table->text('tags')->nullable();
            $table->text('seo_description')->nullable();
            $table->timestamp('published_at');
            $table->boolean('status')->default(true);
            
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('backend_users');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_blog_posts');
    }
}
