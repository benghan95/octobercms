<?php namespace IG\Blog\Models;

use Model;

/**
 * Post Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_blog_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File',
    ];
    public $attachMany = [];

    public $jsonable = [
        'tags'
    ];

    public static function active(){
        return static::where('status', true);
    }

    public function scopeActive($query){
        return $query->where('status', true);
    }

    public function beforeSave() {
        $this->created_by = \BackendAuth::getUser()->id;
        if ($this->published_at)
            $this->published_at = \Carbon\Carbon::parse($this->published_at)->setTime(0, 0, 0);
    }
}
