<?php namespace IG\Transact\Models;

use Model;
use IG\Transact\Models\Payment;
use IG\Transact\Models\Settings;
use Illuminate\Support\Facades\Log;

/**
 * Ipay Model
 */
class Ipay extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_ipays';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'payment' => ['IG\Transact\Models\Payment', 'key' => 'payment_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopePaid($query, $refno){
        return $query->where('ref_no', $refno)->where('status', 1);
    }

    public static function justPay($order, $details){
        $payment = Payment::justCreate($order);

        $ipay88 = new Ipay();
        $ipay88->ipay_payment_id = $details['PaymentId'];
        $ipay88->ref_no          = $details['RefNo'];
        $ipay88->currency        = $details['Currency'];

        $userName = "";
        $userEmail = "";
        $userPhone = "";

        if($order->user){
            $userName = $order->user->name;
            $userEmail =  $order->user->email;

            if ($order->user->contact_no) {
                $userPhone = $order->user->contact_no;
            }
        }else{
            $userName = $order->temp['name'];
            $userEmail = $order->temp['email'];
            $userPhone = $order->temp['contact_no'];
        }

        $ipay88->name           = $userName;
        $ipay88->email          = $userEmail;
        $ipay88->contact        = $userPhone;
        $ipay88->remark         = $details['Remark'];
        $ipay88->signature      = $details['Signature'];
        $ipay88->transaction_no = $details['TransId'];
        $ipay88->auth_code      = $details['AuthCode'];
        if ($details['Status'])
            $ipay88->status         = true;
        else
            $ipay88->status         = false;
        $ipay88->err_desc       = $details['ErrDesc'];
        $ipay88->cc_name        = $details['CCName'];
        $ipay88->cc_no          = $details['CCNo'];
        $ipay88->bank_name      = $details['S_bankname'];
        $ipay88->country        = $details['S_country'];
        $ipay88->lang           = "";
        $ipay88->payment_id     = $payment->id;
        $ipay88->save();

        return $ipay88;
    }

    /**
     *
     * @param $source
     * @return string
     *
     *
     * signIPay88 and hex2bin is for iPay88 payment gateway
     *
     */
    public static function signIPay88($source){
        return base64_encode(hex2bin(sha1($source)));
    }

    public static function signIPay88256($source){
        return hash('sha256', $source);
    }

    private function hex2bin($hexSource)
    {
        $bin = "";
        for ($i=0;$i<strlen($hexSource);$i=$i+2)
        {
            $bin .= chr(hexdec(substr($hexSource,$i,2)));
        }

        return $bin;
    }

    public static function requery($MerchantCode, $RefNo, $Amount){
        if(empty($MerchantCode)
        || empty($RefNo)
        || empty($Amount))
            return FALSE;

        $query = "https://www.mobile88.com/epayment/enquiry.asp?MerchantCode=" .
        $MerchantCode . "&RefNo=" . $RefNo . "&Amount=" . $Amount;
        $url = parse_url($query);
        $host = $url["host"];
        $path = $url["path"] . "?" . $url["query"];
        $timeout = 1;
        $fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
        if ($fp) {
            fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
            $buf = "";
            while (!feof($fp)) {
                $buf .= fgets($fp, 128);
            }
            $lines = explode("\n", $buf);
            $Result = $lines[count($lines)-1];
            fclose($fp);
        }
        else {
            Log::error("Requery failed for: " . $RefNo);
        }

        return $Result;
    }

    public static function verifyResponse($order, $calculated, $details){
        $merchantCode = $details['MerchantCode'];
        $paymentID    = $details['PaymentId'];
        $refNo        = $details['RefNo'];
        $amount       = $details['Amount'];
        $currency     = $details['Currency'];
        $remark       = $details['Remark'];
        $transID      = $details['TransId'];
        $authCode     = $details['AuthCode'];
        $status       = $details['Status'];
        $erroDesc     = $details['ErrDesc'];
        $signature    = $details['Signature'];

        if(empty($merchantCode)
        || empty($paymentID)
        || empty($refNo)
        || empty($amount)
        || empty($currency)
        || empty($status)
        || empty($signature))
            return FALSE;

        //Check signature
        if($signature != Ipay::signaturePost($order, $calculated, $details))
            return FALSE;

        //Check MerchantCode
        if($merchantCode != Settings::get('ipay_merchantcode'))
            return FALSE;

        return TRUE;
    }

    public static function signature($order, $calculated){
        $merchantKey = Settings::get('ipay_merchantkey');
        $merchantCode = Settings::get('ipay_merchantcode');

        $refCode = $order->id;
        $subtotal = Order::grandTotalByOrders($order->id);

        $preSign = $merchantKey . $merchantCode . $refCode . Ipay::removeDotsComma($calculated['grand']['formatted']) . $calculated['currency']['unit'];

        $signed = Ipay::signIPay88256($preSign);

        return $signed;
    }

    public static function signaturePost($order, $calculated, $details){
        $merchantKey = Settings::get('ipay_merchantkey');
        $merchantCode = Settings::get('ipay_merchantcode');

        $refCode = $order->id;
        $amount = Ipay::removeDotsComma($calculated['grand']['formatted']);
        $currency = $calculated['currency']['unit'];
        $status = "1";

        $postSign = $merchantKey . $merchantCode . $details['PaymentId'] . $refCode . $amount . $currency . $status;

        $signed = Ipay::signIPay88256($postSign);

        return $signed;
    }

    public static function removeDotsComma($input){
        $noCommma = str_replace(",", "", $input);
        $noDot = str_replace(".","", $noCommma);

        return $noDot;
    }
}
