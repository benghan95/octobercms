<?php namespace IG\Transact\Models;

use Model;
use Carbon\Carbon;
use IG\Core\Models\OrderStatus;
use IG\Core\Models\Currency;
use IG\Commerce\Classes\IG;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\Category;
use IG\User\Models\User;
use IG\Transact\Classes\Hexing;
use IG\Helper\Models\Settings as HelperSettings;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use RainLab\User\Facades\Auth;

/**
 * Order Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'payment' => ['IG\Transact\Models\Payment', 'key' => 'order_id'],
    ];
    public $hasMany = [
        'orderitems' => ['IG\Transact\Models\OrderItem', 'key' => 'order_id'],
    ];
    public $belongsTo = [
        'courier'          => ['IG\Core\Models\Courier', 'key' => 'courier_id'],
        'currency'         => ['IG\Core\Models\Currency', 'key' => 'currency_id'],
        'orderstatus'      => ['IG\Core\Models\OrderStatus', 'key' => 'status_id'],
        'address'          => ['IG\User\Models\Address', 'key' => 'shipping_id'],
        'billing'          => ['IG\User\Models\Address', 'key' => 'billing_id'],
        'deliverylocation' => ['IG\Core\Models\DeliveryLocation', 'key' => 'delivery_id'],
        'promocode'        => ['IG\Transact\Models\Promocode', 'key' => 'promocode_id'],
        'user'             => ['IG\User\Models\User', 'key' => 'user_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'invoice_pdf' => 'System\Models\File',
    ];
    public $attachMany = [];
    public $morphToMany = [
        'history' => ['IG\Transact\Models\OrderHistory', 'name' => 'historable', 'table' => 'ig_transact_order_historable']
    ];

    public $jsonable = [
        'temp'
    ];

    public function beforeUpdate(){
        if ($this->isDirty('tracking_no') || $this->isDirty('courier_id')){
            if ($this->status_id == OrderStatus::shipped()['id'])
                Event::fire('order.history.status.change', [OrderStatus::shipped()['id'], $this]);
        }
        if ($this->isDirty('status_id')){
            Event::fire('order.history.status.change', [$this->status_id, $this]);
        }
    }

    public function getTotalWeightAttribute(){
        $total_weight = 0;
        foreach ($this->orderitems as $orderitem){
            $product = $orderitem->product;

            if($product == null){
                $orderitem->delete();
                continue;
            }

            $total_weight += $orderitem->unit_weight * $orderitem->quantity;
        }

        return $total_weight;
    }

    public function getThumbAttribute(){
        $product = $this->product();
        $productOption = $this->productoption();

        $productOption = $productOption->first();
        if($productOption){
            if($productOption->smartthumb)
                return $productOption->smartthumb['path'];//->getThumb(50,50);
        }

        $product = $product->first();
        if($product){
            if($product->smartthumb)
                return $product->smartthumb['path'];//->getThumb(50,50);
        }

        return false;
    }

    public function scopeDefaultOrderWiths($query){
        return $query->with(
            'orderitems'
        );
    }

    public function getAmountRefundAttribute()
    {
        $order = self::grandTotalByOrders($this->id);
        return $order['grand']['total'];
    }

    public function getGrandAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['grand']['total'];
    }

    public function getSubtotalAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['subtotal']['total'];
    }

    public function getTaxAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['tax']['total'];
    }

    public function getDiscountAmountAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['discount']['total'];
    }

    public function getDiscountCodeAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['discount']['code'];
    }

    public function getDeliveryChargeAttribute(){
        $order = self::grandTotalByOrders($this->id);

        return $order['delivery_charge']['total'];
    }

    public static function get_existing_if_exists() {
        $existingOrder = Order::pendingAndOwnedByUser();

        if($existingOrder)
            return $existingOrder;

        if(!Auth::check()){
            if(Session::has('guest')){
                $guestOrder = Order::pendingAndOwnedByGuest();
                if($guestOrder)
                    return $guestOrder;
            } else if (Session::has('guest-checkout')) {
                $guestCheckOutOrderID = Session::get('guest-checkout');
                $order = Order::where('id', $guestCheckOutOrderID)->first();

                if($order)
                    return $order;
            }

            return null;
        }
    }

    /**
     * @return bool|Order|mixed
     *
     * This function will either return existing pending order or create new one
     *
     */
    public static function get_existing_order_or_createnew(){
        $existingOrder = Order::pendingAndOwnedByUser();

        if($existingOrder)
            return $existingOrder;

        if(!Auth::check()){
            if(Session::has('guest')){
                $guestOrder = Order::pendingAndOwnedByGuest();
                if($guestOrder)
                    return $guestOrder;

                Session::regenerate();
            }
            else if(Session::has('guest-checkout')){
                $guestCheckOutOrderID = Session::get('guest-checkout');
                $order = Order::where('id', $guestCheckOutOrderID)->first();

                if($order)
                    return $order;

                Session::regenerate();
            }
            else{
                Session::regenerate();
            }

            $newOrder = new Order();
            $newOrder->session_code = Session::getId();
            $newOrder->orderstatus = OrderStatus::guest();
            $newOrder->save();

            $newOrder->hexhash = Hexing::hex($newOrder->id);
            $newOrder->save();

            Session::put('guest', $newOrder->id);
            Event::fire('order.history.status.change', [OrderStatus::order_created()['id'], $newOrder]);

            return $newOrder;
        }

        $newOrder = self::create_order();
        Event::fire('order.history.status.change', [OrderStatus::order_created()['id'], $newOrder]);

        return $newOrder;
    }

    /**
     * @param $user
     * @return bool
     *
     * This function will take unclaimed Bag from guest session and award to new guy that uses the same session to register
     *
     */
    public static function giveThisBagToThisUser($user){
        if(!Session::has('guest'))
            return FALSE;

        $session_code = Session::getId();
        Session::forget('guest');

        $orderOwnedByGuest = Order::where('session_code', $session_code)
                                ->where('status_id', OrderStatus::guest()['id'])
                                ->first();

        $orderOwnedByUser = self::get_existing_order_or_createnew();
        if($orderOwnedByGuest){
            if(count($orderOwnedByGuest->orderitems) > 0){
                foreach ($orderOwnedByGuest->orderitems as &$orderItem ){
                    $orderItemExist = IG::checkItemExistInCart($orderOwnedByUser, $orderItem->SKU);

                    if($orderItemExist){
                        $orderItemExist->quantity = $orderItemExist->quantity + $orderItem->quantity;
                        $orderItemExist->save();

                        $orderItem->delete();
                    }else{
                        $orderItem->order = $orderOwnedByUser;
                        $orderItem->save();
                    }
                }

                $orderOwnedByGuest->delete();
            }
        }
    }

    /**
     * @param bool $user
     * @return Order
     *
     * Create order for an user.
     *
     */
    public static function create_order($user = FALSE){
        $_user = Auth::getUser();

        //Override to assign to user
        if($user)
            $_user = $user;

        $newOrder = new Order();
        $newOrder->user = $_user;
        $user = User::find($_user->id);
        if ($user->agenttype)
            $newOrder->is_agent = true;
        $newOrder->orderstatus = OrderStatus::pending();
        $newOrder->save();

        $newOrder->hexhash = Hexing::hex($newOrder->id);
        // $newOrder->invoice_no = $_user['id'].'.'.$newOrder['id'];
        $newOrder->save();

        return $newOrder;
    }

    public static function grandTotalByOrders($orderid){
        if($orderid == null)
            return false;

        $subtotal = self::subTotalByOrders($orderid);
        $currency = Currency::getUserCurrency();

        $order = Order::where('id', $orderid)->first();

        $deliverylocation = $order->deliverylocation;
        if($deliverylocation){
            $deliveryCharge = self::deliveryFeesByOrder($orderid, $deliverylocation);

            if ($deliverylocation->delivery){
                $delivery = $deliverylocation->delivery;
                if ($delivery->amount_waive > 0){
                    if (!$order->is_agent) {
                        if($subtotal['total'] >= round($delivery->amount_waive * $currency->rate, 2)) {
                            $deliveryCharge = [
                                'currency'     => $currency,
                                'free'         => true,
                                'total'        => 0,
                                'formatted'    => number_format(0.00, 2),
                                'total_weight' => $order->totalWeight,
                            ];
                        }
                    }
                }
            }
        } else {
            $deliveryCharge = [
                'currency'     => $currency,
                'free'         => false,
                'total'        => 0,
                'formatted'    => number_format(0.00, 2),
                'total_weight' => $order->totalWeight,
            ];
        }

        $discountTotal = 0;
        $discountType = null;
        $discountCode = null;

        $promocode = $order->promocode;

        if($promocode){
            $discountType  = "promocode";
            $discountCode  = $promocode->code;
            $discountTotal = self::calcDiscountAmount($order, $promocode, $deliveryCharge);

            if ($promocode->discount_cap != 0){
                if($discountTotal > round($promocode->discount_cap * $currency->rate, 2)){
                    $discountTotal = round($promocode->discount_cap * $currency->rate, 2);
                }
            }

            if ($subtotal['total'] <= $discountTotal){
                $discountTotal = $subtotal['total'];
            }
        }

        $discount = $discountTotal;
        $grand    = round($subtotal['total'] - $discount, 2) + $deliveryCharge['total'];
        $tax      = self::gstTotalByOrders($orderid)['total'];

        return [
            'currency' => $currency,
            'subtotal' => $subtotal,
            'tax' => [
                'currency'  => $currency,
                'total'     => round($tax, 2),
                'formatted' => number_format($tax, 2),
            ],
            'discount' => [
                'currency'  => $currency,
                'type'      => $discountType,
                'code'      => $discountCode,
                'total'     => round($discount, 2),
                'formatted' => number_format($discount, 2),
            ],
            'delivery_charge' => $deliveryCharge,
            'grand' => [
                'currency'  => $currency,
                'total'     => round($grand, 2),
                // 'total'     => round(1.00, 2),
                'formatted' => number_format($grand, 2),
                // 'formatted' => number_format(1.00, 2),
            ]
        ];
    }

    public static function deliveryFeesByOrder($orderid, $deliverylocation){
        $order        = Order::where('id', $orderid)->first();
        $prices       = [];
        $total        = 0;
        $total_weight = 0;
        $currency     = Currency::getUserCurrency();

        $total_weight  = ceil($order->totalWeight);
        $primary_price = round($currency->rate * $deliverylocation->primary_price, 2);
        $consec_price  = round($currency->rate * $deliverylocation->consec_price, 2);

        if ($deliverylocation->primary_weight > 0){
            if ($total_weight <= $deliverylocation->primary_weight){
                $total += $total_weight * $primary_price;
            }
            else{
                $consec_weight = $total_weight - $deliverylocation->primary_weight;
                $total += $deliverylocation->primary_weight * $primary_price;

                if ($deliverylocation->consec_weight > 0){
                    $total += round($consec_weight/$deliverylocation->consec_weight) * $consec_price;
                }
                else{
                    $total += $consec_price;
                }
            }
        }
        else{
            $total += $total_weight * round($currency->rate * $deliverylocation->primary_price, 2);
        }

        return [
            'currency'     => $currency,
            'free'         => false,
            'total'        => round($total, 2),
            'formatted'    => number_format($total, 2),
            'total_weight' => $order->totalWeight,
        ];
    }

    public static function gstTotalByOrders($orderid){
        $order = Order::where('id', $orderid)->first();
        $orderStatus = $order->orderstatus;

        $total_gst = 0;
        $currency = Currency::getUserCurrency();

        foreach($order->orderitems as $orderitem){
            $product = $orderitem->product;

            if($product == null){
                $orderitem->delete();
                continue;
            }

            $total_gst += $orderitem->quantity * round($currency->rate * $orderitem->gst, 2);
        }

        return  [
            'currency'  => $currency,
            'total'     => round($total_gst, 2),
            'formatted' => number_format($total_gst, 2),
        ];
    }

    public static function subTotalByOrders($orderid){
        $order = Order::where('id', $orderid)->first();
        $orderStatus = $order->orderstatus;

        $prices = [];
        $subtotal = 0;
        $currency = Currency::getUserCurrency();

        foreach($order->orderitems as $orderitem){
            $product = $orderitem->product;

            if($product == null){
                $orderitem->delete();
                continue;
            }

            $subtotal += $orderitem->quantity * $orderitem->unit_price;

            $item = [
                'composite' => $orderitem->composite,
                'price'     => $orderitem->pricing,
                'piece'     => $orderitem->quantity
            ];

            array_push($prices, $item);
        }

        return  [
            'currency'  => $currency,
            'total'     => round($subtotal, 2),
            'formatted' => number_format($subtotal, 2),
            'items'     => $prices,
        ];
    }

    public static function lockdownPrice($orderid){

    }

    /**
     * @param bool $with
     * @return bool
     *
     * If user isn't logged in, he is still a guest, we need to have a way to save their information and their cart
     *
     */
    public static function pendingAndOwnedByGuest(){
        if(!Session::has('guest'))
            return FALSE;

        $session_code = Session::getId();
        $orderOwned = Order::defaultOrderWiths()
            ->where('session_code', $session_code)
            ->where('status_id', OrderStatus::guest()['id'])
            ->first();

        if($orderOwned)
            return $orderOwned;

        Session::regenerate();
    }

    /**
     *
     * Returns pending order, optionally with With array(),
     * ie, array('orderitems', 'payments')
     *
     * @param bool $with
     * @return mixed
     */
    public static function pendingAndOwnedByUser(){
        if(!Auth::check())
            return FALSE;

        $currentuser = Auth::getUser();

        $orderOwned = Order::defaultOrderWiths()
                        ->where('user_id', $currentuser['id'])
                        ->whereIn('status_id', [
                            OrderStatus::pending()['id'],
                            OrderStatus::pending_payment()['id']
                        ])
                        ->first();

        if($orderOwned)
            return $orderOwned;

        return FALSE;
    }

    public static function pendingAndByHash($hexhash){
        $order = Order::where('hexhash', $hexhash)
            ->whereIn('status_id', [
                OrderStatus::pending_payment()['id']
            ])
            ->first();

        return $order;
    }

    public static function checkOrderItemOwnedByOwner($orderitem){
        return Order::whereHas('orderitems', function($q) use ($orderitem)
        {
            $q->where('id', $orderitem);
        })
            ->where('user_id', Auth::getUser()['id'])
            ->first();
    }

    /**
     *
     * Return order when given Ref Code
     *
     * @param $query
     * @param $refCode
     * @return mixed
     */
    public function scopeByRefCode($query, $refCode){
        return $query->where('orderid', $refCode);
    }

    /**
     *
     * All functions below is for console use
     * Only for Ready Made Product
     *
     */

    public static function expiredGuestOrder(){
        $expiredGuestOrder = Order::whereIn('status_id', [
                OrderStatus::guest()['id']
            ])
            ->where('updated_at', '<=', Carbon::now('Asia/Kuala_Lumpur')->subMinutes(Settings::get('order_expired')))
            ->get();

        return $expiredGuestOrder;
    }

    public static function expiredUserOrder(){
        $expiredUserOrder = Order::whereIn('status_id', [
                OrderStatus::pending()['id'],
                OrderStatus::pending_payment()['id']
            ])
            ->where('updated_at', '<=', Carbon::now('Asia/Kuala_Lumpur')->subMinutes(Settings::get('order_expired')))
            ->get();

        return $expiredUserOrder;
    }

    public static function pendingOrders(){
        $expiredGuestOrder = Order::whereIn('status_id', [
                OrderStatus::guest()['id'],
                OrderStatus::pending()['id'],
                OrderStatus::pending_payment()['id']
            ])
            ->get();

        return $expiredGuestOrder;
    }

    private static function calcDiscountAmount($order, $promocode, $deliveryCharge){
        $currency = Currency::getUserCurrency();
        foreach($order->orderitems as $orderitem){
            $orderitem->discounted = false;
        }

        $discountTotal = 0.00;
        $discountAmount = $promocode->discount_amount;
        if (!$discountAmount)
            $discountAmount = 0;

        switch ($promocode->discount_type) {
            case "basket-discount":
                $discountType = "price";
                break;

            case "basket-perc-discount":
            default:
                $discountType = "percent";
                break;
        }

        if (!$promocode->specific_categories){
            foreach($order->orderitems as $orderitem){
                $product = $orderitem->product;

                if ($product) {
                    if(!$orderitem->discounted){
                        if($discountType == "price")
                            $discountTotal = round($currency->rate * $discountAmount, 2);
                        else if($discountType == "percent")
                            $discountTotal += $orderitem->totalAmount['total'] * $discountAmount / 100;

                        $orderitem->discounted = true;
                    }
                }
            }

            if ($promocode->free_delivery)
                $discountTotal += $deliveryCharge['total'];

            return $discountTotal;
        }

        if ($promocode->specific_categories){
            foreach($promocode->specific_categories as $specific_category){
                $category = Category::where('slug', $specific_category)->first();

                if ($category){
                    foreach($order->orderitems as $orderitem){
                        if(!$orderitem->discounted){
                            $product = $orderitem->product;

                            if ($product && $product->category) {
                                if($product->product_category->id == $category->id){
                                    if($discountType == "price"){
                                        $discountTotal = round($currency->rate * $discountAmount, 2);
                                    }
                                    else if($discountType == "percent"){
                                        $discountTotal = $orderitem->totalAmount['total'] * $discountAmount / 100;
                                    }
                                    $orderitem->discounted = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($promocode->free_delivery)
            $discountTotal += $deliveryCharge['total'];

        return $discountTotal;
    }

    public static function generateInvoiceNo(){
        $invoice_prefix    = HelperSettings::get('invoice_prefix');
        $invoice_no_length = HelperSettings::get('invoice_no_length');

        if (!$invoice_prefix) {
            $invoice_prefix = 'IWB';
        }

        if (!$invoice_no_length) {
            $invoice_no_length = "6";
        }

        $latest_order = Order::orderBy('invoice_no', 'DESC')->first();

        // $today = Carbon::now();
        // $current_prefix = $today->format('y');

        if($latest_order) {
            if ($latest_order->invoice_no){
                $latest_invoice_no = $latest_order->invoice_no;
                $accumulate_no     = (int) substr($latest_invoice_no, strilen($invoice_prefix));

                $new_no = null;
                while(!$new_no){
                    $accumulate_no = $accumulate_no + 1;
                    $new_no = str_pad($accumulate_no, $invoice_no_length, '0', STR_PAD_LEFT);
                    $exists = Order::where('invoice_no', $invoice_prefix . $new_no);

                    if ($exists) {
                        $new_no = null;
                    }
                }

                $invoice_no = $invoice_prefix . $new_no;
            } else {
                $invoice_no = $invoice_prefix . str_pad('1', $invoice_no_length, '0', STR_PAD_LEFT);
            }
        } else {
            $invoice_no =  $invoice_prefix . str_pad('1', $invoice_no_length, '0', STR_PAD_LEFT);
        }

        return $invoice_no;
    }
}
