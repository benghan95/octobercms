<?php namespace IG\Transact\Models;

use Model;

/**
 * OrderItemStock Model
 */
class OrderItemStock extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_order_item_stocks';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'orderitem' => ['IG\Transact\Models\OrderItem', 'key' => 'orderitem_id'],
        'option'    => ['IG\Commerce\Models\ProductOption', 'key' => 'option_id'],
        'stock'     => ['IG\Commerce\Models\Stock', 'key' => 'stock_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
