<?php namespace IG\Transact\Models;

use Model;
use IG\Transact\Classes\Hexing;
use IG\Core\Models\Currency;

/**
 * OrderItem Model
 */
class OrderItem extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'options'    => ['IG\Transact\Models\OrderItemStock', 'key' => 'orderitem_id'],
        'itemstocks' => ['IG\Transact\Models\OrderItemStock', 'key' => 'orderitem_id'],
    ];
    public $belongsTo = [
        'order'    => ['IG\Transact\Models\Order', 'key' => 'order_id'],
        'product'  => ['IG\Commerce\Models\Product', 'key' => 'product_id'],
        'pricing'  => ['IG\Commerce\Models\Pricing', 'key' => 'pricing_id'],
        'currency' => ['IG\Core\Models\Currency', 'key' => 'currency_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function afterCreate() {
        $this->hexhash = Hexing::hex($this->id);
        $this->save();
    }

    public function toJSONArray(){
        $this->product               = $this->product;
        $this->currency              = $this->currency;
        $this->itemstocks            = $this->itemstocks;
        $this->options               = $this->options;
        $this->product->displayImage = $this->product->displayImage;

        return $this;
    }

    public function getTotalAmountAttribute(){
        $product = $this->product;

        if ($product == null){
            $orderitem = OrderItem::find($this->id);
            $orderitem->delete();
            return;
        }

        $currency = $this->currency;
        $total    = $this->quantity * $this->unit_price;

        if ($total <= 0){
            $orderitem = OrderItem::find($this->id);
            $orderitem->delete();
            return;
        }

        return [
            'currency' => $currency,
            'total' => number_format($this->quantity * $this->unit_price, 2),
        ];
    }

    public function getDisplayPriceAttribute(){
        $pricing = [];

        $pricing['currency'] = $this->currency->unit;
        $pricing['hexhash']  = $this->pricing->hexhash;
        $pricing['rrp']      = round($this->pricing->rrp * $this->currency->rate);
        $pricing['price']    = $this->unit_price;

        return $pricing;
    }
}
