<?php namespace IG\Transact\Models;

use Model;
use IG\Commerce\Models\Category;

/**
 * Promocode Model
 */
class Promocode extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_promocodes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $jsonable = [
        'specific_emails',
        'specific_categories',
        'specific_products',
    ];

    public function beforeSave() {
        $this->created_by = \BackendAuth::getUser()->id;
        if ($this->start_date)
            $this->start_date = \Carbon\Carbon::parse($this->start_date)->setTime(0, 0, 0);

        if ($this->end_date)
            $this->end_date = \Carbon\Carbon::parse($this->end_date)->setTime(0, 0, 0);
    }

    public function getSpecificCategoriesOptions($value, $formData)
    {
        $categories = Category::get(['title','slug'])->toArray();

        $array = [];

        foreach($categories as $category){
            $array[$category['slug']] = $category['title'];
        }

        return $array;
    }
}
