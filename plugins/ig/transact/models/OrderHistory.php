<?php namespace IG\Transact\Models;

use Model;

/**
 * OrderHistory Model
 */
class OrderHistory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_order_histories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'status' => ['IG\Core\Models\OrderStatus', 'key' => 'status_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    public $morphedByMany = [
        'order' => ['IG\Transact\Models\Order', 'name' => 'historable', 'table' => 'ig_transact_order_historable']
    ];
}
