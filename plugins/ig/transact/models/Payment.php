<?php namespace IG\Transact\Models;

use Model;
use IG\Transact\Models\Order;

/**
 * Payment Model
 */
class Payment extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_transact_payments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'ipay' => ['IG\Transact\Models\Ipay', 'key' => 'payment_id'],
    ];
    public $hasMany = [];
    public $belongsTo = [
        'currency' => ['IG\Core\Models\Currency', 'key' => 'currency_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function justCreate($order){
        $order = Order::find($order['id']);
        $calculated = Order::grandTotalByOrders($order->id);

        $payment = new Payment();
        $payment->order_id    = $order->id;
        $payment->subtotal    = $calculated['subtotal']['total'];
        $payment->tax         = $calculated['tax']['total'];
        $payment->shipping    = $calculated['delivery_charge']['total'];
        $payment->discount    = $calculated['discount']['total'];
        $payment->total       = $calculated['grand']['total'];
        $payment->weight      = $order->totalWeight;
        $payment->currency_id = $calculated['currency']['id'];
        $payment->error       = "-";
        $payment->success     = true;
        $payment->save();

        return $payment;
    }
}
