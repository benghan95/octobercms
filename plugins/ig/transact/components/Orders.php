<?php namespace IG\Transact\Components;

use IG\Commerce\Classes\IG;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\ProductOption;
use IG\Transact\Models\Order;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;
use RainLab\User\Facades\Auth;

class Orders extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Orders Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        parent::onRun();

        if(!Auth::check())
            return;

        $user = Auth::getUser();

        $ordersOwned = Order::with(
            'orderitems',
            'orderitems.product',
            'orderitems.productoption',
            'payments'
            )
            ->where('user_id', $user->id)
            ->has('orderitems')
            ->has('payments')
            ->orderBy('id', 'desc')
            ->get();

        $this->page['orders'] = $ordersOwned;
    }
}
