<?php namespace IG\Transact\Components;

use Barryvdh\DomPDF\Facade as PDFStatic;
use IG\Commerce\Classes\IG;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\ProductOption;
use IG\Transact\Models\Order;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Dompdf\Css\Stylesheet;
use Dompdf\Dompdf;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Facades\Auth;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Redirect;

class Receipt extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Receipt Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        parent::onRun();

        $hexHash = $this->param('hexhash');
        $order = Order::where('hexhash', $hexHash)
                        ->has('payment')
                        ->first();

        if (!$order) {
            Flash::info('Your order not found.');
            return Redirect::to('/');
        }

        $this->page['order'] = $order;

        $this->page['calculated'] = Order::grandTotalByOrders($order->id);

        $productsRecommended = Product::active()
            ->orderBy('sale_count', 'desc')
            ->limit(4)
            ->get();

        $this->page['products_recommended'] = $productsRecommended;
    }

//    public function onDownloadReceipt(){
//        $hexHash = $this->param('hexhash');
//
//        return PDFStatic::loadHTML('<h1>Test</h1>')->stream('download.pdf');
//
//        return PDFStatic::loadFile(Config::get('app.url').'/checkout/thanks/'.$hexHash)
//            ->save(realpath(base_path()).'/storage/temp/public/'.$hexHash.'.pdf')
//            ->stream('download.pdf');
//    }
}
