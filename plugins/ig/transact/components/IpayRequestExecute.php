<?php namespace IG\Transact\Components;

use IG\Commerce\Classes\IG;
use IG\Transact\Models\Ipay;
use IG\Transact\Models\Order;
use IG\Transact\Models\Settings;
use IG\Core\Models\Settings as CoreSettings;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Support\Facades\Flash;

class IpayRequestExecute extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IpayRequestExecute Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'response' => [
                'title'       => 'Response Page for iPay88',
                'description' => 'The page that iPay88 will redirect to after payment/failure of payment',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'checkin' => [
                'title'       => 'Server Checkin for iPay88',
                'description' => 'The page that iPay88 use to further verify the payment status',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function getResponseOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getCheckinOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        parent::onRun();

        $order = Order::get_existing_order_or_createnew();
        $calculated = Order::grandTotalByOrders($order->id);

        $products_list = $this->updateOrderItems();

        $this->page['merchant_code'] = Settings::get('ipay_merchantcode');
        $this->page['product_desc']  = $products_list;
        $this->page['refno']         = $order->id;
        $this->page['grandtotal']    = $calculated['grand']['formatted'];
        $this->page['currency']      = $calculated['currency']['unit'];
        $this->page['signature']     = Ipay::signature($order, $calculated);

        $isProd = CoreSettings::get('isProd');

        if (Settings::get('ipay_entryurl')) {
            $this->page['ipay_entry'] = Settings::get('ipay_entryurl');
        } else {
            if ($isProd)
                $this->page['ipay_entry'] = 'https://www.mobile88.com/ePayment/entry.asp';
            else
                $this->page['ipay_entry'] = 'http://1millionlogo.com/entry.php';
        }
        $this->page['return'] = $this->controller->pageUrl($this->property('response')).'/'.$order->hexhash;
        $this->page['backend_check'] = $this->controller->pageUrl($this->property('checkin'));
    }

    public function onUpdateAmount(){
        $order               = Order::get_existing_order_or_createnew();
        $calculated          = Order::grandTotalByOrders($order->id);
        $lockedPriceSubtotal = IG::lockPriceInOrderItem($order);

        if($lockedPriceSubtotal != $calculated['subtotal']['total']){
            Flash::warning('Locked subtotal is different');
            Log::warning('Locked subtotal is different'. \GuzzleHttp\json_encode($calculated).' : '. $lockedPriceSubtotal . ' for order:'. $order->id);
            return Redirect::refresh();
        }

        $products_list = $this->updateOrderItems();

        return [
            'products_list' => $products_list,
            'grandtotal'    => $calculated['grand']['formatted'],
            'signature'     => Ipay::signature($order, $calculated)
        ];
    }

    private function updateOrderItems(){
        $order = Order::get_existing_order_or_createnew();

        $products_list = '';
        $counter = 1;

        foreach ($order->orderitems as $orderitem) {
            $product = $orderitem->product;

            if ($product){
                if ($product->product_type == 'BUNDLE'){
                    $products_list .= $product->title . ' (';
                    $count = 1;
                    foreach ($orderitem->itemstocks as $item){
                        if ($item->option){
                            if ($item->stock->color) {
                                $products_list .= $item->option->title . ' - ' . $item->stock->color->title;
                            } else {
                                $products_list .= $item->option->title;
                            }
                        } else {
                            if ($item->stock->color) {
                                $products_list .= $stock->color->title;
                            }
                        }


                        if ($count < count($orderitem->itemstocks)){
                            $products_list .=  ', ';
                        }

                        $count++;
                    }
                    $products_list .= ') x '. $orderitem->quantity;
                }
                else if ($product->product_type == 'OPTION'){
                    $products_list .= $product->title . ' (' . $orderitem->itemstocks[0]->option->title;
                    if ($orderitem->itemstocks[0]->stock->color){
                        $products_list .= ' - ' . $orderitem->itemstocks[0]->stock->color->title;
                    }
                    $products_list .= ') x '. $orderitem->quantity;
                }
                else{
                    $products_list .= $product->title . ' x '. $orderitem->quantity;
                }
            }

            if ($counter < count($order->orderitems)){
                $products_list .=  ', ';
            }

            $counter++;
        }

        return $products_list;
    }
}
