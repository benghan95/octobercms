<?php namespace IG\Transact\Components;

use Carbon\Carbon;
use IG\Commerce\Classes\IG;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\ProductOption;
use IG\Commerce\Models\Stock;
use IG\Commerce\Models\Settings;
use IG\Core\Models\Currency;
use IG\Core\Models\Country;
use IG\Core\Models\Delivery;
use IG\Core\Models\DeliveryLocation;
use IG\Core\Models\State;
use IG\Transact\Classes\Hexing;
use IG\Transact\Models\Order;
use IG\Transact\Models\OrderItem;
use IG\Transact\Models\Promocode;
use IG\User\Models\Address;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use PhpParser\Error;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;

class Checkout extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Checkout Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (Auth::check()) {
            $user = Auth::getUser();
            if (!$user->name) {
                return Redirect::to('my-account/profile')->with('message', 'Please provide us with your name.');
            }
            if (!$user->name || !$user->email) {
                return Redirect::to('my-account/profile')->with('message', 'Please provide us with your email.');
            }
        }

        parent::onRun();

        $order = IG::retrieve();
        $order->delivery_id = null;
        $order->save();

        $allAddresses      = [];
        $shippingAddresses = [];
        $billingAddresses  = [];
        if(Auth::check()){
            $user = Auth::getUser();
            $allAddresses = Address::where('user_id', $user->id)->get();

            $shippingAddresses = Address::where('user_id', $user->id)
                                      ->where('type', 'shipping')
                                      ->get();

            $billingAddresses = Address::where('user_id', $user->id)
                                      ->where('type', 'billing')
                                      ->get();

            $shippingAddress = Address::where('user_id', $user->id)
                                      ->where('type', 'shipping')
                                      ->first();

            $billingAddress = Address::where('user_id', $user->id)
                                      ->where('type', 'billing')
                                      ->first();


            if ($shippingAddress) {
                $order->shipping_id = $shippingAddress->id;
                if ($shippingAddress->deliveryLocation)
                    $order->delivery_id = $shippingAddress->deliveryLocation->id;
                else
                    $order->delivery_id = null;

                $order->save();

                $this->page['shipping_address'] = $shippingAddress;
            }
            else{
                $this->page['shipping_address'] = null;
            }

            if ($billingAddress) {
                $order->billing_id = $billingAddress->id;
                $order->save();

                $this->page['billing_address'] = $billingAddress;
            }
            else{
                $this->page['billing_address'] = null;
            }
        }
        else{
            $orderTemp = $order->temp;
            if(isset($orderTemp['address_id'])){
                $address = Address::where('id', $orderTemp['address_id'])->first();

                if ($address) {
                    $order->shipping_id = $address->id;
                    if ($address->deliveryLocation)
                        $order->delivery_id = $address->deliveryLocation->id;
                    else
                        $order->delivery_id = null;

                    $order->save();

                    $this->page['shipping_address'] = $address;
                }
                else{
                    $this->page['shipping_address'] = null;
                }

                array_push($allAddresses, $address);
            }

            if(isset($orderTemp['billing_id'])){
                $address = Address::where('id', $orderTemp['billing_id'])->first();

                if ($address) {
                    $order->billing_id = $address->id;
                    $order->save();

                    $this->page['billing_address'] = $address;
                }
                else{
                    $this->page['billing_address'] = null;
                }

                if($orderTemp['address_id'] != $orderTemp['billing_id'])
                    array_push($allAddresses, $address);
            }
        }

        $this->page['order'] = $order;
        $this->page['orderitems'] = $this->concerningOrderItems($order);
        $this->page['calculated'] = Order::grandTotalByOrders($order->id);
        $deliveries = Delivery::all();
        $this->page['deliveries'] = $deliveries;

        $deliverySelected = $deliveries->first();

        if($order->delivery){
            $deliverySelected = $order->delivery;
        }

        $this->page['default_delivery']   = $deliverySelected;
        $this->page['billing_countries']  = Country::all();
        $this->page['countries']          = Country::hasDelivery()->get();
        $this->page['states']             = Country::hasDelivery()->first()->states;
        $this->page['all_addresses']      = $allAddresses;
        $this->page['shipping_addresses'] = $shippingAddresses;
        $this->page['billing_addresses']  = $billingAddresses;
    }

    public function onSubmit(){
        $post = post();

        $order = Order::get_existing_order_or_createnew();

        $shipping = [];
        // $billing = [];
        $user = null;

        if(Auth::check()){
            $user = Auth::getUser();
        }
        else{
            $user = User::findByEmail($post['guest_email']);
            if($user)
                throw new ApplicationException('A user has been found with this email, please login and checkout');

            $guestData = [
                'email'                 => $post['guest_email'],
                'name'                  => $post['guest_name'],
                'password'              => '123456',
                'password_confirmation' => '123456',
                'contact_no'            => $post['guest_telephone']
            ];

            $userRules = [
                'email'      => 'required|email',
                'name'       => 'required|between:2,255',
                'contact_no' => 'required|digits_between:8,15'
            ];

            $validation = Validator::make($guestData, $userRules);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            $order->temp = $guestData;
            $order->save();
        }

        if(array_key_exists('address_id', $post)){
            if($post['address_id'] == "-1"){
                unset($post['address_id']);

                if(isset($post['state_shipping']) && isset($post['postcode_shipping'])) {
                    $invalidCode = $this->validatePostcode($post['state_shipping'], $post['postcode_shipping']);

                    if ($invalidCode) {
                        throw new ApplicationException('Invalid postcode on shipping address, please enter the correct postcode based on the state selected');
                        return;
                    }
                }

                $shipping = [
                    'name'       => $post['name_shipping'],
                    'type'       => 'shipping',
                    'company'    => $post['companyname_shipping'],
                    'telephone'  => $post['phone_shipping'],
                    'city'       => $post['city_shipping'],
                    'country_id' => $post['country_shipping'],
                    'state_id'   => $post['state_shipping'],
                    'postcode'   => $post['postcode_shipping'],
                    'address_1'  => $post['address1_shipping'],
                    'address_2'  => $post['address2_shipping'],
                ];

                $newShipping = new Address();
                $rules = $newShipping->rules;

                $messages = [
                    'address_1.required'  => 'Please enter your shipping address',
                    'country_id.required' => 'Please select a shipping country',
                    'state_id.required'   => 'Please select a shipping state',
                ];

                $validation = Validator::make($shipping, $rules, $messages);
                if ($validation->fails()) {
                    throw new ValidationException($validation);
                }

                $newShipping->fill($shipping);
                $newShipping->save();

                $newShipping->hexhash = Hexing::hex($newShipping->id);
                $newShipping->save();

                $order->shipping_id = $newShipping->id;
                $order->save();

                if($user){
                    $newShipping->user_id = $user->id;
                    $newShipping->save();
                } else {
                    $temp = $order->temp;
                    $temp['shipping_id'] = $newShipping->id;

                    $order->temp = $temp;
                    $order->save();
                }
            } else {
                $address = Address::where('hexhash', $post['address_id'])->first();
                if($address == null)
                    throw new ApplicationException('Address invalid.');

                if ($post['shipping_editted'] && $post['shipping_editted'] == $post['address_id']) {

                    if(isset($post['state_shipping']) && isset($post['postcode_shipping'])) {
                        $invalidCode = $this->validatePostcode($post['state_shipping'], $post['postcode_shipping']);

                        if ($invalidCode) {
                            throw new ApplicationException('Invalid postcode on shipping address, please enter the correct postcode based on the state selected');
                            return;
                        }
                    }

                    $address->name       = $post['name_shipping'];
                    $address->company    = $post['companyname_shipping'];
                    $address->telephone  = $post['phone_shipping'];
                    $address->city       = $post['city_shipping'];
                    $address->country_id = $post['country_shipping'];
                    $address->state_id   = $post['state_shipping'];
                    $address->postcode   = $post['postcode_shipping'];
                    $address->address_1  = $post['address1_shipping'];
                    $address->address_2  = $post['address2_shipping'];
                    $address->save();
                }

                $order->shipping_id = $address->id;
                $order->save();

                if(!$user){
                    $temp = $order->temp;
                    $temp['shipping_id'] = $address->id;

                    $order->temp = $temp;
                    $order->save();
                }
            }
        }

        if (array_key_exists('different_billing', $post)){
            if($post['billing_id'] == "-1"){
                unset($post['billing_id']);

                if(isset($post['state_billing']) && isset($post['postcode_billing'])) {
                    $invalidCode = $this->validatePostcode($post['state_billing'], $post['postcode_billing']);

                    if ($invalidCode) {
                        throw new ApplicationException('Invalid postcode on billing address, please enter the correct postcode based on the state selected');
                        return;
                    }
                }

                $billing = [
                    'name'       => $post['name_billing'],
                    'type'       => 'billing',
                    'company'    => $post['companyname_billing'],
                    'telephone'  => $post['phone_billing'],
                    'city'       => $post['city_billing'],
                    'country_id' => $post['country_billing'],
                    'state_id'   => $post['state_billing'],
                    'postcode'   => $post['postcode_billing'],
                    'address_1'  => $post['address1_billing'],
                    'address_2'  => $post['address2_billing'],
                ];

                $newBilling = new Address();
                $rules = $newBilling->rules;

                $messages = [
                    'address_1.required'  => 'Please enter your billing address',
                    'country_id.required' => 'Please select a shipping country',
                    'state_id.required'   => 'Please select a shipping state',
                ];

                $validation = Validator::make($billing, $rules, $messages);
                if ($validation->fails()) {
                    throw new ValidationException($validation);
                }

                $newBilling->fill($billing);
                $newBilling->save();

                $newBilling->hexhash = Hexing::hex($newBilling->id);
                $newBilling->save();

                $order->billing_id = $newBilling->id;
                $order->save();

                if($user) {
                    $newBilling->user_id = $user->id;
                    $newBilling->save();
                } else {
                    $temp = $order->temp;
                    $temp['billing_id'] = $newBilling->id;

                    $order->temp = $temp;
                    $order->save();
                }
            } else {
                $address = Address::where('hexhash', $post['billing_id'])->first();
                if($address == null)
                    throw new ApplicationException('Billing Address invalid.');

                if ($post['billing_editted'] && $post['billing_editted'] == $post['billing_id']) {
                    if(isset($post['state_billing']) && isset($post['postcode_billing'])) {
                        $invalidCode = $this->validatePostcode($post['state_billing'], $post['postcode_billing']);

                        if ($invalidCode) {
                            throw new ApplicationException('Invalid postcode on billing address, please enter the correct postcode based on the state selected');
                            return;
                        }
                    }

                    $address->name       = $post['name_billing'];
                    $address->company    = $post['companyname_billing'];
                    $address->telephone  = $post['phone_billing'];
                    $address->city       = $post['city_billing'];
                    $address->country_id = $post['country_billing'];
                    $address->state_id   = $post['state_billing'];
                    $address->postcode   = $post['postcode_billing'];
                    $address->address_1  = $post['address1_billing'];
                    $address->address_2  = $post['address2_billing'];
                    $address->save();
                }

                $order->billing_id = $address->id;
                $order->save();

                if(!$user) {
                    $temp = $order->temp;
                    $temp['billing_id'] = $address->id;

                    $order->temp = $temp;
                    $order->save();
                }
            }
        } else {
            $order->billing_id = $order->shipping_id;
            $order->save();
        }

        if (isset($post['delivery_location'])) {
            $delivery = DeliveryLocation::where('id', $post['delivery_location'])->first();
            if($delivery == null)
                throw new ApplicationException('No such delivery method exists');

            $order->delivery_id = $delivery->id;
            $order->save();
        }

        if(!$user){
            Session::put('guest-checkout', $order->id);
            Session::forget('guest');
        }

        IG::lockPriceInOrderItem($order);

        return [
            'status' => 'success',
            'order' => $order->hexhash
        ];
    }

    public function validatePostcode($state, $postal) {
        $invalidCode = false;
        $postal      = intval($postal);
        switch ($state) {
            // Johor
            case '1971':
                if (!($postal >= 79000 && $postal <= 86900))
                    $invalidCode = true;
                break;
            // Kedah
            case '1972':
                if (!($postal >= 5000 && $postal <= 9810))
                    $invalidCode = true;
                break;
            // Kelantan
            case '1973':
                if (!($postal >= 15000 && $postal <= 18500))
                    $invalidCode = true;
                break;
            // Labuan
            case '1974':
                if (!($postal >= 87000 && $postal <= 87033))
                $invalidCode = true;
                break;
            // Melaka
            case '1975':
                if (!($postal >= 75000 && $postal <= 78309))
                    $invalidCode = true;
                break;
            // Negeri Sembilan
            case '1976':
                if (!($postal >= 70000 && $postal <= 73509))
                    $invalidCode = true;
                break;
            // Pahang
            case '1977':
                if (!(($postal >= 25000 && $postal <= 28800) || ($postal >= 39000 && $postal <= 39200) || $postal == 49000 || $postal == 69000))
                    $invalidCode = true;
                break;
            // Perak
            case '1978':
                if (!($postal >= 30000 && $postal <= 36810))
                    $invalidCode = true;
                break;
            // Perlis
            case '1979':
                if (!($postal >= 1000 && $postal <= 2999))
                    $invalidCode = true;
                break;
            // Pulau Pinang
            case '1980':
                if (!($postal >= 10000 && $postal <= 14400))
                    $invalidCode = true;
                break;
            // Sabah
            case '1981':
                if (!($postal >= 88000 && $postal <= 91309))
                    $invalidCode = true;
                break;
            // Sarawak
            case '1982':
                if (!($postal >= 93000 && $postal <= 98859))
                    $invalidCode = true;
                break;
            // Selangor
            case '1983':
                if (!(($postal >= 40000 && $postal <= 48300) || ($postal >= 63000 && $postal <= 68100)))
                    $invalidCode = true;
                break;
            // Terengganu
            case '1984':
                if (!($postal >= 20000 && $postal <= 24300))
                    $invalidCode = true;
                break;
            // Wilayah Persekutuan
            case '1985':
                if (!($postal >= 50000 && $postal <= 60000))
                    $invalidCode = true;
                break;
            // Putrajaya
            case '4047':
                if (!($postal >= 62000 && $postal <= 62988))
                    $invalidCode = true;
                break;
        }

        return $invalidCode;
    }

    public function onTriggerDiscountType(){
        $order = Order::get_existing_order_or_createnew();

        $order->promocode_id = null;
        $order->save();

        return [
            'result' => 'success',
            'calculated' => $this->concerningPrice($order->id),
        ];
    }

    public function onApplyPromocode(){
        $order = Order::get_existing_order_or_createnew();
        $post  = post();
        $currency = Currency::getUserCurrency();

        if($post['promocode']){
            $promocode = Promocode::where('code', $post['promocode'])->first();

            if($promocode){

                if(!$promocode->status){
                    return $this->generateDiscountError('The promocode is invalid.');
                }

                if($promocode->start_date != NULL){
                    if(Carbon::now('Asia/Kuala_Lumpur') <= $promocode->start_date){
                        return $this->generateDiscountError('The promocode is not available yet.');
                    }
                }

                if($promocode->end_date != NULL){
                    if(Carbon::now('Asia/Kuala_Lumpur') >= $promocode->end_date){
                        return $this->generateDiscountError('The promocode has expired.');
                    }
                }

                if($promocode->usage_limit_order != NULL){
                    $count = count(Order::where('promocode_id', $promocode->id)->get());

                    if($promocode->usage_limit_order <= $count){
                        return $this->generateDiscountError('The promocode has fully redeemed.');
                    }
                }

                if($promocode->usage_limit_user != NULL){
                    $user = Auth::getUser();

                    if($user){
                        $count = count(Order::where('promocode_id', $promocode->id)
                                            ->where('user_id', $user->id)
                                            ->get());

                        if($promocode->usage_limit_user <= $count){
                            return $this->generateDiscountError('You have fully redeemed the promocode.');
                        }
                    }
                    else{
                        return $this->generateDiscountError('Please login to use the promocode.');
                    }
                }

                if($promocode->specific_emails){
                    $user = Auth::getUser();

                    if($user){
                        $user_valid = false;

                        foreach ($promocode->specific_emails as $email){
                            if($user->email == $email){
                                $user_valid = true;
                                break;
                            }
                        }

                        if(!$user_valid){
                            return $this->generateDiscountError('The promocode is invalid.');
                        }
                    }
                    else{
                        return $this->generateDiscountError('Please login to use the promocode.');
                    }
                }

                $promocode_valid = false;
                $specific_checking = false;

                if($promocode->specific_categories && !empty($promocode->specific_categories)){
                    $specific_checking = true;

                    foreach ($promocode->specific_categories as $specific_category){
                        $category = Category::where('slug', $specific_category)->first();

                        if ($category){
                            foreach($order->orderitems as $orderitem){
                                $product = $orderitem->product;

                                if ($product && $product->category) {
                                    if ($product->category->id == $category->id){
                                        $promocode_valid = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if($specific_checking && !$promocode_valid){
                    return $this->generateDiscountError('The promocode is not valid for any of your products.');
                }

                $subtotal = Order::subTotalByOrders($order->id);

                if ($subtotal['total'] < round($promocode->min_purchase * $currency->rate, 2)){
                    return $this->generateDiscountError('You must purchase up to ' . $currency->unit . round($promocode->min_purchase * $currency->rate, 2) . ' in order to use this promocode.');
                }

                $order->promocode_id = $promocode->id;
                $order->save();

                $calculated = $this->concerningPrice($order->id);

                if ($calculated['discount']['total'] == 0){
                    $order->promocode_id = NULL;
                    $order->save();

                    return $this->generateDiscountError('The promocode is invalid.');
                }
                else{
                    return [
                        'result'       => 'success',
                        'promocode_id' => $promocode->id,
                        'code'   => $promocode->code,
                        'calculated'   => $this->concerningPrice($order->id),
                    ];
                }
            }
            else{
                return $this->generateDiscountError('The promocode is invalid.');
            }
        }
        else{
            return $this->generateDiscountError('Please enter a promocode.');
        }
    }

    public function onUpdateOrderItem(){
        $hexhash = post('orderitem');

        $orderitem = OrderItem::where('hexhash', $hexhash)->first();

        if($orderitem == null)
            throw new ApplicationException('Item not found, please try again later.');

        $order = $orderitem->order;
        $product = $orderitem->product;

        $quantity = post('quantity');

        if ($quantity <= 0)
            throw new ApplicationException('Quantity must be equal or more than 1 to add to cart');

        foreach ($orderitem->itemstocks as $item){
            $stock = $item->stock;
            if ($stock->in < $quantity)
                throw new ApplicationException('Insufficient stock, please try to reduce the quantity.');
        }

        $orderitem->quantity = $quantity;
        $orderitem->save();

        foreach ($order->orderitems as $item){
            $item->toJSONArray();
        }

        return [
            'status' => 'success',
            'results' => [
                'order'     => $order,
                'itemCount' => count($order->orderitems),
            ],
            'orderitem' => $orderitem,
            'orderitems' => $this->concerningOrderItems($order),
            'calculated' => $this->concerningPrice($order->id),
        ];
    }

    public function onCountryChange(){
        $countryId = post('country');
        $isShipping = post('isShipping');

        $order = IG::retrieve();
        if ($isShipping){
            $order->delivery_id = null;
            $order->save();
        }

        if ($countryId){
            $country = Country::find($countryId);

            if ($country){
                $states = $country->states->sortBy('name')->toArray();

                return [
                    'default'    => '-- Select a state --',
                    'states'     => $states,
                    'calculated' => $this->concerningPrice($order->id),
                ];
            }
            else{
                return [
                    'default'    => '-- Select a valid country --',
                    'calculated' => $this->concerningPrice($order->id),
                ];
            }
        }
        else{
            return [
                'default'    => '-- Select a country first --',
                'calculated' => $this->concerningPrice($order->id),
            ];
        }
    }

    public function onStateChange(){
        $order = IG::retrieve();

        $countryId  = post('country');
        $stateId    = post('state');
        $isShipping = post('isShipping');

        if ($isShipping){
            if ($countryId && $stateId){
                $location = DeliveryLocation::where('country_id', $countryId)
                                            ->where('state_id', $stateId)
                                            ->first();

                if ($location){
                    $order->delivery_id = $location->id;
                    $order->save();
                }
                else{
                    $order->delivery_id = null;
                    $order->save();
                }
            }
            else{
                $order->delivery_id = null;
                $order->save();
            }
        }

        return [
            'calculated' => $this->concerningPrice($order->id),
            'delivery'   => $order->delivery_id,
        ];
    }

    public function onAddressChange(){
        $order = IG::retrieve();

        $hexhash = post('option');
        $status = "success";
        $message = null;

        if ($hexhash == "-1"){
            $order->delivery_id = null;
            $order->save();
        }
        else{
            $address = Address::where('hexhash', $hexhash)->first();

            if($address){
                if ($address->state && $address->country){
                    if ($address->deliveryLocation){
                        $order->delivery_id = $address->deliveryLocation->id;
                        $order->save();
                    }
                    else{
                        $order->delivery_id = null;
                        $order->save();
                        $message = "Shipping does not support on your location, please use a new address.";
                        $status = "error";
                    }
                }
                else{
                    $address->delete();
                    $order->delivery_id = null;
                    $order->save();
                    $message = "Address missing Country or State! Please use a new address.";
                    $status = "error";
                }
            }
            else{
                $message = "Address not found! Please use a new address.";
                $status = "error";
                $order->delivery_id = null;
                $order->save();
            }
        }

        return [
            'status' => $status,
            'message' => $message,
            'calculated' => $this->concerningPrice($order->id),
            'delivery'   => $order->delivery_id,
        ];
    }

    public function onRemoveAddress(){
        $order = IG::retrieve();

        $hexhash    = post('option');
        $isShipping = post('isShipping');
        $status     = "success";
        $message    = null;

        if ($isShipping){
            $order->shipping_id = null;
            $order->delivery_id = null;
            $order->save();
        } else {
            $order->billing_id = null;
            $order->save();
        }

        $address = Address::where('hexhash', $hexhash)->first();

        if ($address) {
            $address->delete();

            return [
                'status' => 'success',
                'message' => 'Address has been deleted.',
                'calculated' => $this->concerningPrice($order->id),
            ];
        } else {
            return [
                'status' => 'error',
                'message' => 'Address not found!',
                'calculated' => $this->concerningPrice($order->id),
            ];
        }
    }

    private function concerningOrderItems($order){
        return $order->orderitems;
    }

    private function concerningPrice($orderid){
        return Order::grandTotalByOrders($orderid);
    }

    private function generateDiscountError($error_message){
        $order = IG::retrieve();

        $order->promocode_id = null;
        $order->save();

        return [
            'result' => 'error',
            'error_message' => $error_message,
            'calculated' => $this->concerningPrice($order->id),
        ];
    }
}
