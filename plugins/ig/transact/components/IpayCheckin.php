<?php namespace IG\Transact\Components;

use Carbon\Carbon;
use IG\Core\Models\OrderStatus;
use IG\Transact\Classes\GuestProcessing;
use IG\Transact\Classes\SaleCount;
use IG\Transact\Models\Ipay;
use IG\Transact\Models\Order;
use IG\Transact\Models\Settings;
use IG\Core\Models\Settings as CoreSettings;
use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class IpayCheckin extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IpayCheckin Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $details = [
            'MerchantCode' => post('MerchantCode'),
            'PaymentId'    => post('PaymentId'),
            'RefNo'        => post('RefNo'),
            'Amount'       => post('Amount'),
            'Currency'     => post('Currency'),
            'Remark'       => post('Remark'),
            'TransId'      => post('TransId'),
            'AuthCode'     => post('AuthCode'),
            'Status'       => post('Status'),
            'ErrDesc'      => post('ErrDesc'),
            'Signature'    => post('Signature'),
            'CCName'       => post('CCName'),
            'CCNo'         => post('CCNo'),
            'S_bankname'   => post('S_bankname'),
            'S_country'    => post('S_country'),
        ];

        $order = Order::where('id', post('RefNo'))->first();
        if($order == null){
            Log::error("Existing Order Not Found for:" . post('RefNo'));
            die();
        }

        $paid = Ipay::paid(post('RefNo'))->first();
        if ($paid) { return; }

        $calculated = Order::grandTotalByOrders($order->id);

        if($calculated['grand']['formatted'] != $details['Amount']){
            Log::error("Existing Order for: " . post('RefNo') . ' has different amount than before ' . $calculated['currency']['unit'] . $calculated['grand']['total'] . '; Ping payment amount: ' . $details['Currency'] . $details['Amount'] . ', terminating auto-ping-processing.');
            die();
        }

        $requery = IPay::requery($details['MerchantCode'], $details['RefNo'], $details['Amount']);
        if($requery != "00"){
            Log::error("IPay88 Response on ". post('RefNo'). ': ' . $requery . ' - '. post('ErrDesc'));
            Event::fire('order.history.status.change', [OrderStatus::ipay_failed()['id'], $order]);
            die();
        }

        $verifyResponse = IPay::verifyResponse($order, $calculated, $details);
        if(!$verifyResponse){
            Log::error("Payment fail verification on " . post('RefNo') . ': ' . post('ErrDesc'));
            Event::fire('order.history.status.change', [OrderStatus::ipay_signature_unmatched()['id'], $order]);
            die();
        }

        if($order->user == null)
            GuestProcessing::proceed($order);

        Ipay::justPay($order, $details);
        SaleCount::proceed($order);

        $order->invoice_no  = Order::generateInvoiceNo();
        $order->invoiced_at = Carbon::now();
        $order->status_id   = OrderStatus::processing()['id'];
        $order->save();

        Event::fire('order.history.status.change', [OrderStatus::ipay_successful()['id'], $order]);
        Event::fire('order.history.status.change', [OrderStatus::order_received()['id'], $order]);
    }
}
