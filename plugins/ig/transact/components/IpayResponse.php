<?php namespace IG\Transact\Components;

/**
 *
 *
 * If logged in, then can show the resonse
 * If !logged in, will need to check for stored session to verify that this is indeed the user who is paying
 *
 * If session is lost, the transaction will need to be halted
 * We store orderid in session for response verification
 *
 *
 */

use Request;
use Event;
use Lang;
use Log;
use Mail;
use Redirect;
use Session;
use Carbon\Carbon;
use IG\Core\Models\OrderStatus;
use IG\Transact\Classes\GuestProcessing;
use IG\Transact\Classes\SaleCount;
use IG\Transact\Models\Ipay;
use IG\Transact\Models\Order;
use IG\Transact\Models\Settings;
use IG\Core\Models\Settings as CoreSettings;
use IG\Helper\Models\Settings as HelperSettings;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use October\Rain\Support\Facades\Flash;
use RainLab\User\Facades\Auth;

class IpayResponse extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IpayResponse Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'receipt' => [
                'title'       => 'Receipt page',
                'description' => 'Final page to show, usually it is receipt page',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'fail' => [
                'title'       => 'Fail page',
                'description' => 'Page to redirect to if payment through iPay fails',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function getReceiptOptions(){
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getFailOptions(){
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun(){
        $details = [
            'MerchantCode' => post('MerchantCode'),
            'PaymentId'    => post('PaymentId'),
            'RefNo'        => post('RefNo'),
            'Amount'       => post('Amount'),
            'Currency'     => post('Currency'),
            'Remark'       => post('Remark'),
            'TransId'      => post('TransId'),
            'AuthCode'     => post('AuthCode'),
            'Status'       => post('Status'),
            'ErrDesc'      => post('ErrDesc'),
            'Signature'    => post('Signature'),
            'CCName'       => post('CCName'),
            'CCNo'         => post('CCNo'),
            'S_bankname'   => post('S_bankname'),
            'S_country'    => post('S_country'),
        ];

        trace_log($details);

        $order = Order::where('id', post('RefNo'))->first();
        $paid = Ipay::paid(post('RefNo'))->first();

        if ($paid)
            return Redirect::to($this->controller->pageUrl($this->property('receipt'), ['orderid' => $order->hexhash]));

        $order = Order::get_existing_order_or_createnew();

        $hexHash = $this->param('hexhash');
        $orderFromHexHash = Order::pendingAndByHash($hexHash);

        if ($orderFromHexHash == null) {
            Flash::warning('Order not found.');
            Log::error('Order not found [4].');
            return Redirect::to('cart');
        }

        if($orderFromHexHash->id != $order->id)
            $order = $orderFromHexHash;

        if($order == null)
            Log::error("Existing Order Not Found for: " . post('RefNo'));

        $calculated = Order::grandTotalByOrders($order->id);

        $isProd = CoreSettings::get('isProd');

        if ($isProd)
            $requery = Ipay::requery($details['MerchantCode'], $details['RefNo'], $details['Amount']);
        else
            $requery = "00";

        if($requery != "00"){
            Flash::error('Payment failed: ' . post('ErrDesc'));
            Log::error("IPay88 Response on ". post('RefNo'). ': ' . $requery . ' - '. post('ErrDesc'));

            // if ($order)
                // $this->sendFailedEmail($order, post('ErrDesc'));

            Event::fire('order.history.status.change', [OrderStatus::ipay_failed()['id'], $order]);
            return Redirect::to($this->controller->pageUrl($this->property('fail')));
        }

        if ($isProd){
            $verifyResponse = Ipay::verifyResponse($order, $calculated, $details);
            if(!$verifyResponse){
                Flash::error('Payment failed: ' . post('ErrDesc'));
                Log::error("Payment fail verification on " . post('RefNo') . ': ' . post('ErrDesc'));
                Event::fire('order.history.status.change', [OrderStatus::ipay_signature_unmatched()['id'], $order]);
                return Redirect::to($this->controller->pageUrl($this->property('fail')));
            }
        }

        if($order->user == null)
            GuestProcessing::proceed($order);

        Ipay::justPay($order, $details);
        SaleCount::proceed($order);

        $order->invoice_no  = Order::generateInvoiceNo();
        $order->invoiced_at = Carbon::now();
        $order->status_id   = OrderStatus::processing()['id'];
        $order->save();

        Event::fire('order.history.status.change', [OrderStatus::ipay_successful()['id'], $order]);
        Event::fire('order.history.status.change', [OrderStatus::order_received()['id'], $order]);

        return Redirect::to($this->controller->pageUrl($this->property('receipt'), ['orderid' => $order->hexhash]));
    }

    private function sendFailedEmail($order, $errorMsg){
        $user = $order->user;
        if ($user) {
            $email = $user->email;

            $recepient = [
                'name' => $user->name,
                'email' => $email
            ];

            $data = [
                'name'    => $user->name,
                'data'    => $order->orderitems,
            ];

            $data['calculated'] = Order::grandTotalByOrders($order->id);
            $data['billing'] = $order->address;

            if($order->billing)
                $data['billing'] = $order->billing;

            $data['order']          = $order;
            $data['delivery']       = $order->address;
            $data['delivery_type']  = $order->delivery;
            $data['isProd']         = CoreSettings::get('isProd');
            $data['assetUrl']       = url('/');
            $data['isFailed']       = true;
            $data['failedReason']   = $errorMsg;

            $isProd = CoreSettings::get('isProd');

            $admin_emails = HelperSettings::get('admin_emails');

            $admin_emails_list = array();

            if ($admin_emails->count() > 0) {
                foreach ($admin_emails as $item) {
                    $admin_emails_list[$item->email] = $item->name;
                }
            } else {
                $admin_emails_list = [
                    'imbenghan@gmail.com' => "Beng Han",
                ];
            }

            if (!$isProd) {
                $admin_emails_list = [
                    'imbenghan@gmail.com' => "Beng Han",
                ];
            }

            // Mail::send("ig.transact::mail.failed", $data, function($message) use ($recepient) {
            //     $message->to($recepient['email'], $recepient['name']);
            // });
            // Mail::send("ig.transact::mail.details", $data, function($message) use ($recepient, $admin_emails_list) {
            //     $message->to($admin_emails_list);
            // });
        }
    }
}
