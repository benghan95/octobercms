<?php namespace IG\Transact\Components;

use Request;
use IG\Commerce\Classes\IG;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\ProductOption;
use IG\Core\Models\OrderStatus;
use IG\User\Models\User;
use IG\User\Models\Wishlist;
use IG\User\Models\WishlistItem;
use IG\Transact\Models\Order;
use IG\Transact\Models\OrderItem;
use Cms\Classes\ComponentBase;
use Cms\Classes\Controller;
use Cms\Facades\Cms;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Facades\Auth;

class Bag extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Bag Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        parent::onRun();

        $order = Order::get_existing_if_exists();

        $this->page['cart_count'] = 0;
        $this->page['cart_items'] = collect();

        if ($order) {
            foreach ($order->orderitems as $orderitem){
                $orderItemDeleted = false;
                if (count($orderitem->itemstocks) <= 0){
                    $orderitem->delete();
                    continue;
                }
                foreach ($orderitem->itemstocks as $item){
                    if (!$item->stock){
                        $item->delete();
                        break;
                    }

                    $stock = $item->stock;
                    if ($stock->in < $orderitem->quantity ){
                        if ($stock->in == 0){
                            $orderItemDeleted = true;
                            $orderitem->delete();
                            break;
                        }
                        else{
                            $orderitem->quantity = $stock->in;
                            $orderitem->save();
                        }
                    }
                }

                if ($orderItemDeleted){
                    $orderItemDeleted = false;
                    continue;
                }
            }

            if ($order->status_id == OrderStatus::pending_payment()['id'] && !(strpos($url, 'checkout/ipay') !== false)){
                $order->status_id = OrderStatus::pending()['id'];
                $order->save();
            }

            $this->page['cart_count'] = count($order->orderitems);
            $this->page['cart_items'] = $order->orderitems;
        }

        $is_agent = false;
        if(Auth::check()) {
            $user = Auth::getUser();
            $user = User::find($user->id);

            if ($user->agenttype) {
                $is_agent = true;
            }
        }

        $this->page['is_agent']   = $is_agent;
    }

    public function onGetOrder(){
        return IG::retrieve()->toArray();
    }

    public function onAddItem(){
        $product_id = post('product_id');
        $quantity   = post('quantity');
        $options    = post('options');
        // $stocks     = post('stocks');

        if ($product_id == null){
            return [
                'status'  => 'failed',
                'results' => [
                    'message' => 'Please refresh the page and try again.',
                ],
            ];
        }

        if (count($options) <= 0){
            return [
                'status'  => 'failed',
                'results' => [
                    'message' => 'Please select an option to add to cart',
                ],
            ];
        }

        if ($quantity <= 0){
            return [
                'status'  => 'failed',
                'results' => [
                    'message' => 'Quantity must be equal or more than 1 to add to cart',
                ],
            ];
        }

        $order = IG::create();

        foreach ($order->orderitems as $item){
            $item->toJSONArray();
        }

        return [
            'status'  => 'success',
            'results' => [
                'order'     => $order,
                'itemCount' => count($order->orderitems),
            ],
        ];
    }

    public function onDeleteOrderItem(){
        $hash = post('hash');

        $orderItem = OrderItem::where('hexhash', $hash)->first();

        if($orderItem == null)
            throw new ApplicationException('No such item in your order, please try again later.');

        $order       = $orderItem->order;
        $deletedHash = $orderItem->hexhash;
        $orderItem->delete();

        foreach ($order->orderitems as $item){
            $item->toJSONArray();
        }

        return [
            'result'      => 'success',
            'order'       => $order,
            'deletedHash' => $deletedHash,
            'itemCount'   => count($order->orderitems),
        ];
    }

    public function onCheckout(){
        return Redirect::to('cart');
    }

    public function onAddToWishlist(){
        if(!Auth::check()) {
            return [
                'status' => 'user-404',
            ];
        }

        $user = Auth::getUser();
        $user = User::find($user->id);

        $hexhash = post('productId');
        $options_hexhash = post('options');


        if ($hexhash){
            $product = Product::where('hexhash', $hexhash)->first();

            if ($product){
                $optionsList = '';
                if (count($options_hexhash) > 0){
                    $counter = 0;
                    foreach ($options_hexhash as $item){
                        $option = ProductOption::where('hexhash', $item)->first();

                        if ($option){
                            if ($counter == 0)
                                $optionsList .= $option->id;
                            else
                                $optionsList .= '|' . $option->id;

                            $counter++;
                        }
                    }
                }
                if ($optionsList != '')
                    $compositeID = $product->id . "|" . $optionsList;
                else
                    $compositeID = $product->id;

                $wishlist = $this->checkItemExistInWishlist($user, $compositeID);

                if (!$wishlist){
                    $wishlist = new Wishlist();
                    $wishlist->user_id = $user->id;
                    $wishlist->product_id = $product->id;
                    $wishlist->favorited = true;
                    $wishlist->composite = $compositeID;
                    $wishlist->save();

                    if (count($options_hexhash) > 0){
                        foreach ($options_hexhash as $item){
                            $option = ProductOption::where('hexhash', $item)->first();

                            if ($option){
                                $item = new WishlistItem();
                                $item->wishlist_id = $wishlist->id;
                                $item->option_id = $option->id;
                                $item->save();
                            }
                        }
                    }
                }
                else{
                    $wishlist->favorited = true;
                    $wishlist->save();
                }

                return [
                    'status' => 'success',
                ];
            }
            else{
                throw new ApplicationException('Error occured. Please try again later.');
            }
        }
    }

    private function checkItemExistInWishlist($user, $compositeID){
        $wishlists = $user->wishlists;

        foreach($wishlists as $wishlist){
            if($wishlist->composite == $compositeID)
                return $wishlist;
        }

        return false;
    }
}
