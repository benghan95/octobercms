<?php namespace IG\Transact\Controllers;

use Event;
// use Excel;
use Input;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Backend\Facades\Backend;
use Carbon\Carbon;
use DateTime;
use IG\Transact\Models\Order as OrderModel;
use IG\Transact\Models\Payment;

/**
 * Order Back-end Controller
 */
class Order extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig         = 'config_form.yaml';
    public $listConfig         = 'config_list.yaml';
    public $relationConfig     = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.Transact', 'transact', 'order');

        $this->addJs('/themes/demo/assets/js/vendor/jquery.ui.widget.js');
        $this->addJs('/themes/demo/assets/js/vendor/jquery.iframe-transport.js');
        $this->addJs('/themes/demo/assets/js/vendor/jquery.fileupload.js');

        $this->addJs('/themes/demo/assets/js/vendor/underscore.js');
        $this->addJs('/themes/demo/assets/js/dojoconfig.js');
        $this->addJs('/themes/demo/assets/js/dojo.js');
        $this->addJs('/themes/demo/assets/js/TopicCenter.js');
        $this->addJs('/themes/demo/assets/js/App.js');
    }

    public function index(){
        $datefrom = $this->vars['datefrom'] = Carbon::now('Asia/Kuala_Lumpur')->subDays(7)->toDateString();
        $dateto = $this->vars['dateto'] = Carbon::now('Asia/Kuala_Lumpur')->toDateString();

        $timefrom = date('H:i');
        $timeto = date('H:i', strtotime($timefrom . ' +6 hours'));

        return Redirect::to('backend/ig/transact/order/range/?df='.$datefrom.'&dt='.$dateto.'&tf='.$timefrom.'&tt='.$timeto);
    }

    public function range(){
        $allInput = Input::get();

        if(empty($allInput['df']) || !isset($allInput['df'])){
            $datefrom = Carbon::now('Asia/Kuala_Lumpur')->subDays(7)->toDateString();
            $dateto = Carbon::now('Asia/Kuala_Lumpur')->toDateString();

            $timefrom = date('H:i');
            $timeto = date('H:i', strtotime($timefrom . ' +6 hours'));

            return Redirect::to('backend/ig/transact/order/range/?df='.$datefrom.'&dt='.$dateto.'&tf='.$timefrom.'&tt='.$timeto);
        }

        $this->vars['datefrom'] = $allInput['df'];
        $this->vars['dateto'] = $allInput['dt'];
        $this->vars['timefrom'] = $allInput['tf'];
        $this->vars['timeto'] = $allInput['tt'];
        $this->vars['controllerURL'] = "/".Backend::uri()."/ig/transact/order/range/";

        $themeFolder = "/themes/demo";
        $this->addCss($themeFolder.'/assets/vendor/datepicker/datepicker.css');
        $this->addJs($themeFolder.'/assets/vendor/datepicker/bootstrap-datepicker.2.js');
        $this->addJs($themeFolder.'/assets/vendor/equalize/equalize.min.js');

        $this->addCss($themeFolder.'/assets/vendor/clockpicker/jquery-clockpicker.css');
        $this->addJs($themeFolder.'/assets/vendor/clockpicker/jquery-clockpicker.js');


        $this->asExtension('ListController')->index();
    }

    public function update($modelID){
        parent::update($modelID);
        $order = OrderModel::where('id', $modelID)->first();

        $payment = Payment::with('ipay')->where('order_id', $order->id)->first();

        $this->vars['payment'] = $payment;
        $this->vars['order'] = $order;
        $this->vars['backendurl'] = Backend::uri();
    }

    public function export(){
        $allInput = Input::get();
        $this->vars['datefrom'] = $allInput['df'];
        $this->vars['dateto'] = $allInput['dt'];
        $this->vars['timefrom'] = $allInput['tf'];
        $this->vars['timeto'] = $allInput['tt'];

        $this->asExtension('ImportExportController')->setFileName("Order Transactions ".$allInput['df']."_".$allInput['dt']."_".$allInput['tf'].":".$allInput['tt'].".csv");
        $this->asExtension('ImportExportController')->export();
    }

    public function report() {
        $this->pageTitle = 'Generate Sales Report';

        $this->addCss("/plugins/tkamine/user/assets/css/custom.css", "1.0.0");
        $this->addJs("/plugins/tkamine/user/assets/js/custom.js", "1.0.0");

        $path = Input::get("path");

        if ($path){
            return Response::download($path);
        }
    }

    public function onSuccessGenerate(){
        Storage::deleteDirectory('media/exports/');
    }

    public function onGenerateReport() {
        $input = Input('Order');

        $start_date = $input['start_date'];
        $end_date   = $input['end_date'];

        $filename = 'Sales-Report-' . Carbon::now('Asia/Kuala_Lumpur')->format('d-m-Y');
        if (!$start_date && !$end_date) {
            $filename = 'Sales-Report-' . Carbon::now('Asia/Kuala_Lumpur')->format('d-m-Y');
        } else if ($start_date && $end_date) {
            $filename = 'Sales-Report-' . Carbon::parse($start_date)->format('d-m-Y') . '-'. Carbon::parse($end_date)->format('d-m-Y');
        } else if ($start_date) {
            $filename = 'Sales-Report-' . Carbon::parse($start_date)->format('d-m-Y');
        } else {
            $filename = 'Sales-Report-' . Carbon::parse($end_date)->format('d-m-Y');
        }

        // $headerArray    = $this->exportHeader();
        // $investorsArray = $this->exportInvestor();

        // $randomFolder1 = mt_rand(100, 999);
        // $randomFolder2 = mt_rand(100, 999);
        // $randomFolder3 = mt_rand(100, 999);

        // $storagePath = 'app/media/exports/' . $randomFolder1 . '/' . $randomFolder2 . '/' . $randomFolder3 . '/';

        // $path = Excel::create($filename, function($excel) use ($investorsArray, $headerArray) {
        //     $excel->sheet('Sheet 1', function($sheet) use ($investorsArray, $headerArray) {
        //         $sheet->fromArray($headerArray);
        //         $sheet->fromArray($investorsArray);
        //     });
        // })->store('xls', storage_path($storagePath), true);

        // $downloadPath = url('/') . '/storage/' . $storagePath . $path['file'];

        // return Redirect::to($downloadPath);
    }

    public function onInvoice(){
        $oids = implode(",", post('checked'));
        return Redirect::to(Backend::uri()."/boozeat/transact/order/invoice?oid=".$oids);
    }

    public function invoice(){
        $allID = get('oid');
        $allIDs = explode(",",$allID);

        $orderids = [];

        foreach ($allIDs as $single){
            $orderid = \Boozeat\Transact\Models\Order::where('id', $single)->first();

            if($orderid)
                array_push($orderids, $orderid->orderid);
        }

        $this->vars['orderids'] = $orderids;
    }

    /**
     * Controller override: Extend the query used for populating the list
     * after the default query is processed.
     * @param October\Rain\Database\Builder $query
     */
    public function listExtendQueryBefore($query, $definition = null)
    {
        $query->has('payment');

        $isSearch = post('listToolbarSearch');

        //By passing the original date limit
        if(isset($isSearch) || !empty($isSearch))
            return $query;

        if($this->action == "index" || $this->action == "range" || $this->action == "export"){
            $datefrom = DateTime::createFromFormat('Y-m-d', $this->vars['datefrom'])->format('Y-m-d '.$this->vars['timefrom'].':00');
            $dateto = DateTime::createFromFormat('Y-m-d', $this->vars['dateto'])->format('Y-m-d '.$this->vars['timeto'].':00');

            $query->where('invoiced_at', '>', $datefrom)->where('invoiced_at', '<=', $dateto);
        }
    }

    /**
     * Called after the creation or updating form is saved.
     * @param Model
     */
    public function formAfterSave($model)
    {
        Event::fire('order.history.status.change', [$model->orderstatus->id, $model]);
    }

}
