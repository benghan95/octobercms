<?php namespace IG\Transact\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Order Item Back-end Controller
 */
class OrderItem extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.Transact', 'transact', 'orderitem');
    }
}
