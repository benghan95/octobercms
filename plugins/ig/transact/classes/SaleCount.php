<?php namespace IG\Transact\Classes;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use RainLab\User\Models\User;

class SaleCount
{
    public static function proceed($order){
        $orderItems = $order->orderitems;

        foreach($orderItems as $orderitem){
            $product = $orderitem->product;

            if($product){
                $product->sale_count = $product->sale_count + $orderitem->quantity;
                $product->save();
            }

            foreach($orderitem->itemstocks as $item){
                if($item->option){
                    $item->option->sale_count = $item->option->sale_count + $orderitem->quantity;
                    $item->option->save();
                }
            }
        }
    }
}