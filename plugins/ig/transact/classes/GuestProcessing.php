<?php namespace IG\Transact\Classes;

use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use RainLab\User\Models\User;

class GuestProcessing
{
    public static function proceed($order){
        $user = self::newUser($order);

        $shipping = $order->address;
        if ($shipping) {
            $shipping->user_id = $user->id;
            $shipping->save();
        }

        $billing = $order->billing;
        if ($billing) {
            $billing->user_id = $user->id;
            $billing->save();
        }

        $order->user_id = $user->id;
        $order->save();

        Session::forget('guest-checkout');
    }

    private static function newUser($order){
        $user = User::findByEmail($order->temp['email']);
        if($user == null){
            $is_deleted = User::withTrashed()->where('email', $order->temp['email'])->first();
            if ($is_deleted) {
                $is_deleted->restore();
                $user = User::findByEmail($order->temp['email']);
                return $user;
            }

            $user = new User();
            $user->is_guest = true;
            $user->contact_no = $order->temp['contact_no'];
            $user->fill($order->temp);
            $user->is_activated = true;
            $user->activated_at = Carbon::now('Asia/Kuala_Lumpur');
            $user->save();

            $user->convertToRegistered(true);
            Event::fire('rainlab.user.registered', [$user]);

            return $user;
        }

        return false;
    }
}