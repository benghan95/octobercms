<?php namespace IG\Transact;

use App;
use Event;
use Mail;
use Log;
use File;
use Backend;
use Carbon\Carbon;
use IG\Core\Models\OrderStatus;
use IG\Core\Models\Settings;
use IG\Transact\Models\OrderHistory;
use IG\Transact\Models\Order;
use IG\Transact\Models\Promocode;
use IG\Helper\Models\Settings as HelperSettings;
use Illuminate\Foundation\AliasLoader;
use System\Classes\PluginBase;
use Renatio\DynamicPDF\Classes\PDF;

/**
 * Transact Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Transact',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-usd'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        Event::listen('order.history.status.change', function($status, $order){

            //Touch Order Timestamp
            switch ($status){
                case OrderStatus::ipay_successful()['id']:
                    break;

                default:
                    break;
            }

            /**
             *
             * If informable, will fire event to Ido.Notify plugin for email
             *
             */
            $orderStatus = OrderStatus::find($status);
            if($orderStatus && $orderStatus->notifiable)
                Event::fire('notify.user.status.change', [$status, $order]);

            $getLastHistory = $order->history;
            if($getLastHistory->count() > 0){
                $getLastHistory = $order->history()->orderBy('created_at', 'desc')->first();
                if($getLastHistory->status_id == $status)
                    return;
            }

            $newHistory = new OrderHistory();
            $newHistory->status = $status;
            $newHistory->order = array($order->id);
            $newHistory->save();
        });

        Event::listen('notify.user.status.change', function($statusid, $order){
            $user = $order->user;
            if ($user) {
                $email = $user->email;

                $recepient = [
                    'name'  => $user->name,
                    'email' => $email
                ];

                $data['calculated']        = Order::grandTotalByOrders($order->id);
                $data['shipping']          = $order->address;
                $data['name']              = $user->name;
                $data['orderitems']        = $order->orderitems;
                $data['status']            = OrderStatus::find($statusid);
                $data['order']             = $order;
                $data['billing']           = $order->billing;
                $data['delivery']          = $order->address;
                $data['delivery_location'] = $order->deliverylocation;
                $data['payment']          = $order->payment;
                $data['isProd']            = Settings::get('isProd');
                if(!$order->billing)
                    $data['billing'] = $order->address;

                $orderStatus = OrderStatus::find($statusid);

                $isProd = Settings::get('isProd');

                $admin_emails = HelperSettings::get('admin_emails');

                $admin_emails_list = array();

                if ($admin_emails->count() > 0) {
                    foreach ($admin_emails as $item) {
                        $admin_emails_list[$item->email] = $item->name;
                    }
                } else {
                    $admin_emails_list = [
                        'imbenghan@gmail.com' => "Beng Han",
                    ];
                }

                if (!$isProd) {
                    $admin_emails_list = [
                        'imbenghan@gmail.com' => "Beng Han",
                    ];
                }

                if($orderStatus == null)
                    return;

                if ($orderStatus->id == OrderStatus::order_received()['id']){
                    if (!$order->invoice_pdf){
                        $pdf = PDF::loadTemplate('ig::invoice', $data);

                        $filename = 'invoice-' . $order->invoice_no;
                        $output   = $pdf->output();
                        $filepath = temp_path() . '/' . $filename . '.pdf';
                        File::put($filepath, $output);

                        $file            = new \System\Models\File;
                        $file->data      = $filepath;
                        $file->is_public = true;
                        $file->save();

                        $order->invoice_pdf()->add($file);

                        $file = $order->invoice_pdf;
                    } else {
                        $file = $order->invoice_pdf;
                    }

                    Mail::send("ig.transact::mail.confirmation", $data, function($message) use ($recepient, $file) {
                        $message->to($recepient['email'], $recepient['name'])
                                ->attach($file->getLocalPath(), ['as' => 'Invoice.pdf']);
                    });
                    Mail::send("ig.transact::mail.confirmation", $data, function($message) use ($recepient, $admin_emails_list, $file) {
                        $message->to($admin_emails_list)
                                ->attach($file->getLocalPath(), ['as' => 'Invoice.pdf']);
                    });
                } else {
                    Mail::send("ig.transact::mail.confirmation", $data, function($message) use ($recepient) {
                        $message->to($recepient['email'], $recepient['name']);
                    });
                    Mail::send("ig.transact::mail.confirmation", $data, function($message) use ($recepient, $admin_emails_list) {
                        $message->to($admin_emails_list);
                    });
                }
            }
        });
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Payment Gateways',
                'description' => 'Manages key and secret keys for payment gateway, iPay88',
                'category'    => 'IG Transact',
                'icon'        => 'icon-cog',
                'class'       => 'IG\Transact\Models\Settings',
                'order'       => 550,
                'permissions' => ['ig.transact.manage_settings']
            ]
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // // Service provider
        // App::register('\Maatwebsite\Excel\ExcelServiceProvider');

        // // Register alias
        // $alias = AliasLoader::getInstance();
        // $alias->alias('Excel', '\Maatwebsite\Excel\Facades\Excel');
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IG\Transact\Components\Bag'                => 'bag',
            'IG\Transact\Components\Checkout'           => 'checkout',
            'IG\Transact\Components\IpayCheckin'        => 'ipayCheckin',
            'IG\Transact\Components\IpayRequestExecute' => 'ipayRequestExecute',
            'IG\Transact\Components\IpayResponse'       => 'ipayResponse',
            'IG\Transact\Components\Receipt'            => 'receipt',
            'IG\Transact\Components\Orders'             => 'orders'
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'ig.transact::mail.confirmation',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'ig.transact.some_permission' => [
                'tab' => 'Transact',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'transact' => [
                'label'       => 'Transact',
                'url'         => Backend::url('ig/transact/order'),
                'icon'        => 'icon-usd',
                'permissions' => ['ig.transact.*'],
                'order'       => 140,
                'sideMenu'    => [
                    'order' => [
                        'label'       => 'Orders',
                        'url'         => Backend::url('ig/transact/order'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.transact.*'],
                        'order'       => 100,
                    ],
                    'payment' => [
                        'label'       => 'Payments',
                        'url'         => Backend::url('ig/transact/payment'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.transact.*'],
                        'order'       => 200,
                    ],
                    'promocode' => [
                        'label'       => 'Promocodes',
                        'url'         => Backend::url('ig/transact/promocode'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.transact.*'],
                        'order'       => 300,
                    ],
                    'ipay' => [
                        'label'       => 'iPay88',
                        'url'         => Backend::url('ig/transact/ipay'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.transact.*'],
                        'order'       => 400,
                    ],
                ]
            ],
        ];
    }
}
