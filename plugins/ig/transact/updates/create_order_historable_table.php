<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderHistorableTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_order_historable', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('order_history_id');
            $table->integer('historable_id');
            $table->string('historable_type');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_order_histories');
    }
}
