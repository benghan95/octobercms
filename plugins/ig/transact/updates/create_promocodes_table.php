<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePromocodesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_promocodes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('code')->unique();
            $table->string('description')->nullable();
            $table->boolean('status')->default(true);
            $table->string('discount_type');
            $table->float('discount_amount', 10, 2)->nullable();
            $table->boolean('free_delivery')->default(false);
            $table->integer('usage_limit_order')->nullable();
            $table->integer('usage_limit_user')->nullable();
            $table->float('min_purchase', 10, 2)->nullable();
            $table->float('discount_cap', 10, 2)->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->text('specific_emails')->nullable();
            $table->text('specific_categories')->nullable();
            $table->text('specific_products')->nullable();

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('backend_users');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_promocodes');
    }
}
