<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_payments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('ig_transact_orders');

            $table->float('subtotal', 10, 2);
            $table->float('tax', 10, 2);
            $table->float('shipping', 10, 2);
            $table->float('discount', 10, 2);
            $table->float('total', 10, 2);
            $table->float('weight', 10, 3);

            $table->integer('currency_id')->unsigned()->nullable();
            $table->foreign('currency_id')->references('id')->on('ig_core_currencies');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_payments');
    }
}
