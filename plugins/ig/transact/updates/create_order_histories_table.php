<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_order_histories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('ig_core_order_statuses');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_order_histories');
    }
}
