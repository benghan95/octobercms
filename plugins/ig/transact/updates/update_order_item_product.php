<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateOrderItemProduct extends Migration
{
    public function up()
    {
        Schema::table('ig_transact_order_items', function($table) {
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('ig_commerce_products');
        });
    }

    public function down()
    {
      $table->dropColumn('product_id');
    }
}