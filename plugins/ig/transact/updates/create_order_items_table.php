<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('ig_transact_orders');

            $table->integer('stock_id')->unsigned();
            $table->foreign('stock_id')->references('id')->on('ig_commerce_stocks');

            $table->string('composite')->nullable();

            $table->integer('pricing_id')->unsigned();
            $table->foreign('pricing_id')->references('id')->on('ig_commerce_pricings');

            $table->integer('currency_id')->unsigned()->nullable();
            $table->foreign('currency_id')->references('id')->on('ig_core_currencies');

            $table->float('unit_price', 10, 2);
            $table->integer('quantity');

            $table->string('hexhash');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_order_items');
    }
}
