<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrderItemStocksTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_order_item_stocks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('orderitem_id')->unsigned();
            $table->foreign('orderitem_id')->references('id')->on('ig_transact_order_items');

            $table->integer('option_id')->unsigned()->nullable();
            $table->foreign('option_id')->references('id')->on('ig_commerce_product_options');

            $table->integer('stock_id')->unsigned();
            $table->foreign('stock_id')->references('id')->on('ig_commerce_stocks');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_order_item_stocks');
    }
}
