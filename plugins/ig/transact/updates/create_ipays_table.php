<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateIpaysTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_ipays', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('payment_id')->unsigned();
            $table->foreign('payment_id')->references('id')->on('ig_transact_payments');

            $table->string('invoice_no')->nullable();
            $table->string('transaction_no')->nullable();
            $table->string('currency')->nullable();
            $table->text('product_description')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('err_desc')->nullable();
            $table->string('remark')->nullable();
            $table->string('lang')->nullable();
            $table->string('signature')->nullable();
            $table->string('auth_code')->nullable();
            $table->string('cc_name')->nullable();
            $table->string('cc_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('country')->nullable();
            $table->boolean('status')->default(true);

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_ipays');
    }
}
