<?php namespace IG\Transact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('ig_transact_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('invoice_no')->nullable();
            $table->timestamp('invoiced_at')->nullable();
            
            $table->integer('shipping_id')->unsigned()->nullable();
            $table->foreign('shipping_id')->references('id')->on('ig_user_addresses');
            
            $table->integer('billing_id')->unsigned()->nullable();
            $table->foreign('billing_id')->references('id')->on('ig_user_addresses');
            
            $table->integer('status_id')->unsigned()->nullable();
            $table->foreign('status_id')->references('id')->on('ig_core_order_statuses');
            
            $table->integer('delivery_id')->unsigned()->nullable();
            $table->foreign('delivery_id')->references('id')->on('ig_core_delivery_locations');
            
            $table->integer('promocode_id')->unsigned()->nullable();
            $table->foreign('promocode_id')->references('id')->on('ig_transact_promocodes');

            $table->string('session_code')->nullable();

            $table->string('hexhash')->nullable();
            $table->text('temp')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_transact_orders');
    }
}
