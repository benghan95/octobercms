<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWishlistItemsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_user_wishlist_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('wishlist_id')->unsigned();
            $table->foreign('wishlist_id')->references('id')->on('ig_user_wishlists');

            $table->integer('option_id')->unsigned()->nullable();
            $table->foreign('option_id')->references('id')->on('ig_commerce_product_options');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_user_wishlist_items');
    }
}
