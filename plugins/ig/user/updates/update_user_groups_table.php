<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUserGroupsTable extends Migration
{
    public function up()
    {
        Schema::table('user_groups', function($table) {
            $table->boolean('is_agent')->default(false);
            $table->float('discount_rate')->nullable();
        });
    }

    public function down()
    {
        Schema::table('user_groups', function($table) {
            $table->dropColumn('is_agent');
            $table->dropColumn('discount_rate');
        });
    }
}
