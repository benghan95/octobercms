<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateUserAgentType extends Migration
{
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('agent_type_id')->unsigned()->nullable();
            $table->foreign('agent_type_id')->references('id')->on('ig_user_agent_types');
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('agent_type_id');
        });
    }
}
