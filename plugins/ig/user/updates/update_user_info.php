<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateUserInfo extends Migration
{
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('contact_no')->nullable();
            $table->datetime('dob')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('contact_no');
            $table->dropColumn('dob');
        });
    }
}
