<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateUserCurrency extends Migration
{
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('currency_id')->unsigned()->nullable();
            $table->foreign('currency_id')->references('id')->on('ig_core_currencies');
        });
    }

    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('currency_id');
        });
    }
}
