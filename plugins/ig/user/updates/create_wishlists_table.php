<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWishlistsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_user_wishlists', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('ig_commerce_products');

            $table->boolean('favorited')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_user_wishlists');
    }
}
