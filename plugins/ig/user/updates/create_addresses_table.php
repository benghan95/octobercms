<?php namespace IG\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('ig_user_addresses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->string('name');
            $table->string('company')->nullable();
            $table->string('address_1');
            $table->string('address_2')->nullable();

            $table->string('postcode');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('ig_core_states');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('ig_core_countries');

            $table->string('telephone');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_user_addresses');
    }
}
