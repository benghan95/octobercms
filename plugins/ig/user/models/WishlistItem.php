<?php namespace IG\User\Models;

use Model;

/**
 * WishlistItem Model
 */
class WishlistItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_user_wishlist_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'option' => ['IG\Commerce\Models\ProductOption', 'key' => 'option_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
