<?php namespace IG\User\Models;

use Model;

/**
 * User Model
 */
class User extends \RainLab\User\Models\User
{
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'orders'    => ['IG\Transact\Models\Order', 'key' => 'user_id'],
        'wishlists' => ['IG\User\Models\Wishlist', 'key' => 'user_id'],
    ];
    public $belongsTo = [
        'agenttype' => ['IG\User\Models\AgentType', 'key' => 'agent_type_id'],
        'currency'  => ['IG\Core\Models\Currency', 'key' => 'currency_id'],
    ];
    public $belongsToMany = [
        'groups' => [\IG\User\Models\UserGroup::class, 'table' => 'users_groups']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
