<?php namespace IG\User\Models;

use Model;

/**
 * UserGroup Model
 */
class UserGroup extends \RainLab\User\Models\UserGroup
{
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'users'       => [\IG\User\Models\User::class, 'table' => 'users_groups'],
        'users_count' => [\IG\User\Models\User::class, 'table' => 'users_groups', 'count' => true]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
