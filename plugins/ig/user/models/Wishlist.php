<?php namespace IG\User\Models;

use Model;

/**
 * Wishlist Model
 */
class Wishlist extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_user_wishlists';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'items' => ['IG\User\Models\WishlistItem', 'key' => 'wishlist_id'],
    ];
    public $belongsTo = [
        'user'    => ['IG\User\Models\User', 'key' => 'user_id'],
        'product' => ['IG\Commerce\Models\Product', 'key' => 'product_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeActive($query){
        return $query->where('favorited', true);
    }
}
