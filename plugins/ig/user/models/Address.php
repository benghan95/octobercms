<?php namespace IG\User\Models;

use Model;
use IG\Core\Models\DeliveryLocation;

/**
 * Address Model
 */
class Address extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_user_addresses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'       => 'required|between:2,255',
        'type'       => 'required',
        'telephone'  => 'required|digits_between:8,15',
        'country_id' => 'required',
        'state_id'   => 'required',
        'city'       => 'required',
        'postcode'   => 'required|between:5,8',
        'address_1'  => 'required|between:5,255',
        'address_2'  => 'between:5,255',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'type',
        'company',
        'address_1',
        'address_2',
        'postcode',
        'state_id',
        'country_id',
        'city',
        'telephone',
        'hexhash',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'state'   => ['IG\Core\Models\State', 'key' => 'state_id'],
        'country' => ['IG\Core\Models\Country', 'key' => 'country_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getDeliveryLocationAttribute(){
        if ($this->state && $this->country){
            $location = DeliveryLocation::where('country_id', $this->country->id)
                                        ->where('state_id', $this->state->id)
                                        ->first();

            if ($location)
                return $location;
            else
                return null;
        }
        else{
            return null;
        }
    }
}
