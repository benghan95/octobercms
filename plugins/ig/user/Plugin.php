<?php namespace IG\User;

use Mail;
use Backend;
use System\Classes\PluginBase;
use IG\User\Models\User as UserModel;

/**
 * User Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'User',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-user'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('rainlab.user.register', function ($user) {
            $this->newRegistered($user);
        });
        \Event::listen('rainlab.user.registered', function ($user) {
            $this->newRegistered($user);
        });
    }

    private function newRegistered($user) {
        $newUser = UserModel::find($user->id);
        if ($newUser) {
            $newUser->agent_type_id = 1;
            $newUser->save();

            Mail::send('ig.user::mail.welcome', $newUser->toArray(), function($message) use ($newUser) {
                $message->to($newUser->email);
            });
        }
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IG\User\Components\Authentication'  => 'authentication',
            'IG\User\Components\AccountOverview' => 'accountOverview',
            'IG\User\Components\OrderHistory'    => 'orderHistory',
            'IG\User\Components\UserWishlist'    => 'userWishlist',
            'IG\User\Components\UserHistory'     => 'userHistory',
            'IG\User\Components\EditAddress'     => 'editAddress',
            'IG\User\Components\UserAddress'     => 'userAddress',
            'IG\User\Components\ForgotPassword'  => 'forgotPassword',
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'ig.user::mail.welcome',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'ig.user.some_permission' => [
                'tab' => 'User',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'user' => [
                'label'       => 'User',
                'url'         => Backend::url('ig/user/user'),
                'icon'        => 'icon-user',
                'iconSvg'     => 'plugins/rainlab/user/assets/images/user-icon.svg',
                'permissions' => ['ig.user.*'],
                'order'       => 110,
                'sideMenu'    => [
                    'user' => [
                        'label'       => 'Users',
                        'url'         => Backend::url('ig/user/user'),
                        'icon'        => 'icon-user',
                        'permissions' => ['ig.user.*'],
                        'order'       => 100,
                    ],
                    'agenttype' => [
                        'label'       => 'Agent Types',
                        'url'         => Backend::url('ig/user/agenttype'),
                        'icon'        => 'icon-users',
                        'permissions' => ['ig.user.*'],
                        'order'       => 200,
                    ],
                ]
            ],
        ];
    }
}
