<?php namespace IG\User\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * User Back-end Controller
 */
class User extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.User', 'user', 'user');
    }
}
