<?php namespace IG\User\Components;

use Cms\Classes\ComponentBase;

class UserHistory extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserHistory Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
