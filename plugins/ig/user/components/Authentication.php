<?php namespace IG\User\Components;

use IG\Core\Models\User;
use Exception;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;
use RainLab\User\Facades\Auth;

class Authentication extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Auth Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $user = Auth::getUser();

        if ($user)
            return Redirect::to('/my-account/profile');
    }
}
