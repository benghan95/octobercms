<?php namespace IG\User\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use IG\Transact\Models\Order;
use IG\Commerce\Models\Product;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;
use RainLab\User\Facades\Auth;

class OrderHistory extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'OrderHistory Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        parent::onRun();

        if(!Auth::check())
            return;

        $user = Auth::getUser();

        $ordersOwned = Order::where('user_id', $user->id)
            ->whereHas('orderitems')
            ->whereHas('payment')
            ->orderBy('id', 'desc')
            ->get();

        $this->page['orders'] = $ordersOwned;
    }
}
