<?php namespace IG\User\Components;

use IG\Core\Models\Country;
use Cms\Classes\ComponentBase;
use IG\User\Models\Address;
use RainLab\User\Facades\Auth;
use October\Rain\Exception\ApplicationException;

class EditAddress extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'EditAddress Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        if(!Auth::check())
            return $this->controller->run('404');

        $user = Auth::getUser();

        $address_id = $this->param('addressId');

        $address = Address::find($address_id);

        if (!$address) {
            return $this->controller->run('404');
        }

        if ($address->user_id != $user->id) {
            return $this->controller->run('404');
        }

        $this->page['address']   = $address;
        $this->page['countries'] = Country::hasDelivery()->get();
        $this->page['states']    = Country::hasDelivery()->first()->states;
    }

    public function onUpdateAddress() {
        $post = post();

        $address_id = $this->param('addressId');

        if (!$post['name_shipping']) {
            throw new ApplicationException('Please enter your name.');
            return;
        }

        if (!$post['phone_shipping']) {
            throw new ApplicationException('Please enter your telephone number.');
            return;
        }

        if (!$post['address1_shipping']) {
            throw new ApplicationException('Please enter an address.');
            return;
        }

        if (!$post['country_shipping']) {
            throw new ApplicationException('Please select a country.');
            return;
        }

        if (!$post['state_shipping']) {
            throw new ApplicationException('Please select a state.');
            return;
        }

        if (!$post['city_shipping']) {
            throw new ApplicationException('Please enter a city.');
            return;
        }

        if (!$post['postcode_shipping']) {
            throw new ApplicationException('Please enter address postcode.');
            return;
        }

        $invalidCode = false;
        $postal = intval($post['postcode_shipping']);
        switch ($post['state_shipping']) {
            // Johor
            case '1971':
                if (!($postal >= 79000 && $postal <= 86900))
                    $invalidCode = true;
                break;
            // Kedah
            case '1972':
                if (!($postal >= 5000 && $postal <= 9810))
                    $invalidCode = true;
                break;
            // Kelantan
            case '1973':
                if (!($postal >= 15000 && $postal <= 18500))
                    $invalidCode = true;
                break;
            // Labuan
            case '1974':
                if (!($postal >= 87000 && $postal <= 87033))
                $invalidCode = true;
                break;
            // Melaka
            case '1975':
                if (!($postal >= 75000 && $postal <= 78309))
                    $invalidCode = true;
                break;
            // Negeri Sembilan
            case '1976':
                if (!($postal >= 70000 && $postal <= 73509))
                    $invalidCode = true;
                break;
            // Pahang
            case '1977':
                if (!(($postal >= 25000 && $postal <= 28800) || ($postal >= 39000 && $postal <= 39200) || $postal == 49000 || $postal == 69000))
                    $invalidCode = true;
                break;
            // Perak
            case '1978':
                if (!($postal >= 30000 && $postal <= 36810))
                    $invalidCode = true;
                break;
            // Perlis
            case '1979':
                if (!($postal >= 1000 && $postal <= 2999))
                    $invalidCode = true;
                break;
            // Pulau Pinang
            case '1980':
                if (!($postal >= 10000 && $postal <= 14400))
                    $invalidCode = true;
                break;
            // Sabah
            case '1981':
                if (!($postal >= 88000 && $postal <= 91309))
                    $invalidCode = true;
                break;
            // Sarawak
            case '1982':
                if (!($postal >= 93000 && $postal <= 98859))
                    $invalidCode = true;
                break;
            // Selangor
            case '1983':
                if (!(($postal >= 40000 && $postal <= 48300) || ($postal >= 63000 && $postal <= 68100)))
                    $invalidCode = true;
                break;
            // Terengganu
            case '1984':
                if (!($postal >= 20000 && $postal <= 24300))
                    $invalidCode = true;
                break;
            // Wilayah Persekutuan
            case '1985':
                if (!($postal >= 50000 && $postal <= 60000))
                    $invalidCode = true;
                break;
            // Putrajaya
            case '4047':
                if (!($postal >= 62000 && $postal <= 62988))
                    $invalidCode = true;
                break;
        }

        if ($invalidCode) {
            throw new ApplicationException('Invalid postcode on shipping address, please enter the correct postcode based on the state selected.');
            return;
        }

        $address = Address::find($address_id);

        if ($address) {
            $address->name       = $post['name_shipping'];
            $address->company    = $post['companyname_shipping'];
            $address->telephone  = $post['phone_shipping'];
            $address->city       = $post['city_shipping'];
            $address->country_id = $post['country_shipping'];
            $address->state_id   = $post['state_shipping'];
            $address->postcode   = $post['postcode_shipping'];
            $address->address_1  = $post['address1_shipping'];
            $address->address_2  = $post['address2_shipping'];
            $address->save();

            return [
                'status' => 'success',
            ];
        }
    }

    public function onCountryChange(){
        $countryId = post('country');

        if ($countryId){
            $country = Country::find($countryId);

            if ($country){
                $states = $country->states->sortBy('name')->toArray();

                return [
                    'default'    => '-- Select a state --',
                    'states'     => $states,
                ];
            }
            else{
                return [
                    'default'    => '-- Select a valid country --',
                ];
            }
        }
        else{
            return [
                'default'    => '-- Select a country first --',
            ];
        }
    }
}
