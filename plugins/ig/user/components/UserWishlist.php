<?php namespace IG\User\Components;

use IG\User\Models\User;
use IG\User\Models\Wishlist;
use RainLab\User\Facades\Auth;
use Cms\Classes\ComponentBase;
use October\Rain\Exception\ApplicationException;

class UserWishlist extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserWishlist Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        if(!Auth::check())
            return;

        $user = Auth::getUser();
        $user = User::find($user->id);
        $wishlists = $user->wishlists->where('favorited', true);

        foreach ($wishlists as $wishlist){
            $product = $wishlist->product;
            $wishlist->has_stock = false;

            if ($product){
                if (count($wishlist->items) > 0){
                    $wishlist->has_stock = true;
                    foreach ($wishlist->items as $item){
                        if ($item->option->stock->in <= 0){
                            $wishlist->has_stock = false;
                            break;
                        }
                    }
                }
                else{
                    foreach ($product->options as $option){
                        $stock = $option->stock;
                        if ($stock->in > 0){
                            $wishlist->has_stock = true;
                            break;
                        }
                    }
                }
            }
        }

        $this->page['user'] = $user;
        $this->page['wishlists'] = $wishlists;
    }

    public function onRemoveWishlist(){
        $wishlist_id = post('wishlist_id');

        if ($wishlist_id){
            $wishlist = Wishlist::find($wishlist_id);

            if ($wishlist){
                $wishlist->favorited = false;
                $wishlist->save();

                return [
                    'status' => 'success',
                    'results' => $wishlist,
                ];
            }
            else{
                throw new ApplicationException('Error occured. Please try again later.');
            }
        }
        else{
            throw new ApplicationException('Error occured. Please try again later.');
        }
    }
}
