<?php namespace IG\User\Components;

use Carbon\Carbon;
use IG\User\Models\User;
use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;

class AccountOverview extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AccountOverview Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        if(!Auth::check())
            return;

        $user = Auth::getUser();
        $user = User::find($user->id);

        $this->page['user'] = $user;
    }

    public function onUpdateProfile(){
        if(!Auth::check())
            return;

        $data = [
            'name'       => post('name'),
            'contact_no' => post('contact_no'),
            'dob'        => post('dob'),
            'company'    => post('company'),
        ];

        $user             = Auth::getUser();
        $user             = User::find($user->id);
        $user->name       = $data['name'];
        $user->contact_no = $data['contact_no'];
        $user->company    = $data['company'];
        if ($data['dob']){
            $dob       = Carbon::createFromFormat('d/m/Y', $data['dob'])->setTime(0, 0, 0);
            $user->dob = $dob;
        }
        $user->save();

        return [
            'status' => 'success',
        ];
    }
}
