<?php namespace IG\User\Components;

use Cms\Classes\ComponentBase;
use IG\User\Models\Address;
use IG\User\Models\User;
use RainLab\User\Facades\Auth;

class UserAddress extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserAddress Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        if(!Auth::check())
            return $this->controller->run('404');

        $user = Auth::getUser();
        $user = User::find($user->id);

        $addresses = collect();
        $shipping_addresses = collect();
        $billing_addresses = collect();
        if ($user) {
            $addresses = Address::where('user_id', $user->id)->get();
            $shipping_addresses = Address::where('user_id', $user->id)->where('type', 'shipping')->get();
            $billing_addresses = Address::where('user_id', $user->id)->where('type', 'billing')->get();
        }

        $this->page['user']               = $user;
        $this->page['addresses']          = $addresses;
        $this->page['shipping_addresses'] = $shipping_addresses;
        $this->page['billing_addresses']  = $billing_addresses;
    }

    public function onEditAddress(){
        $address_id = post('address_id');

        if ($address_id){
            $address = Address::find($address_id);

            if ($address){
                $address->delete();

                return [
                    'status' => 'success',
                ];
            }
            else{
                throw new ApplicationException('Error occured. Please try again later.');
            }
        }
        else{
            throw new ApplicationException('Error occured. Please try again later.');
        }
    }

    public function onRemoveAddress(){
        $address_id = post('address_id');

        if ($address_id){
            $address = Address::find($address_id);

            if ($address){
                $address->delete();

                return [
                    'status' => 'success',
                ];
            }
            else{
                throw new ApplicationException('Error occured. Please try again later.');
            }
        }
        else{
            throw new ApplicationException('Error occured. Please try again later.');
        }
    }
}
