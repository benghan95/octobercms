<?php namespace IG\Banner\Components;

use Cms\Classes\ComponentBase;
use IG\Banner\Models\Banner;

class HomeBanner extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'HomeBanner Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $banners = Banner::where('banner_location', 'home-banner')
                         ->displayable()
                         ->get();
        
        $this->page['banners'] = $banners;
    }
}
