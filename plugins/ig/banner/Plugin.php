<?php namespace IG\Banner;

use Backend;
use System\Classes\PluginBase;

/**
 * Banner Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Banner',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-bullhorn'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IG\Banner\Components\HomeBanner' => 'homeBanner',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'ig.banner.some_permission' => [
                'tab' => 'Banner',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'banner' => [
                'label'       => 'Banner',
                'url'         => Backend::url('ig/banner/banner'),
                'icon'        => 'icon-bullhorn',
                'permissions' => ['ig.banner.*'],
                'order'       => 160,
            ],
        ];
    }
}
