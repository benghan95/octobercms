<?php namespace IG\Banner\Models;

use Model;
use Carbon\Carbon;

/**
 * Banner Model
 */
class Banner extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_banner_banners';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'author' => ['Backend\Models\User', 'key' => 'created_by'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'banner_image'        => 'System\Models\File',
        'mobile_banner_image' => 'System\Models\File',
    ];
    public $attachMany = [];

    public $attributes = [
        'banner_location' => '',
    ];

    public function beforeSave() {
        $this->created_by = \BackendAuth::getUser()->id;
        if ($this->start_date)
            $this->start_date = \Carbon\Carbon::parse($this->start_date)->setTime(0, 0, 0);

        if($this->start_date == null)
            $this->start_date = Carbon::now('Asia/Kuala_Lumpur');

        if ($this->end_date)
            $this->end_date = \Carbon\Carbon::parse($this->end_date)->setTime(0, 0, 0);
    }

    public function scopeDisplayable($query){
        $dt = Carbon::now('Asia/Kuala_Lumpur');
        $dt->addDay(1);

        return $query->where('status', '=', 1)
                     ->where('start_date', '<=', $dt->toDateString())
                     ->where(function($query) use ($dt){
                        $query->where('end_date', NULL)
                              ->orWhere('end_date', '>=', $dt->toDateString());
                     });
    }


    public static function getBannerLocationOptions()
    {
        return[
            'home-banner' => 'Homepage Banner',
        ];
    }

    public function getBannerLocationAttribute()
    {
        $result = $this->attributes['banner_location'];
        $options = $this->getBannerLocationOptions();

        foreach($options as $key=>$value)
        {
            if($key == $result)
            {
                return $value;
            }
        }
    }
}
