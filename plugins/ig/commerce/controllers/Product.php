<?php namespace IG\Commerce\Controllers;

use stdClass;
use Excel;
use Input;
use Response;
use Redirect;
use Storage;
use BackendMenu;
use Carbon\Carbon;
use Backend\Classes\Controller;
use IG\Commerce\Models\Product as ProductModel;
use IG\Commerce\Models\ProductOption;
use IG\Commerce\Models\Category;
use IG\Commerce\Controllers\ProductExport;
use October\Rain\Exception\ApplicationException;

/**
 * Product Back-end Controller
 */
class Product extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Backend.Behaviors.ImportExportController',
    ];

    public $formConfig         = 'config_form.yaml';
    public $listConfig         = 'config_list.yaml';
    public $relationConfig     = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('IG.Commerce', 'commerce', 'product');

        $this->addCss("/themes/demo/assets/vendor/sweetalert/sweetalert.css", "1.0.0");
        $this->addJs("/plugins/ig/commerce/assets/js/product.js", "1.0.0");
    }

    public function onProcess() {
        $products = ProductModel::all();
        foreach ($products as $product){
            $product->processTransientPrice();
        }

        return [
            'status' => 'success',
        ];
    }

    public function export(){
        $this->bodyClass = '';

        $this->pageTitle = 'Export Products';

        $categories = Category::active()->get();

        $this->vars['categories'] = $categories;

        $this->addCss("/plugins/ig/commerce/assets/css/custom.css", "1.0.0");
        $this->addJs("/plugins/ig/commerce/assets/js/custom.js", "1.0.0");

        $path = Input::get("path");

        if ($path){
            return Response::download($path);
        }
    }

    public function onSuccessExport(){
        Storage::deleteDirectory('media/exports/');
    }

    public function onExportRecords(){
        $product_category = post('export_category');

        if($product_category == ""){
            throw new ApplicationException('Please select the category of products that you would like to export.');
            return;
        }

        $filename = preg_replace("/[^A-Za-z0-9]/i", "-", 'export-' . $product_category . '-' . Carbon::now('Asia/Kuala_Lumpur')->toDateTimeString()) . '.xlsx';

        // $productsArray = $this->productExport($product_category);

        $randomFolder1 = mt_rand(100, 999);
        $randomFolder2 = mt_rand(100, 999);
        $randomFolder3 = mt_rand(100, 999);

        $storagePath = '/media/exports/' . $randomFolder1 . '/' . $randomFolder2 . '/' . $randomFolder3 . '/';

        // $path = Excel::create($filename, function($excel) use ($productsArray) {
        //     $excel->sheet('Sheet 1', function($sheet) use ($productsArray) {
        //         $sheet->fromArray($productsArray);
        //     });
        // })->store('csv', storage_path($storagePath), true);

        $downloadPath = url('/') . '/storage/app' . $storagePath . $filename;

        Excel::store(new ProductExport($product_category), $storagePath . $filename);

        return Redirect::to($downloadPath);
    }
}
