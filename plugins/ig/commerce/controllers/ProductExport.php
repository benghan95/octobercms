<?php namespace IG\Commerce\Controllers;

use stdClass;
use BackendMenu;
use Carbon\Carbon;
use Backend\Classes\Controller;
use IG\Commerce\Models\Product as ProductModel;
use IG\Commerce\Models\ProductOption;
use IG\Commerce\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use October\Rain\Exception\ApplicationException;


class ProductExport extends Controller implements FromCollection, WithHeadings, WithStrictNullComparison
{
    use Exportable;

    private $category = null;

    public function __construct($product_category)
    {
        parent::__construct();

        $this->category = $product_category;

        BackendMenu::setContext('IG.Commerce', 'commerce', 'productexport');
    }

    public function collection()
    {
        $productsArray = $this->productExport($this->category);
        return collect([
            $productsArray
        ]);
    }

    public function headings(): array
    {
        return [
            'ID',
            'PARENT ID',
            'PRODUCT NAME',
            'HAS COLOUR',
            'SINGLE PRODUCT',
            'IN SET',
            'DESCRIPTION',
            'SKU',
            'SALE LABEL',
            'CATEGORY',
            'SUBCATEGORY',
            'IS POPULAR',
            'IS CHEAPEST',
            'IS NEW',
            'SALE START',
            'SALE END',
            'CAN BE DISPLAYED',
            'AVAILABILITY',
            'WEIGHT',
            'DEFAULT SELECTED',
            'DISPLAY ORDER',
            'STOCK',
            'RESERVED',
            'EXPECTED STOCK',
            'RETAIL PRICE',
            'IG PREMIUM PRICE',
            'SALE PRICE',
            'IMAGE NAME',
        ];
    }

    private function productExport($category){
        $productCategory = Category::where('slug', $category)->first();
        if (!$productCategory) {
            throw new ApplicationException('The product category is not valid.');
            return;
        }

        $products = $productCategory->products;

        $productsArray = [];

        foreach ($products as $product){
            if ($product->product_type == 'NORMAL') {
                $productArray = $this->generateProductArray($product);
                array_push($productsArray, $productArray);
            } else {
                $productArray = $this->generateProductArray($product);
                array_push($productsArray, $productArray);

                $options = $product->options;
                if (count($options) > 0) {
                    foreach ($options as $option){
                        $optionArray = $this->generateProductOptionArray($productArray, $option);
                        array_push($productsArray, $optionArray);
                    }
                }
            }
        }

        return $productsArray;
    }

    private function generateProductArray($product){
        $retail_price     = 0;
        $ig_price         = 0;
        $sale_price       = 0;
        $weight           = 0;
        $display_order    = 0;
        $in_stock         = 0;
        $reserved_stock   = 0;
        $expected_stock   = 0;
        $has_color        = null;
        $single_product   = "No";
        $in_set           = "No";
        $default_selected = null;

        $sale_start_date = null;
        if ($product->sale_start){
            $sale_start_date = Carbon::parse($product->sale_start)->format('d/m/Y');
        }

        $sale_end_date = null;
        if ($product->sale_end){
            $sale_end_date = Carbon::parse($product->sale_end)->format('d/m/Y');
        }

        $sale_labels = null;
        if($product->sale_label) {
            foreach ($product->sale_label as $key=>$label) {
                if ($key == 0) {
                    $sale_labels = $label;
                } else {
                    $sale_labels .= ',' . $label;
                }
            }
        }

        $saleStart = null;
        if ($product->sale_start)
            $saleStart = Carbon::parse($product->sale_start)->format('d/m/Y');

        $saleEnd = null;
        if ($product->sale_start)
            $saleEnd = Carbon::parse($product->sale_start)->format('d/m/Y');


        $product_type  = $product->product_type;
        $single_option = new stdClass;
        $single_stock  = new stdClass;

        if ($product_type == 'NORMAL') {
            if (count($product->options) > 0) {
                $single_option    = $product->options[0];

                $single_stock     = $single_option->stock;
                $default_selected = $single_option->default_selected ? "Yes" : "No";
                $display_order    = $single_option->sort_order;

                if ($single_option->pricing) {
                    $retail_price = $single_option->pricing->rrp;
                    $ig_price     = $single_option->pricing->price;
                    $sale_price   = $single_option->pricing->sale_price;
                }

                if ($single_stock) {
                    if ($single_stock->color) {
                        $has_color = $single_stock->color->title;
                    }
                    $weight         = $single_stock->weight;
                    $in_stock       = $single_stock->in;
                    $reserved_stock = $single_stock->reserved;
                    $expected_stock = $single_stock->expected;
                }
            }

            $single_product = "Yes";
            $in_set         = "No";
        } else if ($product_type == 'COLOR') {
            $single_product = "No";
            $in_set         = "No";
        } else if ($product_type == 'BUNDLE') {
            $single_product = "No";
            $in_set         = "Yes";
        } else if ($product_type == 'SET') {
            $single_product = "Yes";
            $in_set         = "Yes";
        } else {
            $single_product = "No";
            $in_set         = "No";
        }

        $rowArray = array();

        $rowArray['ID']               = $product->ref_id;
        $rowArray['PARENT ID']        = '0';
        $rowArray['PRODUCT NAME']     = $product->title;
        $rowArray['HAS COLOUR']       = $has_color;
        $rowArray['SINGLE PRODUCT']   = $single_product;
        $rowArray['IN SET']           = $in_set;
        $rowArray['DESCRIPTION']      = $product->description;
        $rowArray['SKU']              = $product->sku;
        $rowArray['SALE LABEL']       = $sale_labels;
        $rowArray['CATEGORY']         = $product->category->title;
        $rowArray['SUBCATEGORY']      = $product->subcategory->title;
        $rowArray['IS POPULAR']       = $product->is_popular ? "Yes" : "No";
        $rowArray['IS CHEAPEST']      = $product->is_cheapest ? "Yes" : "No";
        $rowArray['IS NEW']           = $product->is_new ? "Yes" : "No";
        $rowArray['SALE START']       = $sale_start_date;
        $rowArray['SALE END']         = $sale_end_date;
        $rowArray['CAN BE DISPLAYED'] = $product->status ? "Yes" : "No";
        $rowArray['AVAILABILITY']     = $product->available ? "Yes" : "No";
        $rowArray['WEIGHT']           = $weight;
        $rowArray['DEFAULT SELECTED'] = $default_selected;
        $rowArray['DISPLAY ORDER']    = $display_order;
        $rowArray['STOCK']            = $in_stock;
        $rowArray['RESERVED']         = $reserved_stock;
        $rowArray['EXPECTED STOCK']   = $expected_stock;
        $rowArray['RETAIL PRICE']     = $retail_price;
        $rowArray['IG PREMIUM PRICE'] = $ig_price;
        $rowArray['SALE PRICE']       = $sale_price;
        $rowArray['IMAGE NAME']       = $product->image_filename;

        return $rowArray;
    }

    private function generateProductOptionArray($productArray, $option){
        $retail_price     = 0;
        $ig_price         = 0;
        $sale_price       = 0;
        $weight           = 0;
        $display_order    = 0;
        $in_stock         = 0;
        $reserved_stock   = 0;
        $expected_stock   = 0;
        $has_color        = null;
        $single_product   = null;
        $in_set           = null;
        $default_selected = "No";

        $optionId = $option->ref_id;
        $parentId = $productArray['ID'];
        if (!$optionId) {
            $all_options    = ProductOption::all()->sortByDesc('ref_id')->first();
            $option->ref_id = $all_options + 1;
            $option->save();
            $optionId = $option->ref_id;
        }

        $display_order    = $option->sort_order;
        $default_selected = $option->default_selected ? "Yes" : "No";
        if ($option->stock) {
            $stock          = $option->stock;
            $weight         = $stock->weight;
            $in_stock       = $stock->in;
            $reserved_stock = $stock->reserved;
            $expected_stock = $stock->expected;

            if ($stock->color) {
                $has_color = $stock->color->title;
            }
            if ($option->pricing) {
                $pricing      = $option->pricing;
                $retail_price = $pricing->rrp;
                $ig_price     = $pricing->price;
                $sale_price   = $pricing->sale_price;
            }
        }

        $rowArray = $productArray;

        $rowArray['ID']               = $optionId;
        $rowArray['PARENT ID']        = $parentId;
        $rowArray['PRODUCT NAME']     = $option->title;
        $rowArray['HAS COLOUR']       = $has_color;
        $rowArray['SINGLE PRODUCT']   = $single_product;
        $rowArray['IN SET']           = $in_set;
        $rowArray['DESCRIPTION']      = null;
        $rowArray['SKU']              = $option->sku;
        $rowArray['SALE LABEL']       = null;
        $rowArray['CATEGORY']         = null;
        $rowArray['SUBCATEGORY']      = null;
        $rowArray['IS POPULAR']       = null;
        $rowArray['IS CHEAPEST']      = null;
        $rowArray['IS NEW']           = null;
        $rowArray['SALE START']       = null;
        $rowArray['SALE END']         = null;
        $rowArray['CAN BE DISPLAYED'] = $option->status ? "Yes" : "No";
        $rowArray['AVAILABILITY']     = $option->available ? "Yes" : "No";
        $rowArray['WEIGHT']           = $weight;
        $rowArray['DEFAULT SELECTED'] = $default_selected;
        $rowArray['DISPLAY ORDER']    = $display_order;
        $rowArray['STOCK']            = $in_stock;
        $rowArray['RESERVED']         = $reserved_stock;
        $rowArray['EXPECTED STOCK']   = $expected_stock;
        $rowArray['RETAIL PRICE']     = $retail_price;
        $rowArray['IG PREMIUM PRICE'] = $ig_price;
        $rowArray['SALE PRICE']       = $sale_price;
        $rowArray['IMAGE NAME']       = $option->image_filename;

        return $rowArray;
    }
}