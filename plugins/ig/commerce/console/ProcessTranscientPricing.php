<?php namespace IG\Commerce\Console;

use IG\Commerce\Models\Product;
use IG\Transact\Models\OrderItem;
use Illuminate\Console\Command;

class ProcessTranscientPricing extends Command {

    protected $name = 'product:pricing';
    protected $description = "Process transient pricing";
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $products = Product::all();
        foreach ($products as $product){
            $product->processTransientPrice();
        }
    }
}