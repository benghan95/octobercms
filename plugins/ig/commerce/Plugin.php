<?php namespace IG\Commerce;

use Backend;
use System\Classes\PluginBase;
use IG\Commerce\Models\Product;

/**
 * Commerce Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Commerce',
            'description' => 'No description provided yet...',
            'author'      => 'IG',
            'icon'        => 'icon-shopping-cart'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('product.pricing', 'IG\Commerce\Console\ProcessTranscientPricing');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('offline.sitesearch.query', function ($query) {

            // Search your plugin's contents
            $items = Product::active()
                            ->where(function ($q) use ($query){
                                $q->where('title', 'like', "%${query}%")
                                  ->orWhere('description', 'like', "%${query}%");
                            })
                            ->orderBy('sale_count', 'desc')
                            ->orderBy('available', 'desc')
                            ->get()->take(20);

            // Now build a results array
            $results = $items->map(function ($item) use ($query) {

                // If the query is found in the title, set a relevance of 2
                $relevance = mb_stripos($item->title, $query) !== false ? 2 : 1;

                return [
                    'title'     => $item->title,
                    'text'      => $item->description,
                    // 'url'       => 'http://103.6.198.130/~igpremiu/product/' . $item->slug,
                    'url'       => $item->slug,
                    // 'url'       => '' . url('/product/' . $item->slug),
                    'thumb'     => $item->thumb, // Instance of System\Models\File
                    'relevance' => $relevance, // higher relevance results in a higher
                    // position in the results listing
                    // 'meta' => 'data',       // optional, any other information you want
                    // to associate with this result
                    'model' => $item,       // optional, pass along the original model
                ];
            });

            return [
                'provider' => 'Product', // The badge to display for this result
                'results'  => $results,
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'IG\Commerce\Components\Placeholder'         => 'placeholder',
            'IG\Commerce\Components\ProductView'         => 'productView',
            'IG\Commerce\Components\HomeProductsListing' => 'homeProductsListing',
            'IG\Commerce\Components\HotDeals'            => 'hotDeals',
            'IG\Commerce\Components\Catalogues'          => 'catalogues',
            'IG\Commerce\Components\NewArrivals'         => 'newArrivals',
            'IG\Commerce\Components\CategoriesList'      => 'categoriesList',
            'IG\Commerce\Components\ProductsListing'     => 'productsListing',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'ig.commerce.manage_products' => [
                'tab' => 'Commerce',
                'label' => 'Manage Products'
            ],
            'ig.commerce.manage_catalogues' => [
                'tab' => 'Commerce',
                'label' => 'Manage Catalogues'
            ],
            'ig.commerce.manage_categories' => [
                'tab' => 'Commerce',
                'label' => 'Manage Categories'
            ],
            'ig.commerce.manage_subcategories' => [
                'tab' => 'Commerce',
                'label' => 'Manage Subcategories'
            ],
            'ig.commerce.manage_pricings' => [
                'tab' => 'Commerce',
                'label' => 'Manage Pricings'
            ],
            'ig.commerce.manage_productoptions' => [
                'tab' => 'Commerce',
                'label' => 'Manage Product Options'
            ],
            'ig.commerce.manage_colors' => [
                'tab' => 'Commerce',
                'label' => 'Manage Colors'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'commerce' => [
                'label'       => 'Commerce',
                'url'         => Backend::url('ig/commerce/product'),
                'icon'        => 'icon-shopping-cart',
                'permissions' => ['ig.commerce.*'],
                'order'       => 120,
                'sideMenu'    => [
                    'product' => [
                        'label'       => 'Products',
                        'url'         => Backend::url('ig/commerce/product'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_products'],
                        'order'       => 100,
                    ],
                    'stock' => [
                        'label'       => 'Stocks',
                        'url'         => Backend::url('ig/commerce/stock'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_stocks'],
                        'order'       => 100,
                    ],
                    'catalogue' => [
                        'label'       => 'Catalogues',
                        'url'         => Backend::url('ig/commerce/catalogue'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_catalogues'],
                        'order'       => 100,
                    ],
                    'category' => [
                        'label'       => 'Categories',
                        'url'         => Backend::url('ig/commerce/category'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_categories'],
                        'order'       => 200,
                    ],
                    'subcategory' => [
                        'label'       => 'Subcategories',
                        'url'         => Backend::url('ig/commerce/subcategory'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_subcategories'],
                        'order'       => 300,
                    ],
                    'color' => [
                        'label'       => 'Colors',
                        'url'         => Backend::url('ig/commerce/color'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['ig.commerce.manage_colors'],
                        'order'       => 300,
                    ],
                ]
            ],
        ];
    }
}
