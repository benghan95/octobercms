<?php namespace IG\Commerce\Models;

use Config;
use Model;
use Carbon\Carbon;
use IG\User\Models\User;
use IG\Core\Models\Currency;
use IG\Commerce\Models\Pricing;
use IG\Commerce\Models\Stock;
use IG\Commerce\Models\Color;
use IG\Transact\Classes\Hexing;
use RainLab\User\Facades\Auth;
use October\Rain\Exception\ApplicationException;

/**
 * ProductOption Model
 */
class ProductOption extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_product_options';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'pricing' => ['IG\Commerce\Models\Pricing', 'key' => 'option_id'],
    ];
    public $hasMany = [];
    public $belongsTo = [
        'stock'   => ['IG\Commerce\Models\Stock', 'key' => 'stock_id'],
        'product' => ['IG\Commerce\Models\Product', 'key' => 'product_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'product_image' => 'System\Models\File',
    ];
    public $attachMany = [];

    public function getNewColorOptions() {
        $color = Color::all();

        $options = [];
        foreach ($color as $item){
            $options[$item->id] = $item->title . ' - ' . $item->color_code;
        }

        return $options;
    }

    public function getCreateColorOptions() {
        $color = Color::all();

        $options = [];
        foreach ($color as $item){
            $options[$item->id] = $item->title . ' - ' . $item->color_code;
        }

        return $options;
    }

    public function getCreateStockOptions() {
        $stocks = Stock::all();

        $options = [];
        foreach ($stocks as $stock){
            if ($stock->color) {
                $options[$stock->id] = '# ' . $stock->id . ' - ' . $stock->color->title;
            } else {
                $options[$stock->id] = '# ' . $stock->id;
            }

            // $options[$stock->id] .=  ' - IN (' . $stock->in . ') - EXPECTED (' . $stock->expected . ') - RESERVERD (' . $stock->reserved . ')';

            $options[$stock->id] .= ' (Used: ';
            $counter = 0;
            foreach ($stock->options as $option) {
                if ($counter == 0) {
                    if ($option->title) {
                        $options[$stock->id] .=  $option->title;
                    } else {
                        $options[$stock->id] .=  $option->sku;
                    }
                } else {
                    if ($option->title) {
                        $options[$stock->id] .=  ', ' . $option->title;
                    } else {
                        $options[$stock->id] .=  ', ' . $option->sku;
                    }
                }
                $counter ++;
            }
            $options[$stock->id] .= ')';
        }

        return $options;
    }

    public function beforeUpdate() {
        $modified = false;
        // if ($this->new_rrp == '')
        //     $this->new_rrp = $this->pricing->rrp;
        if ($this->new_price == '')
            $this->new_price = $this->pricing->price;
        if ($this->new_sale_price == '')
            $this->new_sale_price = $this->pricing->sale_price;
        if ($this->new_in == '')
            $this->new_in = $this->stock->in;
        if ($this->new_reserved == '')
            $this->new_reserved = $this->stock->reserved;
        if ($this->new_expected == '')
            $this->new_expected = $this->stock->expected;
        if ($this->new_weight == '')
            $this->new_weight = $this->stock->weight;

        if ($this->pricing) {
            $pricing = Pricing::find($this->pricing->id);

            if ($pricing) {
                // if ($pricing->rrp != $this->new_rrp) {
                //     $modified = true;
                //     $pricing->rrp = $this->new_rrp;
                // }
                if ($pricing->price != $this->new_price) {
                    $modified = true;
                    $pricing->price = $this->new_price;
                }
                if ($pricing->sale_price != $this->new_sale_price) {
                    $modified = true;
                    $pricing->sale_price = $this->new_sale_price;
                }

                if ($modified) {
                    foreach ($this->product->options as $option) {
                        if ($option->pricing) {
                            $option->pricing->price = $pricing->price;
                            $option->pricing->sale_price = $pricing->sale_price;
                            $option->pricing->save();
                        }
                    }
                    $pricing->save();
                    $this->product->processTransientPrice();
                }
            }
        }

        $modified = false;
        if ($this->stock) {
            $stock = Stock::find($this->stock->id);

            if ($stock) {
                if ($stock->in != $this->new_in) {
                    $modified = true;
                    $stock->in = $this->new_in;
                }
                if ($stock->reserved != $this->new_reserved) {
                    $modified = true;
                    $stock->reserved = $this->new_reserved;
                }
                if ($stock->expected != $this->new_expected) {
                    $modified = true;
                    $stock->expected = $this->new_expected;
                }
                if ($stock->weight != $this->new_weight) {
                    $modified = true;
                    $stock->weight = $this->new_weight;
                }

                if ($this->new_color) {
                    $modified = true;
                    $stock->color_id = $this->new_color;
                } else if ($this->new_color_title) {
                    $modified = true;
                    $slug     = $this->createSlug($this->new_color_title);
                    $exists   = Color::where('slug', $slug)->first();
                    if ($exists) {
                        $stock->color_id = $exists->id;
                    } else {
                        $color        = new Color();
                        $color->title = $this->new_color_title;
                        $color->slug  = $slug;
                        if ($this->new_color_code) {
                            $color->color_code = $this->new_color_code;
                        } else {
                            $color->color_code = Color::getColorCode($this->new_color_title);
                        }
                        $color->save();

                        $stock->color_id = $color->id;
                    }
                }

                if ($modified) {
                    $stock->save();
                }
            }
        }

        // unset($this->rrp);
        unset($this->product_stock);
        unset($this->color);
        unset($this->new_color);
        unset($this->new_color_title);
        unset($this->new_color_code);
        unset($this->price);
        unset($this->sale_price);
        unset($this->in);
        unset($this->reserved);
        unset($this->expected);
        unset($this->weight);
        // unset($this->new_rrp);
        unset($this->new_price);
        unset($this->new_sale_price);
        unset($this->new_in);
        unset($this->new_reserved);
        unset($this->new_expected);
        unset($this->new_weight);
    }

    public function beforeCreate() {
        $this->created_by = \BackendAuth::getUser()->id;

        $color_title = null;
        $color_code  = null;
        if ($this->create_color_title != '')
            $color_title = $this->create_color_title;
        if ($this->create_color_code != '')
            $color_code = $this->create_color_code;

        // if ($this->create_rrp == '')
        //     $this->create_rrp = 0;
        if ($this->create_price == '')
            $this->create_price = 0;
        if ($this->create_sale_price == '')
            $this->create_sale_price = 0;
        if ($this->create_in == '')
            $this->create_in = 0;
        if ($this->create_reserved == '')
            $this->create_reserved = 0;
        if ($this->create_expected == '')
            $this->create_expected = 0;
        if ($this->create_weight == '')
            $this->create_weight = 0;

        if (!$this->create_stock) {
            $stock = new Stock();
            $stock->color_id = null;
            if ($this->create_color) {
                $stock->color_id = $this->create_color;
            } else if ($color_title) {
                $slug = $this->createSlug($color_title);
                $exists = Color::where('slug', $slug)->first();
                if ($exists) {
                    throw new ApplicationException('Color exists! Please choose the color from dropdown.');
                    return;
                }

                $color        = new Color();
                $color->title = $color_title;
                $color->slug  = $slug;
                if ($color_code) {
                    $color->color_code = $color_code;
                } else {
                    $color->color_code = Color::getColorCode($color_title);
                }
                $color->save();

                $stock->color_id = $color->id;
            }
            $stock->weight     = $this->create_weight;
            $stock->in         = $this->create_in;
            $stock->expected   = $this->create_expected;
            $stock->reserved   = $this->create_reserved;
            $stock->sale_count = 0;
            $stock->save();

            $this->stock_id = $stock->id;
        } else {
            $this->stock_id = $this->create_stock;
        }

        $last_options = ProductOption::orderBy('ref_id', 'DESC')->first();

        $this->ref_id = $last_options->ref_id + 1;

        $pricing             = new Pricing();
        $pricing->option_id  = null;
        $pricing->rrp        = 0;
        $pricing->price      = $this->create_price;
        $pricing->sale_price = $this->create_sale_price;
        $pricing->temp       = $this->ref_id;
        $pricing->save();

        unset($this->create_color);
        unset($this->create_color_title);
        unset($this->create_color_code);
        unset($this->create_stock);
        // unset($this->create_rrp);
        unset($this->create_price);
        unset($this->create_sale_price);
        unset($this->create_weight);
        unset($this->create_in);
        unset($this->create_reserved);
        unset($this->create_expected);
    }

    public function afterCreate() {
        $pricing = Pricing::where('temp', $this->ref_id)->first();

        if ($pricing) {
            $pricing->option_id = $this->id;
            $pricing->save();
        }

        $this->hexhash = Hexing::hex($this->id);
        $this->save();
    }

    public function createSlug($str, $delimiter = '-'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

    public function beforeSave() {
    }

    public function getDisplayImageAttribute(){
        if ($this->product_image) {
            return $this->product_image->getPath();
        }
        if ($this->image_filename){
            return Config::get('app.url') . '/themes/demo/assets/images/product-images/' . $this->image_filename;
        }
        if (count($this->product_images) > 0)
            return $this->product_images[rand(0, count($this->product_images) - 1)]->getPath();
        else
            return "http://via.placeholder.com/235x250";
    }

    public function getActivePricingAttribute(){
        $user = Auth::getUser();

        $discount = 1;
        $is_agent = false;

        if ($user){
            $user = User::find($user->id);
            if ($user->agenttype){
                $is_agent = true;
                $discount = (1 - $user->agenttype->default_rebate / 100) * (1 - $user->agenttype->extra_rebate / 100);
            }
        }

        $pricing = [];
        if($this->pricing){
            if ($this->pricing->status){
                $total_rrp        = $this->pricing->rrp + $this->pricing->rrp_gst;
                $total_price      = $this->pricing->price + $this->pricing->price_gst;
                $total_sale_price = $this->pricing->sale_price + $this->pricing->sale_price_gst;

                $pricing['currency'] = Currency::getUserCurrency()->unit;
                $pricing['hexhash']  = $this->pricing->hexhash;
                $pricing['rrp']      = round($total_price * Currency::getUserCurrency()->rate * $discount, 2);
                $pricing['price']    = round($total_price * Currency::getUserCurrency()->rate * $discount, 2);
                $pricing['gst']      = $this->pricing->price_gst;

                if ($this->product->isInSale() && !$is_agent){
                    if ($this->pricing->sale_price > 0){
                        $pricing['price'] = round($total_sale_price  * Currency::getUserCurrency()->rate, 2);
                        $pricing['gst']   = $this->pricing->sale_price_gst;
                        $pricing['rrp']   = $this->pricing->price;
                    }
                }

                return $pricing;
            }
            else{
                return [
                    'currency' => Currency::getUserCurrency()->unit,
                    'hexhash'  => null,
                    'rrp'      => '0',
                    'price'    => '0',
                    'gst'      => '0'
                ];;
            }
        }
        else{
            return [
                'currency' => Currency::getUserCurrency()->unit,
                'hexhash'  => null,
                'rrp'      => '0',
                'price'    => '0',
                'gst'      => '0'
            ];
        }

        return [
            'currency' => Currency::getUserCurrency()->unit,
            'hexhash'  => null,
            'rrp'      => '0',
            'price'    => '0',
            'gst'      => '0'
        ];
    }
}
