<?php namespace IG\Commerce\Models;

use Model;
use IG\Transact\Classes\Hexing;

/**
 * Pricing Model
 */
class Pricing extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_pricings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function afterCreate() {
        $this->hexhash = Hexing::hex($this->id);
        $this->save();
    }
}
