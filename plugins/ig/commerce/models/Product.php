<?php namespace IG\Commerce\Models;

use Config;
use Model;
use Request;
use Carbon\Carbon;
use IG\Core\Models\Currency;
use IG\Transact\Classes\Hexing;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Subcategory;
use RainLab\User\Facades\Auth;

/**
 * Product Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_products';

    /**
     * @var float
     *
     * Price - RRP > 20% to show
     *
     */
    public $percentageToShowRRP = 0;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        // 'stocks' => ['IG\Commerce\Models\Stock', 'key' => 'product_id'],
        'options' => ['IG\Commerce\Models\ProductOption', 'key' => 'product_id'],
    ];
    public $belongsTo = [
        'author'      => ['Backend\Models\User', 'key' => 'created_by'],
        'category'    => ['IG\Commerce\Models\Category', 'key' => 'category_id'],
        'subcategory' => ['IG\Commerce\Models\Subcategory', 'key' => 'subcategory_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumb' => 'System\Models\File',
    ];
    public $attachMany = [
        'product_images' => 'System\Models\File',
    ];

    public $jsonable = [
        'sale_label'
    ];

    public function getHasStockAttribute(){
        foreach ($this->options as $option){
            if ($option->stock){
                if ($option->stock->in > 0)
                    return true;
            }
        }
        // foreach ($this->stocks as $stock){
        //     if ($stock->in > 0)
        //         return true;
        // }
        return false;
    }

    public function getProductOptionsAttribute(){
        $product_options = collect();
        foreach ($this->options as $option){
            $exists = false;
            foreach ($product_options as $item){
                if ($item->title == $option->title){
                    $exists = true;
                    break;
                }
            }
            if (!$exists)
                $product_options->push($option);
        }

        return $product_options;
    }

    public function getCategoryIdOptions(){
        $categories = Category::active()->get();
        $options = [];
        $options[""] = "--- Select a Category ---";
        foreach ($categories as $category){
            $options[$category->id] = $category->title;
        }

        return $options;
    }

    public function getSubcategoryIdOptions(){
        if($this->category_id){
            $category = Category::find($this->category_id);

            if ($category){
                $subcategories = $category->subcategories;
                $options = [];
                // $options[""] = "--- Select Subcategory ---";

                foreach ($subcategories as $category){
                    $options[$category->id] = $category->title;
                }

                if (!empty($options))
                    return $options;
                else
                    return ["" => "No Subcategory Found"];
            }
        }

        return ["" => "--- Select Category First ---"];
    }

    public function beforeSave() {
        if (\BackendAuth::getUser())
            $this->created_by = \BackendAuth::getUser()->id;

        if (!$this->category_id) {
            $this->category_id = null;
        }

        if (!$this->subcategory_id) {
            $this->subcategory_id = null;
        }

        if ($this->sale_start)
            $this->sale_start = \Carbon\Carbon::parse($this->sale_start)->setTime(0, 0, 0);

        if ($this->sale_end)
            $this->sale_end = \Carbon\Carbon::parse($this->sale_end)->setTime(0, 0, 0);

        if (!$this->subcategory_id){
            $this->subcategory_id = null;
        }
    }

    public function afterCreate() {
        $this->hexhash = Hexing::hex($this->id);
        $this->save();
    }

    public function scopeActive($query){
        return $query->where('status', true)->whereHas('options');
    }

    public static function active(){
        return static::where('status', true)->whereHas('options');
    }

    // public function getDefaultOptionAttribute(){
    //     $default = $this->options->where('default_selected', true)->first();

    //     if ($default){
    //         return $default;
    //     }
    //     else {
    //         if (count($this->options) > 0)
    //             return $this->options[0];
    //         else
    //             return null;
    //     }
    // }

    public function isInSale(){
        $current = Carbon::now('Asia/Kuala_Lumpur');

        if(!$this->sale_start && !$this->sale_start)
            return false;

        if ($this->sale_start && $this->sale_end) {
            if($this->sale_start <= $current && $this->sale_end >= $current)
                return true;
            else
                return false;
        }

        if ($this->sale_start && !$this->sale_end) {
            if ($this->sale_start <= $current)
                return true;
            else
                return false;
        }

        if (!$this->sale_start && $this->sale_end) {
            if ($this->sale_end >= $current)
                return true;
            else
                return false;
        }

        return false;
    }

    public function getSaleDiscountAttribute() {
        $highest = 0;
        if (count($this->options) > 0) {
            foreach ($this->options as $option) {
                if ($option->pricing) {
                    $pricing = $option->pricing;
                    if ($pricing->sale_price > 0) {
                        $discount = $pricing->sale_price / $pricing->price;
                        $discount_rate = round((1 - $discount) * 100);
                        if ($discount_rate > $highest) {
                            $highest = $discount_rate;
                        }
                    }
                }
            }

            return $highest;
        } else {
            return 0;
        }
    }

    public function getSmartsaleslabelAttribute(){
        if (count($this->sale_label) > 0)
            return $this->sale_label[rand(0, count($this->sale_label) - 1)];
    }

    public function getLowestPrice(){
        return $this->options->sortBy('price')->first()->finalPrice;
    }

    public function getHighestPrice(){
        return $this->options->sortByDesc('price')->first()->finalPrice;
    }

    public function getAllImagesAttribute(){
        $images = [];

        if (count($this->product_images) > 0){
            foreach ($this->product_images as $image){
                array_push($images, [
                    'hexhash' => null,
                    'url' => $image->getPath()
                ]);
            }
        } else if ($this->image_filename) {
            array_push($images, [
                'hexhash' => null,
                'url' => Config::get('app.url') . '/themes/demo/assets/images/product-images/' . $this->image_filename,
            ]);
        }

        if (count($this->options) > 0){
            foreach ($this->options as $option){
                array_push($images, [
                    'hexhash' => $option->hexhash,
                    'url' => $option->displayImage
                ]);
            }
        }

        return $images;
    }

    public function getDisplayImageAttribute(){
        if ($this->image_filename){
            return Config::get('app.url') . '/themes/demo/assets/images/product-images/' . $this->image_filename;
        }
        if (count($this->product_images) > 0)
            return $this->product_images[rand(0, count($this->product_images) - 1)]->getPath();
        else
            return "http://via.placeholder.com/235x250";
    }

    public function getDisplayPriceAttribute(){
        if (count($this->options) <= 0) {
            trace_log("Option not found! #" . $this->id . " (" . $this->title . ")");
        }
        $cheapest_option = $this->options[0];
        foreach ($this->options as $option){
            if ($option->activePricing['price'] < $cheapest_option->activePricing['price']
             && $option->activePricing['price'] != 0)
                $cheapest_option = $option;
        }

        return $cheapest_option->activePricing;
    }

    public function processTransientPrice(){
        $this->saveLowestHighestPrice();
    }

    private function saveLowestHighestPrice(){
        $gst = 1.00;
        $options = $this->options;
        $lowest_price  = 0;
        $highest_price = 0;
        foreach ($options as $option){
            $pricing = $option->pricing;

            if ($pricing){
                // $total_rrp        = $this->roundUpTo5Cents($pricing->rrp * $gst);
                $total_price      = $this->roundUpTo5Cents($pricing->price * $gst);
                $total_sale_price = $this->roundUpTo5Cents($pricing->sale_price * $gst);

                // $pricing->rrp_gst        = $total_rrp - $pricing->rrp;
                // $pricing->price_gst      = $total_price - $pricing->price;
                // $pricing->sale_price_gst = $total_sale_price - $pricing->sale_price;
                $pricing->rrp_gst      = 0;
                $pricing->price_gst      = 0;
                $pricing->sale_price_gst = 0;
                $pricing->save();

                if ($pricing->sale_price > 0){
                    if ($pricing->sale_price <= $lowest_price || $lowest_price == 0)
                        $lowest_price = $pricing->sale_price;

                    if ($pricing->sale_price >= $highest_price || $highest_price == 0)
                        $highest_price = $pricing->sale_price;
                }
                else{
                    if (($pricing->price <= $lowest_price || $lowest_price == 0) && $pricing->price != 0)
                        $lowest_price = $pricing->price;

                    if ($pricing->price >= $highest_price || $highest_price == 0)
                        $highest_price = $pricing->price;
                }
            }
        }

        $this->lowest_price  = $lowest_price;
        $this->highest_price = $highest_price;
        $this->save();
    }

    public function roundUpTo5Cents($value) {
        $valueInString = strval(round($value,2));
        if (strpos($valueInString, ".") == 0) $valueInString = $valueInString.".00";

        $valueArray = explode(".", $valueInString);
        $substringValue = substr($valueArray[1], 1);

        if ($substringValue >= 1 && $substringValue <= 5) {
            $tempValue = str_replace(substr($valueArray[1], 1), 5, substr($valueArray[1], 1));
            $tempValue = substr($valueArray[1],0,1).$tempValue;
            $newvalue = floatval($valueArray[0].".".$tempValue);
        } elseif ($substringValue == 0) {
            $newvalue = floatval($value);
        } else {
            $newFloat = floatval($valueArray[0].".".substr($valueArray[1],0,1));
            $newvalue = ($newFloat+0.1);
        }

        return $newvalue;
    }
}