<?php namespace IG\Commerce\Models;

use Model;

/**
 * Subcategory Model
 */
class Subcategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_subcategories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => ['IG\Commerce\Models\Product', 'key' => 'subcategory_id'],
    ];
    public $belongsTo = [
        'category' => ['IG\Commerce\Models\Category', 'key' => 'category_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function active(){
        return static::whereHas('products');
    }
}
