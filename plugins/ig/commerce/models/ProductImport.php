<?php namespace IG\Commerce\Models;

use Carbon\Carbon;
use Backend\Models\ImportModel;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\Pricing;
use IG\Commerce\Models\ProductOption;
use IG\Commerce\Models\Stock;
use IG\Commerce\Models\Color;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Subcategory;
use IG\Core\Models\Country;
use IG\Core\Models\Currency;
use IG\Core\Models\Store;
use IG\Core\Models\State;
use IG\Transact\Classes\Hexing;
use Illuminate\Support\Facades\Log;
use October\Rain\Exception\ApplicationException;

/**
 * TrackingCodeImport Model
 */
class ProductImport extends ImportModel
{
    public $currentRow;

    public $currentId;

    public $rules = [
        'id' => 'required'
    ];

    public function importData($results, $sessionKey = null)
    {
        foreach ($results as $row => $data) {
            $this->currentRow = $row + 1;

            if(!array_key_exists('ref_id', $data)){
                $this->logSkipped($row, $data['ref_id']);
                continue;
            }

            $this->currentId = $data['ref_id'];

            try {
                foreach ($data as $key=>$item){
                    $data[$key] = trim($item);
                }
                $this->createConfigurationsAndProducts($data);
                $this->logCreated();
            }
            catch (\Exception $ex) {
                $this->logError($row, $data['ref_id'].'::'.$ex->getMessage());
            }
        }
    }

    private function createConfigurationsAndProducts($data){
        if ($data['parent_id'] == null)
            $data['parent_id'] = 0;

        $product = null;
        if ($data['ref_id'] != null && $data['parent_id'] != null){
            if ($data['parent_id'] == 0)
                $product = $this->createProduct($data);
            else
                $option = $this->createProductOption($data);
        }

        if($product)
            $product->processTransientPrice();
        else if($option)
            $option->product->processTransientPrice();
    }

    private function createProduct($data){
        $refID = preg_replace("/[^0-9]/", "", iconv('windows-1250', 'utf-8', $data['ref_id']));
        $parentRefID = preg_replace("/[^0-9]/", "", iconv('windows-1250', 'utf-8', $data['parent_id']));

        if($parentRefID != "0")
            return null;

        $product = Product::where('ref_id', $refID)->first();

        $updating = true;
        if($product == null){
            $product = new Product();
            $updating = false;
        }

        $product->ref_id = $refID;
        $product->title = $data['product_name'];

        $slug = str_slug(iconv('windows-1250', 'utf-8', $data['product_name']));

        if(!$updating){
            $slugExists = Product::where('slug', $slug)->first();
            while($slugExists){
                $slug .= '-1';

                $slugExists = Product::where('slug', $slug)->first();
            }
            $product->slug = $slug;
        }
        else{
            $product->slug = str_slug($data['product_name']);
        }

        if ($data['in_set'] == 'Yes' && $data['single_product'] == 'Yes')
            $product->product_type = 'SET';
        else if ($data['in_set'] == 'Yes')
            $product->product_type = 'BUNDLE';
        else if ($data['single_product'] == 'Yes')
            $product->product_type = 'NORMAL';
        else
            $product->product_type = 'UNDEFINED';

        $product->description  = $data['description'];
        $product->sku          = $data['sku'];
        $product->sale_label   = $this->getSaleLabel($data['sale_label']);
        $product->published_at = Carbon::now();

        $category = $this->getCategory($data['category']);
        if($category)
            $product->category_id = $category->id;

        $subcategory = $this->getSubcategory($data['subcategory'], $category);
        if($subcategory)
            $product->subcategory_id = $subcategory->id;

        if ($data['is_new'] == 'Yes')
            $product->is_new = true;
        else
            $product->is_new = false;

        if ($data['is_popular'] == 'Yes')
            $product->is_popular = true;
        else
            $product->is_popular = false;

        if ($data['is_cheapest'] == 'Yes')
            $product->is_cheapest = true;
        else
            $product->is_cheapest = false;

        $product->sale_start = $this->validateDate($data['sale_start']);
        $product->sale_end = $this->validateDate($data['sale_end']);
        if ($data['status'] == 'Yes')
            $product->status = true;
        else
            $product->status = false;

        if ($data['available'] == 'Yes')
            $product->available = true;
        else
            $product->available = false;

        $product->image_filename = $data['image'];

        $product->save();

        if ($data['has_color']){
            $product->product_type = "COLOR";
            $product->save();

            $stock = $this->manageStock($data, $refID);
            $option = $this->manageOption($product, $stock, $data, $refID, $parentRefID);
            $pricing = $this->managePricing($option, $data);
        }
        else if ($product->product_type == 'NORMAL'){
            $stock = $this->manageStock($data, $refID);
            $option = $this->manageOption($product, $stock, $data, $refID, $parentRefID);
            $pricing = $this->managePricing($option, $data);
        }

        return $product;
    }

    private function createProductOption($data){
        $refID = preg_replace("/[^0-9]/", "", iconv('windows-1250', 'utf-8', $data['ref_id']));
        $parentRefID = preg_replace("/[^0-9]/", "", iconv('windows-1250', 'utf-8', $data['parent_id']));

        $product = Product::where('ref_id', $parentRefID)->first();
        $option = null;

        if($product == null){
            if ($this->currentRow && $this->currentId)
                $this->logError($this->currentRow, $this->currentId . '::' . 'Parent product not found during import:'. $parentRefID. ' for option:' . $refID);
            else
                Log::error('Parent product not found during import:' . $parentRefID. ' for option:' . $data['product_name']);

            return false;
        }

        $stock = $this->manageStock($data, $refID);

        if ($data['product_name'] == null && $data['has_color']){
            $product->product_type = "COLOR";
            $product->save();
        }
        else if ($data['product_name'] && $data['has_color'] == null){
            if ($product->product_type != 'BUNDLE' && $product->product_type != 'SET'){
                $product->product_type = "OPTION";
                $product->save();
            }
        }
        else if ($data['product_name'] && $data['has_color']){
            if ($product->product_type != 'BUNDLE' && $product->product_type != 'SET'){
                $product->product_type = 'OPTION';
                $product->save();
            }
        }
        else{
            $this->logError($this->currentRow, $this->currentId . '::' . 'Undefined Product:'. $parentRefID. ' for option:' . $refID);
        }

        $option = $this->manageOption($product, $stock, $data, $refID, $parentRefID);
        $pricing = $this->managePricing($option, $data);

        return $option;
    }

    private function manageOption($product, $stock, $data, $ref_id, $parent_id){
        if (!$stock)
            $this->logError($this->currentRow, $this->currentId . '::' . 'Undefined Stock:'. $ref_id. ' for parent:' . $parent_id);

        $option = ProductOption::where('ref_id', $ref_id)
                               ->where('parent_id', $parent_id)
                               ->where('product_id', $product->id)
                               ->where('stock_id', $stock->id)
                               ->first();

        if($option == null)
            $option = new ProductOption();

        $option->ref_id    = $ref_id;
        $option->parent_id = $parent_id;

        $option->product_id = $product->id;
        $option->stock_id   = $stock->id;
        if ($product->product_type != 'COLOR')
            $option->title      = $data['product_name'];

        $option->sku        = $data['sku'];
        $option->sort_order = $data['order'];
        if ($data['order'] == null)
            $option->sort_order = 0;

        if ($data['default_selected'] == 'Yes')
            $option->default_selected = true;
        else
            $option->default_selected = false;

        if ($data['status'] == 'Yes')
            $option->status = true;
        else
            $option->status = false;

        if ($data['available'] == 'Yes')
            $option->available = true;
        else
            $option->available = false;

        $option->image_filename = $data['image'];

        $option->save();

        return $option;
    }

    private function manageStock($data, $ref_id){
        $stock = Stock::where('ref_id', $ref_id)->first();

        if ($stock == null)
            $stock = new Stock();

        $stock->ref_id    = $ref_id;

        if ($data['has_color']){
            $color = $this->getColor($data['has_color']);
            $stock->color_id = $color->id;
        }
        else{
            $stock->color_id = null;
        }

        $stock->weight = $data['weight'];
        if ($data['weight'] == null)
            $stock->weight = 0;

        $stock->in = $data['stock'];
        if ($data['stock'] == null)
            $stock->in = 0;

        $stock->reserved = $data['reserved'];
        if ($data['reserved'] == null)
            $stock->reserved = 0;

        $stock->expected = $data['expected'];
        if ($data['expected'] == null)
            $stock->expected = 0;

        $stock->image_filename = $data['image'];
        $stock->save();

        return $stock;
    }

    private function managePricing($option, $data){
        $pricing = Pricing::where('option_id', $option->id)
                          ->where('status', true)
                          ->first();

        if($pricing == null)
            $pricing = new Pricing();

        $pricing->option_id = $option->id;

        if($data['rrp']){
            if ($data['rrp'] == 'C4P' || $data['rrp'] == 'Call Us' || $data['rrp'] == 'CALL US')
                $pricing->rrp = 0;
            else if($data['rrp'] == '-')
                $pricing->rrp = 0;
            else
                $pricing->rrp = str_replace(',','',$data['rrp']);
        }
        else{
            $pricing->rrp = 0;
        }

        if($data['price']){
            if ($data['price'] == 'C4P' || $data['price'] == 'Call Us' || $data['price'] == 'CALL US')
                $pricing->price = 0;
            else if($data['price'] == '-')
                $pricing->price = 0;
            else
                $pricing->price = str_replace(',','',$data['price']);
        }
        else{
            $pricing->price = 0;
        }

        if ($data['sale_price']){
            if ($data['sale_price'] == 'C4P' || $data['sale_price'] == 'Call Us' || $data['price'] == 'CALL US')
                $pricing->sale_price = 0;
            else if($data['sale_price'] == '-')
                $pricing->sale_price = 0;
            else
                $pricing->sale_price = str_replace(',','',$data['sale_price']);
        }
        else{
            $pricing->sale_price = 0;
        }

        $pricing->status = true;
        $pricing->save();

        return $pricing;
    }

    private function getColor($value){
        $value = trim($value);

        $color = Color::where('slug', str_slug($value))->first();

        if($color)
            return $color;

        if($value){
            $color = new Color();
            $color->title = $value;
            $color->slug = str_slug($value);
            $color->color_code = Color::getColorCode($value);
            $color->save();
        }
        else{
            return null;
        }

        return $color;
    }

    private function getSaleLabel($value){
        $value = trim($value);

        if ($value){
            $data = explode(',', $value);

            foreach ($data as $key=>$item){
                $data[$key] = trim($item);
            }

            return $data;
        }
        return null;
    }

    private function getCategory($value){
        $value = trim($value);
        $category = Category::where('slug', str_slug($value))->first();

        if($category)
            return $category;

        if($value){
            $category = new Category();
            $category->title = $value;
            $category->slug = str_slug($value);
            $category->save();
        }
        else{
            return null;
        }

        return $category;
    }

    private function getSubcategory($value, $category = null){
        $value = trim($value);
        $subcategory = Subcategory::where('slug', str_slug($value))->first();

        if($subcategory)
            return $subcategory;

        if($value){
            $subcategory = new Subcategory();
            $subcategory->title = $value;
            $subcategory->slug = str_slug($value);
            $subcategory->save();
        }
        else{
            return null;
        }

        if ($category)
            $subcategory->category = $category;

        $subcategory->save();

        return $subcategory;
    }

    private function validateDate($date)
    {
        $date = trim($date);

        if ($date){
            if ($this->compareDateFormat($date, 'n/j/Y')) {
                return \DateTime::createFromFormat('n/j/Y', $date)->format('Y-m-d 00:00:00');
            } else if ($this->compareDateFormat($date, 'd/m/Y')) {
                return \DateTime::createFromFormat('d/m/Y', $date)->format('Y-m-d 00:00:00');
            } else {
                return \DateTime::createFromFormat('m/d/Y', $date)->format('Y-m-d 00:00:00');
            }
        }

        return null;
    }
}
