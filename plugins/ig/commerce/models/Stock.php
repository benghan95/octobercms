<?php namespace IG\Commerce\Models;

use Config;
use Model;
use Carbon\Carbon;
use IG\Transact\Classes\Hexing;
use IG\User\Models\User;
use IG\Core\Models\Currency;
use IG\Commerce\Models\ProductOption;
use RainLab\User\Facades\Auth;

/**
 * Stock Model
 */
class Stock extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_stocks';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        // 'pricing' => ['IG\Commerce\Models\Pricing', 'key' => 'stock_id'],
    ];
    public $hasMany = [
        'options' => ['IG\Commerce\Models\ProductOption', 'key' => 'stock_id'],
    ];
    public $belongsTo = [
        'color' => ['IG\Commerce\Models\Color', 'key' => 'color_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'product_image' => 'System\Models\File',
    ];
    public $attachMany = [];

    public function afterCreate() {
        $this->hexhash = Hexing::hex($this->id);
        $this->save();
    }

    public function getDisplayImageAttribute(){
        if ($this->image_filename)
            return Config::get('app.url') . '/themes/demo/assets/images/product-images/' . $this->image_filename;

        if ($this->product_image)
            return $this->product_image->getPath();
        else
            return "http://via.placeholder.com/235x250";
    }

    // public function getActivePricingAttribute(){
    //     $user = Auth::getUser();

    //     $discount = 1;
    //     $is_agent = false;

    //     if ($user){
    //         $user = User::find($user->id);
    //         if ($user->agenttype){
    //             $is_agent = true;
    //             $discount = 1 - ($user->agenttype->rebate_rate / 100);
    //         }
    //     }

    //     $pricing = [];
    //     if($this->pricing){
    //         if ($this->pricing->status){
    //             $pricing['currency'] = Currency::getUserCurrency()->unit;
    //             $pricing['hexhash'] = $this->pricing->hexhash;
    //             $pricing['rrp'] = round($this->pricing->rrp * Currency::getUserCurrency()->rate * $discount);

    //             $pricing['price'] = round($this->pricing->price * Currency::getUserCurrency()->rate * $discount, 2);
    //             if ($this->pricing->sale_price > 0){
    //                 if ($this->product){
    //                     $today = Carbon::now();

    //                     $in_sale = false;
    //                     if ($this->product->sale_start && $this->product->sale_end){
    //                         if ($this->product->sale_start <= $today && $this->product->sale_end >= $today){
    //                             $in_sale = true;
    //                         }
    //                     }
    //                     else if ($this->product->sale_start){
    //                         if ($this->product->sale_start <= $today){
    //                             $in_sale = true;
    //                         }
    //                     }
    //                     else if ($this->product->sale_end){
    //                         if ($this->product->sale_end >= $today){
    //                             $in_sale = true;
    //                         }
    //                     }

    //                     if ($in_sale){
    //                         if ($is_agent)
    //                             $pricing['price'] = $this->pricing->sale_price * Currency::getUserCurrency()->rate;
    //                         else
    //                             $pricing['price'] = $this->pricing->sale_price * Currency::getUserCurrency()->rate * $discount;
    //                     }
    //                 }
    //             }
    //             return $pricing;
    //         }
    //         else{
    //             return [
    //                 'currency' => Currency::getUserCurrency()->unit,
    //                 'hexhash' => null,
    //                 'rrp' => '0',
    //                 'price' => '0'
    //             ];;
    //         }
    //     }
    //     else{
    //         return [
    //             'currency' => Currency::getUserCurrency()->unit,
    //             'hexhash' => null,
    //             'rrp' => '0',
    //             'price' => '0'
    //         ];
    //     }
    // }
}
