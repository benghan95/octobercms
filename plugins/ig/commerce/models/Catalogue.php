<?php namespace IG\Commerce\Models;

use Model;
use Carbon\Carbon;

/**
 * Catalogue Model
 */
class Catalogue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_catalogues';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumb' => 'System\Models\File',
        'file' => 'System\Models\File',
    ];
    public $attachMany = [];

    public function beforeSave(){
        if ($this->start_date)
            $this->start_date = Carbon::parse($this->start_date)->setTime(0, 0, 0);
        if ($this->end_date)
            $this->end_date = Carbon::parse($this->end_date)->setTime(0, 0, 0);
    }

    public static function active(){
        return static::where('status', true)
                     ->where(function($query){
                        $query->where(function($q){
                                    $q->where('start_date', null)
                                      ->where('end_date', null);
                                })
                              ->orWhere(function($q){
                                  $today = Carbon::now();
                                  $q->where('start_date', '<=', $today->toDateString())
                                    ->where('end_date', null);
                                })
                              ->orWhere(function($q){
                                  $today = Carbon::now();
                                  $q->where('start_date', null)
                                    ->where('end_date', '>=', $today->toDateString());
                                })
                              ->orWhere(function($q){
                                  $today = Carbon::now();
                                  $q->where('start_date', '<=', $today->toDateString())
                                    ->where('end_date', '>=', $today->toDateString());
                                });
                     });
    }

    public static function ended(){
        $today = Carbon::now();
        return static::where('status', true)
                     ->where('end_date', '<=', $today->toDateString());
    }
}
