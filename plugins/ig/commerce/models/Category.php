<?php namespace IG\Commerce\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'ig_commerce_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'subcategories' => ['IG\Commerce\Models\Subcategory', 'key' => 'category_id'],
        'products' => ['IG\Commerce\Models\Product', 'key' => 'category_id'],
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumb' => 'System\Models\File',
    ];
    public $attachMany = [];

    public static function active(){
        return static::whereHas('products');
    }
}
