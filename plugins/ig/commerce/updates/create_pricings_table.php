<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePricingsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_commerce_pricings', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('stock_id')->unsigned();
            $table->foreign('stock_id')->references('id')->on('ig_commerce_stocks');

            $table->float('rrp', 10, 2)->default(0.00);
            $table->float('price', 10, 2)->default(0.00);
            $table->float('sale_price', 10, 2)->default(0.00);

            $table->boolean('status')->default(true);

            $table->string('hexhash')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_commerce_pricings');
    }
}
