<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateSubcategoriesRelations extends Migration
{
    public function up()
    {
        Schema::table('ig_commerce_subcategories', function($table) {
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('ig_commerce_categories');
        });
    }

    public function down()
    {
        Schema::table('ig_commerce_subcategories', function($table) {
            $table->dropColumn('category_id');
        });
    }
}
