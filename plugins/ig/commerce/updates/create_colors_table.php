<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateColorsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_commerce_colors', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug')->unique();
            $table->string('color_code');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_commerce_colors');
    }
}
