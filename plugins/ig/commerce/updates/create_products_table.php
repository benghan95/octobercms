<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_commerce_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug')->unique();
            $table->text('description');
            $table->string('sku')->nullable();
            $table->string('sale_label')->nullable();
            $table->timestamp('published_at');

            $table->string('product_type');

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('ig_commerce_categories')->onDelete('SET NULL');

            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->foreign('subcategory_id')->references('id')->on('ig_commerce_subcategories')->onDelete('SET NULL');

            $table->boolean('is_new')->default(false);
            $table->boolean('is_popular')->default(false);
            $table->boolean('is_cheapest')->default(false);

            $table->timestamp('sale_start')->nullable();
            $table->timestamp('sale_end')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('available')->default(true);
            $table->integer('sale_count')->default(0);

            $table->float('lowest_price', 10, 2)->default(0.00);
            $table->float('highest_price', 10, 2)->default(0.00);

            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('backend_users');

            $table->string('hexhash')->nullable();

            $table->string('image_filename')->nullable();

            $table->integer('ref_id')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_commerce_products');
    }
}
