<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('ig_commerce_product_options', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('ig_commerce_products');

            $table->string('title');
            $table->string('sku')->nullable();
            $table->integer('sort_order')->default(0);
            $table->boolean('default_selected')->default(false);

            $table->boolean('status')->default(true);
            $table->boolean('available')->default(true);

            $table->string('hexhash')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('backend_users');

            $table->integer('ref_id')->nullable();
            $table->integer('parent_id')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_commerce_product_options');
    }
}
