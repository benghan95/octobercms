<?php namespace IG\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStocksTable extends Migration
{
    public function up()
    {
        Schema::create('ig_commerce_stocks', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('ig_commerce_products');

            $table->integer('option_id')->unsigned()->nullable();
            $table->foreign('option_id')->references('id')->on('ig_commerce_product_options');

            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('ig_commerce_colors');

            $table->float('weight', 10, 3)->nullable();

            $table->integer('in')->default(0);
            $table->integer('reserved')->default(0);
            $table->integer('expected')->default(0);
            $table->integer('sale_count')->default(0);

            $table->string('hexhash')->nullable();

            $table->string('image_filename')->nullable();

            $table->integer('ref_id')->nullable();
            $table->integer('parent_id')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ig_commerce_stocks');
    }
}
