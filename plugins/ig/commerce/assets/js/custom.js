$(document).ready(function(){
  $('form').on('ajaxSuccess', function(event, context, data) {
      switch (context.handler){
          case "onExportRecords":
                $('.popup-backdrop').removeClass('loading');
                $('.popup-backdrop .modal-content').removeClass('popup-loading-indicator');
                $('.modal-content').html("Export Successful!");
                $('.popup-backdrop').after('<div class="sweet-alert showSweetAlert visible" data-has-confirm-button="true" data-animation="pop" data-timer="null" style="display: block; margin-top: -171px;"><div class="sa-icon sa-success animate" style="display: block;"><span class="sa-line sa-tip animateSuccessTip"></span><span class="sa-line sa-long animateSuccessLong"></span><div class="sa-placeholder"></div><div class="sa-fix"></div></div><h2>success</h2><p style="display: block;">Products have been exported!</p><div class="sa-button-container"><div class="sa-confirm-button-container"><button class="confirm" tabindex="1" style="display: inline-block; background-color: rgb(140, 212, 245); box-shadow: rgba(140, 212, 245, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset;">OK</button><div class="la-ball-fall"></div></div></div></div>');

                $('.sweet-alert .confirm').on('click', function(){
                    $.request('onSuccessExport', {
                        success: function() {
                            $('.popup-backdrop').css("display", "none !important");
                            $('.sweet-alert.showSweetAlert').css("display", "none !important");
                            console.log('Deleted file!');
                            location.reload();
                        }
                    });
                });
            break;

          default:
              break;
      }
  });
});