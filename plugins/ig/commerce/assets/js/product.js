$('button').on('ajaxSuccess', function(event, context, res) {
  console.log(context.handler);
  console.log(res);
  if (context.handler == 'onProcess'){
    if(res.status == "success"){
      swal({
        title: "Success!",
        text : "Lowest Price & Highest Price of the products have been processed.",
        type : "success",
      });
    } else {
      swal({
        title: "Error!",
        text : "Error occured. Please try again later.",
        type : "error",
      });
    }
  }
});