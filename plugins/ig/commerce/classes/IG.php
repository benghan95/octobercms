<?php namespace IG\Commerce\Classes;

use IG\Commerce\Models\Pricing;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\ProductOption;
use IG\Commerce\Models\Stock;
use IG\Core\Models\OrderStatus;
use IG\Core\Models\Currency;
use IG\Transact\Classes\Hexing;
use IG\Transact\Models\Order;
use IG\Transact\Models\OrderItem;
use IG\Transact\Models\OrderItemStock;
use IG\Commerce\Models\Settings;
use Carbon\Carbon;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Facades\Auth;

class IG
{
    public static function create(){
        return self::_create();
    }

    private static function _create(){
        $order = Order::get_existing_order_or_createnew();

        if($order->user){
            $order->status_id = OrderStatus::pending()['id'];
            $order->save();
        }else{
            $order->status_id = OrderStatus::guest()['id'];
            $order->save();
        }

        $product_id = post('product_id');
        $quantity   = post('quantity');
        $options    = post('options');
        // $stocks     = post('stocks');

        $product = Product::where('hexhash', $product_id)->first();
        if($product == null)
            throw new ApplicationException('Product cannot be found.');

        if(count($options) <= 0)
            throw new ApplicationException('Please select an option to add to cart.');

        if(!$product->available)
            throw new ApplicationException($product->title. ' is not available!');

        if(!isset($product_id) && empty($product_id))
            throw new ApplicationException('Please refresh the page and try again');

        if($product->product_type == 'SET' 
        || $product->product_type == 'BUNDLE' 
        || $product->product_type == 'OPTION'){
            if(count($options) <= 0)
                throw new ApplicationException('Please select an option to add to cart.');
        }

        if ($quantity > 0)
            self::addOrderItem($order, $product_id, $options, $options, $quantity);

        return $order;
    }

    public static function addOrderItem($order, $product_id, $options, $options_hexhash, $quantity){
        $product = Product::where('hexhash', $product_id)->first();

        if (!$product){
            throw new ApplicationException('Item not found, please refresh and try again.');
        }

        $options = collect();

        foreach ($options_hexhash as $item){
            $option = ProductOption::where('hexhash', $item)->first();

            if (!$option)
                throw new ApplicationException('Option not found, please try again later.');

            if ($option->activePricing['price'] <= 0)
                throw new ApplicationException('Please call us at +06-3377 733 / 23 for pricing enquiries.');

            if ($option->stock->in <= 0)
                throw new ApplicationException('Item is out of stock, please call us at +06-3377 733 / 23 for any enquiries.');

            if ($quantity > $option->stock->in)
                throw new ApplicationException('Insufficient stock, please try to reduce the quantity.');

            $options->push($option);
        }

        $optionsList = '';
        if($product->product_type == 'SET' 
        || $product->product_type == 'BUNDLE' 
        || $product->product_type == 'OPTION'){
            foreach ($options as $key=>$item){
                if ($key == 0)
                    $optionsList .= $item->id;
                else
                    $optionsList .= '|' . $item->id;
            }
        }
        else{
            $optionsList .= $options[0]->id;
        }

        $compositeID = $product->id . "|" . $optionsList;
        $orderItemExist = self::checkItemExistInCart($order, $compositeID);

        $pricing = Pricing::where('hexhash', $options[0]->activePricing['hexhash'])->first();

        if($orderItemExist){
            if ($pricing)
                $orderItemExist->pricing_id = $pricing->id;

            foreach ($options as $option){
                if ($orderItemExist->quantity + $quantity > $option->stock->in)
                    throw new ApplicationException('Insufficient stock.');
            }

            $orderItemExist->currency    = Currency::getCurrencyUnit($options[0]->activePricing['currency']);
            $orderItemExist->unit_price  = $options[0]->activePricing['price'];
            $orderItemExist->gst         = $options[0]->activePricing['gst'];
            $orderItemExist->unit_weight = $options[0]->stock->weight;
            $orderItemExist->quantity    = $orderItemExist->quantity + $quantity;
            $orderItemExist->save();
        } else {
            $orderItem             = new OrderItem();
            $orderItem->order_id   = $order->id;
            $orderItem->product_id = $product->id;
            $orderItem->composite  = $compositeID;

            if ($pricing)
                $orderItem->pricing_id = $pricing->id;

            $orderItem->currency    = Currency::getCurrencyUnit($options[0]->activePricing['currency']);
            $orderItem->unit_price  = $options[0]->activePricing['price'];
            $orderItem->gst         = $options[0]->activePricing['gst'];
            $orderItem->unit_weight = $options[0]->stock->weight;
            $orderItem->quantity    = $quantity;
            $orderItem->save();

            foreach($options as $option){
                $itemstock               = new OrderItemStock();
                $itemstock->orderitem_id = $orderItem->id;
                $itemstock->option_id    = $option->id;
                $itemstock->stock_id     = $option->stock->id;
                $itemstock->save();
            }
        }

        $order->touch();
    }

    public static function lockPriceInOrderItem($order){
        $subtotal = 0;
        $orderitems = $order->orderitems;

        foreach($orderitems as $orderitem){
            $product = $orderitem->product;
            foreach ($orderitem->itemstocks as $item){
                $stock = $item->stock;
                if ($stock){
                    $stock->in = $stock->in - $orderitem->quantity;
                    $stock->save();
                }
            }
            $subtotal += $orderitem->quantity * round($orderitem->currency->rate * $orderitem->unit_price, 2);
        }

        $order->status_id = OrderStatus::pending_payment()['id'];
        $order->save();

        return $subtotal;
    }

    public static function checkItemExistInCart($order, $compositeID){
        $orderitems = $order->orderitems;

        foreach($orderitems as $orderitem){
            if($orderitem->composite == $compositeID)
                return $orderitem;
        }

        return false;
    }

    public static function retrieve(){
        $getOrder = Order::get_existing_order_or_createnew();

        $order = Order::find($getOrder->id);

        $order->promocode_id = null;
        $order->save();

        return $order;
    }
}