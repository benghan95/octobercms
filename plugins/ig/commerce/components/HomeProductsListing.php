<?php namespace IG\Commerce\Components;

use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Product;
use IG\Commerce\Models\Category;

class HomeProductsListing extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'HomeProductsListing Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $popular_products = Product::active()
                                   ->where('is_popular', true)
                                   ->get()
                                   ->take(24);

        $not_popular = Product::active()
                              ->where('is_popular', false)
                              ->where('sale_count', 'DESC')
                              ->get()
                              ->take(24 - ($popular_products->count()));

        $popular_products = $popular_products->merge($not_popular);

        $cheapest_products = Product::active()
                                    ->where('lowest_price', '>', 0.00)
                                    ->orderBY('lowest_price', 'ASC')
                                    ->get()
                                    ->take(24);

        $featured_categories = Category::active()
                                    ->where('featured', true)
                                    ->get()
                                    ->take(4);

        foreach ($featured_categories as $item) {
            $products = $item->products
                            ->where('status', true)
                            ->sortByDesc('sale_count')
                            ->sortByDesc('updated_at')
                            ->take(24);

            $active_products = collect();

            foreach ($products as $product) {
                if (count($active_products) >= 24) {
                    break;
                }
                if (count($product->options) > 0)
                    $active_products->push($product);
            }

            $item->active_products = $active_products;
        }


        $this->page['popular_products']    = $popular_products;
        $this->page['cheapest_products']   = $cheapest_products;
        $this->page['featured_categories'] = $featured_categories;
    }
}
