<?php namespace IG\Commerce\Components;

use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Product;

class CategoriesList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CategoriesList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $categories = Category::active()->get();

        $hasHotDeals = false;
        $hotDealsProducts = Product::active()->where(function($query){
            $query->where('sale_start', '<>', null)
                  ->orWhere('sale_end', '<>', null);
        })->get();

        if (count($hotDealsProducts) > 0)
            $hasHotDeals = true;

        $this->page['categories'] = $categories;
        $this->page['hasHotDeals'] = $hasHotDeals;
    }
}
