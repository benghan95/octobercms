<?php namespace IG\Commerce\Components;

use Config;
use Carbon\Carbon;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Color;
use IG\Commerce\Models\Subcategory;
use IG\User\Models\User;
use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Product;
use Illuminate\Support\Facades\Request;
use RainLab\User\Facades\Auth;

class HotDeals extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'hotDeals Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $today = Carbon::now('Asia/Kuala_Lumpur');

        if(Auth::check()) {
            $user = Auth::getUser();
            $user = User::find($user->id);

            if ($user->agenttype) {
                return $this->controller->run('404');
            }
        }

        extract($this->getProductListing(Request::ajax() === true ? 'post' : 'get' ));

        $this->page['products'] = $products;

        // TYPE
        $this->page['filter_categories'] = Category::with([
            'products' => function($q) use ($today) {
               return $q->active()->where(function($query) use ($today) {
                    $query->where(function($q) use ($today) {
                       $q->where('sale_start', '<>', null)
                         ->where('sale_start', '>=', $today);
                    })
                    ->orWhere(function($q) use ($today) {
                        $q->where('sale_end', '<>', null)
                        ->where('sale_end', '<=', $today);
                    });
               });
            }
        ])->groupBy('id')->get()->sortBy('title');

        // TYPE
        $this->page['subcategories'] = Subcategory::with([
            'products' => function($q) use ($today) {
                return $q->active()->where(function($query) use ($today) {
                    $query->where(function($q) use ($today) {
                       $q->where('sale_start', '<>', null)
                         ->where('sale_start', '>=', $today);
                    })
                    ->orWhere(function($q) use ($today) {
                        $q->where('sale_end', '<>', null)
                        ->where('sale_end', '<=', $today);
                    });
                });
            }
        ])->groupBy('id')->get()->sortBy('title');

        // COLORS
        $colors = Color::with([
            'stocks' => function($q) use ($today) {
                $q->whereHas('options', function($q) use ($today) {
                    return $q->whereHas('product', function($q) use ($today) {
                        $q->where(function($query) use ($today){
                            $query->where(function($q) use ($today) {
                               $q->where('sale_start', '<>', null)
                                 ->where('sale_start', '>=', $today);
                            })
                            ->orWhere(function($q) use ($today) {
                                $q->where('sale_end', '<>', null)
                                ->where('sale_end', '<=', $today);
                            });
                        });
                    });
                });
            }
        ])->groupBy('id')->get()->sortBy('title');

        foreach ($colors as $color){
            $products = Product::active()->where(function($query) use ($today) {
                                            $query->where(function($q) use ($today) {
                                               $q->where('sale_start', '<>', null)
                                                 ->where('sale_start', '>=', $today);
                                            })
                                            ->orWhere(function($q) use ($today) {
                                                $q->where('sale_end', '<>', null)
                                                ->where('sale_end', '<=', $today);
                                            });
                                         })
                                         ->whereHas('options', function($q) use ($color) {
                                             $q->whereHas('stock', function($q) use ($color){
                                                    $q->where('color_id', $color->id);
                                                }
                                             );
                                         })->get();

            $color->products = $products;
        }

        $this->page['colors'] = $colors;

        $this->page['inStock'] = Product::active()
                                        ->where(function($query) use ($today) {
                                            $query->where(function($q) use ($today) {
                                               $q->where('sale_start', '<>', null)
                                                 ->where('sale_start', '>=', $today);
                                            })
                                            ->orWhere(function($q) use ($today) {
                                                $q->where('sale_end', '<>', null)
                                                ->where('sale_end', '<=', $today);
                                            });
                                        })
                                        ->where('available', true)
                                        ->groupBy('id')->get()->sortBy('title');

        $this->page['outOfStock'] = Product::active()
                                            ->where(function($query) use ($today) {
                                                $query->where(function($q) use ($today) {
                                                   $q->where('sale_start', '<>', null)
                                                     ->where('sale_start', '>=', $today);
                                                })
                                                ->orWhere(function($q) use ($today) {
                                                    $q->where('sale_end', '<>', null)
                                                    ->where('sale_end', '<=', $today);
                                                });
                                            })
                                            ->where('available', false)
                                            ->groupBy('id')->get()->sortBy('title');

        $this->page['filters'] = $filters;
    }

    public function onProductListingFilter()
    {
        $_GET['page'] = 1;

        $productListing = $this->getProductListing('post');

        return View::make('boozeat.commerce::product_listing', ['products' => $productListing['products']]);
    }

    protected function getProductListing($method = 'get')
    {
        $filters = [];
        $today = Carbon::now();

        $keyword           = $method('keyword', null);     // type
        $colors            = $method('color', []);         // type
        $filter_categories = $method('category', []);      // type
        $subcategories     = $method('subcategory', []);   // type
        $priceRange        = $method('price', '0-500');
        $availability      = $method('availability', []);

        $allProducts  = Product::active()->where(function($query) use ($today) {
            $query->where(function($q) use ($today) {
                $q->where('sale_start', '<>', null)
                  ->where('sale_start', '>=', $today);
            })
                  ->orWhere(function($q) use ($today) {
                      $q->where('sale_end', '<>', null)
                        ->where('sale_end', '<=', $today);
                  });
        })->get();
        $productQuery = Product::active()->where(function($query) use ($today) {
                                            $query->where(function($q) use ($today) {
                                                $q->where('sale_start', '<>', null)
                                                  ->where('sale_start', '>=', $today);
                                            })
                                                  ->orWhere(function($q) use ($today) {
                                                      $q->where('sale_end', '<>', null)
                                                        ->where('sale_end', '<=', $today);
                                                  });
                                        });

        $highestPrice = 500;
        $highest = $allProducts->sortByDesc('highest_price')->first();

        if ($highest)
            $highestPrice = ceil($highest->highest_price);

        $priceRange = $method('price', '0-' . $highestPrice);
        $this->page['highestPrice'] = $highestPrice;

        $filters['filter_categories'] = $filter_categories;
        if ($filter_categories) {
            $productQuery->whereHas('category', function($q) use ($filter_categories) {
                return $q->whereIn('id', $filter_categories);
            });
        }

        $filters['subcategories'] = $subcategories;
        if ($subcategories) {
            $productQuery->whereHas('subcategory', function($q) use ($subcategories) {
                return $q->whereIn('id', $subcategories);
            });
        }

        $filters['keyword'] = $keyword;
        if ($keyword){
            $productQuery->where(function ($q) use ($keyword) {
                $q->where('title', 'LIKE', '%' . $keyword . '%')
                  ->orWhere('description', 'LIKE', '%' . $keyword . '%');
            });
        }

        $filters['colors'] = $colors;
        if ($colors) {
            $productQuery->whereHas('options', function($q) use ($colors) {
                $q->whereHas('stock', function($q) use ($colors){
                    return $q->whereIn('color_id', $colors);
                });
            });
            // ->whereHas('stocks', function($q) use ($colors) {
            //     return $q->whereIn('color_id', $colors);
            // });
        }

        // $filters['categories'] = $categories;
        // if ($categories) {
        //     $productQuery->whereHas('categories', function($q) use ($categories) {
        //         return $q->whereIn('category_id', $categories);
        //     });
        // }

        $filters['availability'] = $availability;
        if ($availability) {
            if (in_array('in-stock', $availability) && in_array('out-of-stock', $availability)){

            }
            else if (in_array('out-of-stock', $availability)){
                $productQuery->where('available', false);
            }
            else{
                $productQuery->where('available', true);
            }
        }

        list($lowest_price, $highest_price) = explode('-', $priceRange);
        $filters['price'] = ['l' => $lowest_price, 'h' => $highest_price];
        $productQuery->where(function($q) use ($lowest_price, $highest_price) {
            $q->where('lowest_price', '>=', $lowest_price)
              ->where('highest_price', '<=', $highest_price);
        });

        $productQuery = $productQuery->orderBy('available', 'DESC')
                                    ->orderBy('updated_at', 'DESC');

        $page = get('page', 1);
        if ($method == 'post') $page = 1;

        $products = $productQuery->paginate(24, $page);
        $products->setPath(Config::get('app.url') . '/hot-deals');

        $products->appends($method());

        return [
            'products' => $products,
            'filters'  => $filters
        ];
    }
}
