<?php namespace IG\Commerce\Components;

use Cms\Classes\ComponentBase;

class Placeholder extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Placeholder Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
