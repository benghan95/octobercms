<?php namespace IG\Commerce\Components;

use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Catalogue;

class Catalogues extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Catalogues Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        $active_catalogues = Catalogue::active()->get();
        $ended_catalogues = Catalogue::ended()->get();

        $this->page['active_catalogues'] = $active_catalogues;
        $this->page['ended_catalogues'] = $ended_catalogues;
    }
}
