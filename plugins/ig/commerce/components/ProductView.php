<?php namespace IG\Commerce\Components;

use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Product;

class ProductView extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductView Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){
        if ($slug = $this->param('slug') != null) {
            $product = Product::where('slug', $this->param('slug'))->first();
            if ($product->category) {
                $products = Product::active()->where('category_id', $product->category->id)->where('slug', '<>', $product->slug)->get()->take(6);
            } else {
                $products = Product::inRandomOrder()->active()->where('slug', '<>', $product->slug)->get()->take(6);
            }

            if( !$product ) {
                $this->setStatusCode(404);
                return $this->controller->run('404');
            }

            $this->page['title']            = $product->title;
            $this->page['meta_title']       = $product->title;
            $this->page['product']          = $product;
            $this->page['related_products'] = $products;
        }
    }
}
