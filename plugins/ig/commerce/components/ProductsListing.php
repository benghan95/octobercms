<?php namespace IG\Commerce\Components;

use Config;
use Response;
use Cms\Classes\ComponentBase;
use IG\Commerce\Models\Category;
use IG\Commerce\Models\Color;
use IG\Commerce\Models\Subcategory;
use IG\Commerce\Models\Product;
use Illuminate\Support\Facades\Request;

class ProductsListing extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ProductsListing Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun(){

        $categorySlug    = $this->param('category');
        $subcategorySlug = $this->param('subcategory');

        if (!$categorySlug) {
            return $this->controller->run('404');
        }

        $slug     = str_replace("-supplier-malaysia", "", $categorySlug);
        $category = Category::where('slug', $slug)->first();
        if (!$category) {
            return $this->controller->run('404');
        }

        $allProducts  = $category->products()->active()->get();
        $productQuery = $category->products()->active();
        $subcategory  = null;
        if ($subcategorySlug){
            $subcategory = Subcategory::where('slug', $subcategorySlug)->first();
            if (!$subcategory) {
                return $this->controller->run('404');
            }

            $allProducts = $subcategory->products()->active()->get();
            $productQuery = $subcategory->products()->active()->where('category_id', $category->id);
        }

        extract($this->getProductListing(Request::ajax() === true ? 'post' : 'get' ));

        $this->page['category']    = $category;
        $this->page['subcategory'] = $subcategory;
        $this->page['products']    = $products;
        $this->page['title']       = $category->title;
        $this->page['meta_title']  = $category->title;
        if ($subcategory){
            $this->page['title']       = $subcategory->title;
            $this->page['meta_title']  = $subcategory->title;
        }

        if ($subcategory){
            // COLORS
            $colors = Color::with([
                'stocks' => function($q) use ($category, $subcategory) {
                    $q->whereHas('options', function($q) use ($category, $subcategory) {
                        return $q->whereHas('product', function($q) use ($category, $subcategory) {
                                $q->where('category_id', $category->id)
                                ->where('subcategory_id', $subcategory->id);
                        });
                    });
                }
            ])->groupBy('id')->get()->sortBy('title');

            // $colors = Color::with([
            //     'stocks' => function($q) use ($category, $subcategory) {
            //         $q->with([
            //             'product' => function($qu) use ($category, $subcategory) {
            //                 return $qu->where('category_id', $category->id)
            //                           ->where('subcategory_id', $subcategory->id);
            //             }
            //         ]);
            //     }
            // ])->groupBy('id')->get()->sortBy('title');

            foreach ($colors as $color){
                $products = Product::active()->where('category_id', $category->id)
                                             ->where('subcategory_id', $subcategory->id)
                                             ->whereHas('options', function($q) use ($color) {
                                                 $q->whereHas('stock', function($q) use ($color){
                                                        $q->where('color_id', $color->id);
                                                    }
                                                 );
                                             })->get();

                $color->products = $products;
            }

            $this->page['colors'] = $colors;

            $this->page['inStock'] = Product::active()
                                            ->where('category_id', $category->id)
                                            ->where('subcategory_id', $subcategory->id)
                                            ->where('available', true)
                                            ->groupBy('id')->get()->sortBy('title');

            $this->page['outOfStock'] = Product::active()
                                                ->where('category_id', $category->id)
                                                ->where('subcategory_id', $subcategory->id)
                                                ->where('available', false)
                                                ->groupBy('id')->get()->sortBy('title');

            $this->page['filters'] = $filters;
        }
        else{
            // TYPE
            $this->page['subcategories'] = Subcategory::with([
                'products' => function($q) use ($category) {
                   return $q->active()->where('category_id', $category->id);
                }
            ])->groupBy('id')->get()->sortBy('title');

            // COLORS
            $colors = Color::with([
                'stocks' => function($q) use ($category) {
                    $q->whereHas('options', function($q) use ($category) {
                        return $q->whereHas('product', function($q) use ($category) {
                                $q->where('category_id', $category->id);
                        });
                    });
                }
            ])->groupBy('id')->get()->sortBy('title');

            foreach ($colors as $color){
                $products = Product::active()->where('category_id', $category->id)
                                             ->whereHas('options', function($q) use ($color) {
                                                 $q->whereHas('stock', function($q) use ($color){
                                                        $q->where('color_id', $color->id);
                                                    }
                                                 );
                                             })->get();

                $color->products = $products;
            }

            $this->page['colors'] = $colors;

            $this->page['inStock'] = Product::active()
                                            ->where('category_id', $category->id)
                                            ->where('available', true)
                                            ->groupBy('id')->get()->sortBy('title');

            $this->page['outOfStock'] = Product::active()
                                                ->where('category_id', $category->id)
                                                ->where('available', false)
                                                ->groupBy('id')->get()->sortBy('title');

            $this->page['filters'] = $filters;
        }
    }

    public function onProductListingFilter()
    {
        $_GET['page'] = 1;

        $productListing = $this->getProductListing('post');

        return View::make('ig.commerce::product_listing', ['products' => $productListing['products']]);
    }

    protected function getProductListing($method = 'get')
    {
        $filters = [];

        $categorySlug    = $this->param('category');
        $subcategorySlug = $this->param('subcategory');
        $keyword         = $method('keyword', null);     // type
        $colors          = $method('color', []);         // type
        $subcategories   = $method('subcategory', []);   // type
        $priceRange      = $method('price', '0-500');
        $availability    = $method('availability', []);

        if (!$categorySlug) {
            return $this->controller->run('404');
        }

        $slug     = str_replace("-supplier-malaysia", "", $categorySlug);
        $category = Category::where('slug', $slug)->first();
        if (!$category) {
            return $this->controller->run('404');
        }

        $allProducts  = $category->products()->active()->get();
        $productQuery = $category->products()->active();
        $subcategory  = null;
        if ($subcategorySlug){
            $subcategory = Subcategory::where('slug', $subcategorySlug)->first();
            if (!$subcategory) {
                return $this->controller->run('404');
            }

            $allProducts = $subcategory->products()->active()->get();
            $productQuery = $subcategory->products()->active()->where('category_id', $category->id);
        }

        $highestPrice = 500;
        $highest = $allProducts->sortByDesc('highest_price')->first();

        if ($highest)
            $highestPrice = ceil($highest->highest_price);

        $priceRange = $method('price', '0-' . $highestPrice);
        $this->page['highestPrice'] = $highestPrice;

        $filters['subcategories'] = $subcategories;
        if ($subcategories) {
            $productQuery->whereHas('subcategory', function($q) use ($subcategories) {
                return $q->whereIn('id', $subcategories);
            });
        }

        $filters['keyword'] = $keyword;
        if ($keyword){
            $productQuery->where(function ($q) use ($keyword) {
                $q->where('title', 'LIKE', '%' . $keyword . '%')
                  ->orWhere('description', 'LIKE', '%' . $keyword . '%');
            });
        }

        $filters['colors'] = $colors;
        if ($colors) {
            $productQuery->whereHas('options', function($q) use ($colors) {
                $q->whereHas('stock', function($q) use ($colors){
                    return $q->whereIn('color_id', $colors);
                });
            });
            // ->whereHas('stocks', function($q) use ($colors) {
            //     return $q->whereIn('color_id', $colors);
            // });
        }

        // $filters['categories'] = $categories;
        // if ($categories) {
        //     $productQuery->whereHas('categories', function($q) use ($categories) {
        //         return $q->whereIn('category_id', $categories);
        //     });
        // }

        $filters['availability'] = $availability;
        if ($availability) {
            if (in_array('in-stock', $availability) && in_array('out-of-stock', $availability)){

            }
            else if (in_array('out-of-stock', $availability)){
                $productQuery->where('available', false);
            }
            else{
                $productQuery->where('available', true);
            }
        }

        list($lowest_price, $highest_price) = explode('-', $priceRange);
        $filters['price'] = ['l' => $lowest_price, 'h' => $highest_price];
        $productQuery->where(function($q) use ($lowest_price, $highest_price) {
            $q->where('lowest_price', '>=', $lowest_price)
              ->where('highest_price', '<=', $highest_price);
        });

        $productQuery = $productQuery->orderBy('available', 'DESC')
                                    ->orderBy('updated_at', 'DESC');

        $page = get('page', 1);
        if ($method == 'post') $page = 1;

        $products = $productQuery->paginate(24, $page);
        if ($subcategory)
            $products->setPath(Config::get('app.url') . '/products/' . $category->slug . '-supplier-malaysia/' . $subcategory->slug);
        else
            $products->setPath(Config::get('app.url') . '/products/' . $category->slug . '-supplier-malaysia');

        $products->appends($method());

        return [
            'category'    => $category,
            'subcategory' => $subcategory,
            'products'    => $products,
            'filters'     => $filters
        ];
    }
}
