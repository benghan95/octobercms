$(document).ready(function(){
  $("#ipayPaymentID").val($('.payment-method.selected').attr('data-option'));

  $('.payment-method').on('click', function() {
      $('.payment-method').removeClass('selected');

      $(this).addClass('selected');

      $('#payment_method').val($(this).attr('data-option'));
      $("#ipayPaymentID").val($(this).attr('data-option'));
  });

  $('.enter-promo').on('click', function(e) {
    e.preventDefault();
    $('.promo-code-form').removeClass('hide');
  });

  $('.edit-product-modal .btn-remove').on('click', function(e) {
    e.preventDefault();

    var hash = $(this).parents('.edit-product-modal').find('input[name="orderitem"]').val();

    $.request('onDeleteOrderItem', {
      data   : { hash: hash },
      success: function(res){
        if(res.result == "success"){
          removeCartItem(res.deletedHash);
          updateCartItems(res.order, res.itemCount);
          $('a[data-hash="' + res.deletedHash + '"]').parents('.edit-product-modal').modal('hide');
          $('a[data-orderitem="' + res.deletedHash + '"]').parents('tr').remove();

          swal({
            title: "Removed!",
            text : "An order item has been removed from your cart.",
            type : "success",
          },
          function(){
            $('a[data-hash="' + res.deletedHash + '"]').parents('.edit-product-modal').remove();
            console.log(res);
            if (res.itemCount <= 0)
              location.reload();
          });
        }

        if(typeof res.X_OCTOBER_REDIRECT !== "undefined"){
          location.href = res.X_OCTOBER_REDIRECT;
        }
      }
    });
  });

  $('form').on('ajaxSuccess', function(event, context, res) {
    if (context.handler == 'onUpdateOrderItem'){
      if(res.status == "success"){
        updateCartItems(res.results.order, res.results.itemCount);
        updatePricingSummary(res.calculated);
        $('#productItem_' + res.orderitem.id).modal('hide');
        $('.orderitem-count-' + res.orderitem.id).html(res.orderitem.quantity);
        var price = res.calculated.currency.unit + (res.orderitem.quantity * res.orderitem.unit_price).toFixed(2);
        $('.price-' + res.orderitem.id).html(price);

        swal({
          title: "Success!",
          text : "You have updated an item in your cart.",
          type : "success",
        });
      }
      else if (res.status == "failed"){
        swal({
          title: "Failed!",
          text : res.error_message,
          type : "error",
        });
      }
    }
    else if (context.handler == 'onSubmit'){
      if(res.status == "success"){
        swal({
          title            : "Requesting Payment Authority",
          text             : "Please wait a moment, do not close your browser.",
          type             : "info",
          showCancelButton : false,
          showConfirmButton: false
        });

        $.request('onUpdateAmount',{
          success: function(response){
            $("#ipay_amount").val(response.grandtotal);
            $("#ipay_signature").val(response.signature);
            $("#product_desc").val(response.products_list);
            $("#ePayment").submit();
          }
        });
      }
    }
  });

  $('#different-billing').on('click', function() {
    if ($(this).is(':checked')) {
      $(this).val(1);
      // $(this).parents('.form-group').find('.fa-check-square').addClass('checked');

      $('.billing-address').removeClass('hide');
    } else {
        $(this).val(0);
        // $(this).parents('.form-group').find('.fa-check-square').removeClass('checked');

        $('.billing-address').addClass('hide');
    }
  });

  $('.apply-promocode').on('click', function(e){
    e.preventDefault();

    var promocode = $(this).siblings('input').val();
    if (promocode){
      $.request('onApplyPromocode', {
        data   : { promocode: promocode },
        success: function(response){
          console.log(response);
            if(response.result == "success"){
                $('.discount-amount').removeClass('hide');
                $('#discount_type').html(response.result.code);
                $('.promocode-message').addClass('success');
                $('.promocode-message').html('Promocode valid.');
                updatePricingSummary(response.calculated);
            }
            else{
              $('.promocode-message').html(response.error_message);
            }
          },
          error: function(response){
            $('.promocode-message').html('Error occured. Please try again later.');
            console.log(response);
        }
      });
    }
    else{
      $('.promocode-message').html('Please enter a valid promocode.');
      return;
    }
  });

  $('.selection-card .btn-delete-address').on('click', function(e) {
    e.stopPropagation();
    var row        = $(this).parents('tr');
    var hexhash    = row.data('option');
    var isShipping = false;

    var table = row.parents('table');
    if (table.hasClass('shipping-selections')){
      isShipping = true;
    }
    $.request('onRemoveAddress', {
      data   : {
        option    : row.data('option'),
        isShipping: isShipping
      },
      success: function(res){
        if (res.status == "success"){
          table.find('.selection-card[data-option="-1"]').trigger('click');
          $('tr[data-option="' + hexhash + '"]').remove();
          swal({
            title: "Success",
            text : res.message,
            type : "error",
          });
        }
        else if (res.status == "error"){
          swal({
            title: "Error",
            text : res.message,
            type : "error",
          });
        }
        updatePricingSummary(res.calculated);
      }
    });
  })

  $('.shipping-selections .selection-card .btn-edit-address').on('click', function(e) {
    e.stopPropagation();
    $('#shipping-address-container').removeClass('hidden');
    var row = $(this).parents('tr');
    $('#shipping_editted').val(row.data('option'));
    $('#name_shipping').val(row.data('name'));
    $('#phone_shipping').val(row.data('phone'));
    $('#companyname_shipping').val(row.data('company'));
    $('#address1_shipping').val(row.data('address_1'));
    $('#address2_shipping').val(row.data('address_2'));
    $('#country_shipping').val(row.data('country'));
    $('#city_shipping').val(row.data('city'));
    $('#postcode_shipping').val(row.data('postcode'));
    $("#country_shipping").trigger('change');
    setTimeout(function() {
      $('#state_shipping').val(row.data('state'));
      $('#state_shipping').trigger('change');
    }, 500);
  })

  $('.billing-selections .selection-card .btn-edit-address').on('click', function(e) {
    e.stopPropagation();
    $('#billing-address-container').removeClass('hidden');
    var row = $(this).parents('tr');
    $('#billing_editted').val(row.data('option'));
    $('#name_billing').val(row.data('name'));
    $('#phone_billing').val(row.data('phone'));
    $('#companyname_billing').val(row.data('company'));
    $('#address1_billing').val(row.data('address_1'));
    $('#address2_billing').val(row.data('address_2'));
    $('#country_billing').val(row.data('country'));
    $('#city_billing').val(row.data('city'));
    $('#postcode_billing').val(row.data('postcode'));
    $("#country_billing").trigger('change');
    setTimeout(function() {
      $('#state_billing').val(row.data('state'));
      $('#state_billing').trigger('change');
    }, 500);
  })

  $('.shipping-selections .selection-card').on('click', function(){
    var card = $(this);
    $.request('onAddressChange', {
        data   : {
          option: $(this).data('option'),
        },
        success: function(res){
          if (res.status == "success"){
            card.addClass(function() {
              if(card.hasClass("selected")) return "";
              return "selected";
            });
            card.siblings(".selection-card").removeClass("selected");
            $('#shipping_address_id').val(card.data('option'));

            if (card.data('option') == "-1"){
              $('#shipping-address-container').removeClass('hidden');
              $('#name_shipping').val('');
              $('#phone_shipping').val('');
              $('#companyname_shipping').val('');
              $('#address1_shipping').val('');
              $('#address2_shipping').val('');
              $('#country_shipping').val('');
              $('#state_shipping').val('');
              $('#city_shipping').val('');
              $('#postcode_shipping').val('');
            }
            else {
              $('#shipping-address-container').addClass('hidden');
            }
          }
          else if (res.status == "error"){
            swal({
              title: "Error",
              text : res.message,
              type : "error",
            });
          }
          $('#delivery_location').val(res.delivery);
          updatePricingSummary(res.calculated);
        }
    });
  })

  $('.billing-selections .selection-card').on('click', function(){
    var card = $(this);

    $(this).addClass(function() {
      if($(this).hasClass("selected")) return "";
      return "selected";
    });
    $(this).siblings(".selection-card").removeClass("selected");
    $('#billing_address_id').val($(this).data('option'));

    if (card.data('option') == "-1"){
      $('#billing-address-container').removeClass('hidden');
      $('#name_billing').val('');
      $('#phone_billing').val('');
      $('#companyname_billing').val('');
      $('#address1_billing').val('');
      $('#address2_billing').val('');
      $('#country_billing').val('');
      $('#state_billing').val('');
      $('#city_billing').val('');
      $('#postcode_billing').val('');
    }
    else {
      $('#billing-address-container').addClass('hidden');
    }
  })

  $(".country-select").change(function() {
      var stateSelect = $(this).parents('.address-container').find('.state-select');
      stateSelect.empty();

      var country    = $(this).val();
      var isShipping = true;

      if ($(this).attr('name') == 'country_billing'){
        isShipping = false;
      }

      $.request('onCountryChange', {
          data   : {
            country   : country,
            isShipping: isShipping,
          },
          success: function(response){
            console.log(response.states);
            var el  = "<option value=''>";
                el += response.default;
                el += "</option>";

            stateSelect.append(el);

            if (response.states){
              $.each(response.states, function(index, state){
                el  = "<option value='" + state.id + "'>";
                el += state.name;
                el += "</option>";

                stateSelect.append(el);
              });
            }

            if (isShipping){
              $('#delivery_location').val(null);
              updatePricingSummary(response.calculated);
            }
          }
      });
  });

  $(".state-select").change(function() {
      var country = $(this).parents('.address-container').find('.country-select').val();
      var state   = $(this).val();

      var isShipping = true;

      if ($(this).attr('name') == 'state_billing'){
        isShipping = false;
      }

      $.request('onStateChange', {
          data   : {
            country   : country,
            state     : state,
            isShipping: isShipping,
          },
          success: function(response){
            if (isShipping){
              $('#delivery_location').val(response.delivery);
              updatePricingSummary(response.calculated);
            }
          }
      });
  });

  function updatePricingSummary(data){
    console.log(data);
    $('#total-weight').html(data.delivery_charge.total_weight.toFixed(3));
    $('#discount').html('- ' + data.currency.unit + data.discount.formatted);
    if (data.delivery_charge.total == 0){
      if (data.delivery_charge.free) {
        $("#shipping_price").html(data.currency.unit + "0.00");
      } else {
        $("#shipping_price").html('<strong style="font-size: 14px; line-height: 1; text-transform: none;">Enter your shipping address<br>to view the charges</strong>');
      }
    } else {
      $("#shipping_price").html(data.currency.unit + data.delivery_charge.formatted);
    }
    $("#tax_amount").html(data.tax.formatted);
    $("#total_amount").html(data.grand.formatted);
    $("#subtotal").html(data.subtotal.formatted);
  }

  $('#guest_name').on('click change input paste keyup keydown', function(){
    $('input[name="UserName"]').val($('#guest_name').val());
  });
  $('#guest_email').on('click change input paste keyup keydown', function(){
    $('input[name="UserEmail"]').val($(this).val());
  });
  $('#guest_telephone').on('click change input paste keyup keydown', function(){
    $('input[name="UserContact"]').val($(this).val());
  });

  if($('input[name="UserName"]').val() == "" 
  && $('input[name="UserEmail"]').val() == "" 
  && $('input[name="UserContact"]').val() == ""){
    $('#guest_name, #guest_email, #guest_telephone').click();
  }

  function updateCartItems(order, itemCount){
    $('.shopping_cart .cart-count').html(itemCount);
    console.log(order);

    if (order['orderitems'].length > 0){
      var orderitems = order['orderitems'];
      var compiled   = '<table class="table table-striped cart-lists"><tbody>';

      for (var i = 0; i < orderitems.length; i++) { 
        var totalPrice = (parseFloat(orderitems[i]['unit_price']) * parseInt(orderitems[i]['quantity'])).toFixed(2);

        // var stock      = orderitems[i]['stock'];
        var product = orderitems[i]['product'];
        // if (product['product_type'] == '')

        compiled += '<tr data-hash="' + orderitems[i]['hexhash'] + '">';
        compiled += '<td class="text-left">';
        compiled += '<a class="cart_product_name" href="/product/' + product['slug'] + '">' + product['title'];
        compiled += '</a>';
        compiled += '</td>';
        compiled += '<td class="text-center"> x<span class="orderitem-quantity">' + orderitems[i]['quantity'] + '</span> </td>';
        compiled += '<td class="text-center"> <span class="orderitem-amount">' + orderitems[i]['currency']['unit'] + totalPrice + '</span> </td>';
        compiled += '<td class="text-right">';
        compiled += '  <a href="#" class="btn btn-default btn-remove" data-hash="' + orderitems[i]['hexhash'] + '"><i class="fa fa-times"></i></a>';
        compiled += '</td>';
        compiled += '</tr>';

      }

      compiled += '</tbody></table>';

      $('.cart-lists').html(compiled);
      // addRemoveEvent();
    }
    else{
      $('.cart-lists').html('<p>No item in your cart yet. Add something to your cart now!</p>');
    }


    $('tr[data-hash="' +  + '"]')
  }

  function removeCartItem(deletedHash){
    $('tr[data-hash="' + deletedHash + '"]').remove();
  }
});