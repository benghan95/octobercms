$(document).ready(function() {
  $(".country-select").change(function() {
    var stateSelect = $(this).parents('form').find('.state-select');
    stateSelect.empty();

    var country    = $(this).val();
    var stateId = $('input[name="temp_state"]').val();

    $.request('onCountryChange', {
      data   : {
        country   : country,
      },
      success: function(response){
        console.log(response.states);
        var el  = "<option value=''>";
            el += response.default;
            el += "</option>";

        stateSelect.append(el);

        if (response.states){
          $.each(response.states, function(index, state){
            if (state.id == stateId)
              el  = "<option value='" + state.id + "' selected>";
            else
              el  = "<option value='" + state.id + "'>";
            el += state.name;
            el += "</option>";

            stateSelect.append(el);
          });
        }
      }
    });
  });

  $('.country-select').trigger('change');

  $('form').on('ajaxSuccess', function(event, context, data) {
    if (context.handler == 'onUpdateAddress'){
      if(data.status == "success"){
        swal({
          title: "Success!",
          text: "Your address has been updated!",
          type: "success",
        });
      }
    }
  });
})