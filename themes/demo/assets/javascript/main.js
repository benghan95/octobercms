$(document).ready(function(){
  $('.marquee').marquee({
    duration: 10000,
    // gap: 50,
    delayBeforeStart: 0,
    direction       : 'left',
    // duplicated: true
    pauseOnHover: true,
  });

  $('.countdown_timer').each(function() {
    var endDate = $(this).data('sale-end');
    var date = moment(endDate);
    $(this).countdown(date.toDate(), function(event) {
      var $this = $(this).html(event.strftime(''
         + '<div class="time-item time-day"><div class="num-time">%D</div><div class="name-time">Day </div></div>'
         + '<div class="time-item time-hour"><div class="num-time">%H</div><div class="name-time">Hour </div></div>'
         + '<div class="time-item time-min"><div class="num-time">%M</div><div class="name-time">Min </div></div>'
         + '<div class="time-item time-sec"><div class="num-time">%S</div><div class="name-time">Sec </div></div>'));
    });
  })

  $(window).scroll(function () {
      // set distance user needs to scroll before we start fadeIn
    if ($(this).scrollTop() > 225) {
      $('#header').addClass('fixed-top');
      // $('#header .header-bottom').slideDown();
    } else {
      $('#header').removeClass('fixed-top');
      // $('#header').slideUp();
    }
  });

  $(".products-slider").owlCarousel({
    navText   : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 2,
      },
      767: {
        items: 3,
      },
      991: {
        items: 4,
      },
      1199: {
        items: 5,
      },
    }
  });

  function initSlick(elem){
    elem.css({
      "height"    : "auto",
      "overflow"  : "visible",
      "visibility": "visible",
      "opacity"   : "1"
    });
    elem.slick({
      prevArrow     : elem.parents('.product-slider').find('.prev'),
      nextArrow     : elem.parents('.product-slider').find('.next'),
      infinite      : false,
      speed         : 1000,
      slidesToShow  : 4,
      slidesToScroll: 4,
      responsive    : [
        {
          breakpoint: 1200,
          settings  : {
            slidesToShow  : 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 767,
          settings  : {
            slidesToShow  : 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings  : {
            slidesToShow  : 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }

  $('.product-slider').each(function(){
    initSlick($(this).find('.slider'));
  })

  $('.btn-add-to-wishlist').each(function(){
    $(this).on('click', function(e){
      e.preventDefault();

      var options = $(this).siblings('input[name="options[]"]').map(function(){ 
          return this.value;
      }).get();

      $('form').request('onAddToWishlist', {
        data: {
          productId: $(this).siblings('input[name="product-hash"]').val(),
          options  : options,
        },
        success: function(res) {
          if (res.status == 'user-404') {
            swal({
              title: "Warning!",
              text : "Please login to add item into wishlist.",
              type : "warning",
              showCancelButton: true,
              confirmButtonText: "Login Now",
              closeOnConfirm: false
            },
            function (isConfirm) {
              if (isConfirm) {
                window.location.href = 'https://igpremium.com.my/auth/login';
              }
            }
            );
          } else {
            swal({
              title: "Success!",
              text : "Item has been added to your wishlist.",
              type : "success",
            });
          }
        },
      });
    });
  });

  $('.currencies-block .dropdown-menu a').each(function(){
    $(this).on('click', function(e){
      e.preventDefault();

      $('form').request('onChangeCurrency', {
        data: {
          currency: $(this).attr('data-currency')
        },
        success: function(res) {
          location.reload();
        },
      });
    });
  });
});