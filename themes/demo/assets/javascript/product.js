$(document).ready(function(){
  // $("#thumb-slider .owl-nav .owl-prev").html('<i class="fa fa-angle-left"></i>');
  // $("#thumb-slider .owl-nav .owl-next").html('<i class="fa fa-angle-right"></i>');

  // var zoomCollection = '.large-image img';
  // $( zoomCollection ).elevateZoom({
  //   zoomType          : "inner",
  //   lensSize          : "10",
  //   easing            : true,
  //   gallery           : 'thumb-slider',
  //   cursor            : 'pointer',
  //   loadingIcon       : '../html/image/theme/lazy-loader.gif',
  //   galleryActiveClass: "active"
  // });

  var images = [];

  $('#thumb-slider .img img').each(function(){
    if($(this).attr('src')){
      images.push({
        src: $(this).attr('src')
      })
    }
  });

  var zoomCollection = '.large-image img';
  $( zoomCollection ).elevateZoom({
    zoomType          : "inner",
    lensSize          : "50",
    easing            : true,
    gallery           : 'thumb-slider-vertical',
    cursor            : 'pointer',
    galleryActiveClass: "active"
  });
  // $('.large-image').magnificPopup({
  //   items    : images,
  //   gallery  : { enabled: true, preload: [0,2] },
  //   type     : 'image',
  //   mainClass: 'mfp-fade',
  //   callbacks: {
  //     open: function() {
  //       var activeIndex   = parseInt($('#thumb-slider-vertical .img.active').attr('data-index'));
  //       var magnificPopup = $.magnificPopup.instance;
  //       magnificPopup.goTo(activeIndex);
  //     }
  //   }
  // });

  $("#thumb-slider-vertical .owl2-item").each(function() {
    $(this).find("[data-index='0']").addClass('active');
  });

  $(".thumb-vertical-outer .next-thumb").click(function () {
    $( ".thumb-vertical-outer .lSNext" ).trigger( "click" );
  });

  $(".thumb-vertical-outer .prev-thumb").click(function () {
    $( ".thumb-vertical-outer .lSPrev" ).trigger( "click" );
  });

  $(".thumb-vertical-outer .thumb-vertical").lightSlider({
    item          : 3,
    autoWidth     : false,
    vertical      : true,
    verticalHeight: '450',
    slideMargin   : 5,
    pager         : false,
    controls      : true,
    prevHtml      : '<i class="fa fa-angle-up"></i>',
    nextHtml      : '<i class="fa fa-angle-down"></i>',
    responsive    : [
      {
        breakpoint: 1199,
        settings  : {
          // verticalHeight: 330,
          // item: 3,
        }
      },
      {
        breakpoint: 1024,
        settings  : {
          // verticalHeight: 235,
          // item: 2,
          // slideMargin   : 5,
        }
      },
      {
        breakpoint: 768,
        settings  : {
          // verticalHeight: 340,
          // item: 3,
        }
      }
      ,
      {
        breakpoint: 480,
        settings  : {
          // verticalHeight: 100,
          // item: 1,
        }
      }

    ]
  });
  // $("#thumb-slider .owl2-item").each(function() {
  //   $(this).find("[data-index='0']").addClass('active');
  // });

  $('#product .option-selections .selection-card').on('click', function(){
    $(this).addClass(function() {
      if($(this).hasClass("selected")) return "";
      return "selected";
    });
    $(this).siblings(".selection-card").removeClass("selected");
    $(this).siblings(".selection-card").find('.color-selections .radio').removeClass("active");
    $(this).siblings(".selection-card").find('.selection-price h3').html('');
    $(this).find('.product-options li.radio:first-child').addClass('active');
    $(this).find('.product-options li.radio.active').trigger('click');

    var stockId = $(this).find('.product-options .radio.active .icon').data('stock-id');
    if (stockId != null)
      $(this).attr('data-stock', stockId);

    $('input[name="options[]"]').val($(this).attr('data-hash'));
    $('input[name="stocks[]"]').val($(this).attr('data-stock'));
  });

  $('.product-options li.radio').click(function(e){
    e.stopImmediatePropagation();
    $(this).addClass(function() {
      if($(this).hasClass("active")) return "";
      return "active";
    });

    $(this).siblings("li").removeClass("active");

    var icon        = $(this).find('.icon-color');
    var colorCode   = icon.data('color-title');
    var title       = icon.data('original-title');
    var price       = icon.data('price');
    var stock       = icon.data('stock-count');
    var optionId    = icon.data('option-id');
    var stockId     = icon.data('stock-id');
    var optionTitle = icon.data('option-title');

    $(this).parents('.option-card').attr('data-option', optionId);

    if (colorCode == '#ffffff' || colorCode == '#ffff00'){
      icon.parents('.product-options').addClass('is-white');
    }
    else{
      icon.parents('.product-options').removeClass('is-white');
    }

    if (price != "out-of-stock" || stock > 0){
      if ($('#product-type').val() == 'SET'){
        $(this).parent().find('.selected-option').html('<span class="label" style="background-color: ' + colorCode + '">'+ title +'</span>');
      }
      else{
        $(this).parent().find('.selected-option').html('<span class="label" style="background-color: ' + colorCode + '">'+ title + ' ' + price +'</span>');
      }
      $(this).parents('.selection-card').find('.selection-price h3').html(price);
    }
    else{
      $(this).parent().find('.selected-option').html('<span class="label" style="background-color: ' + colorCode + '">'+ title +'</span>');
      $(this).parents('.selection-card').find('.selection-price h3').html('Out of Stock');
    }

    if ($('#product-type').val() != 'BUNDLE'){
      $('input[name="options[]"]').val(optionId);
    } else{
      $('input[name="options[]"][data-option="' + optionTitle + '"]').val(optionId);
    }

    $( ".thumb-vertical-outer .owl2-item a[data-hash='" + optionId + "']" ).trigger( "click" );

    if (price == "out-of-stock" || stock <= 0){
      $('#button-cart').addClass('disabled');
      $('#button-cart').val('Out of Stock');
    }
    else{
      $('#button-cart').removeClass('disabled');
      $('#button-cart').val('Add to Cart');
    }
  });

  $('.product-options li.radio.active').trigger('click');

  $(".selection-card.selected").trigger('click');

  $('.releate-products').owlCarousel({
    pagination        : false,
    center            : false,
    nav               : true,
    dots              : false,
    loop              : true,
    margin            : 10,
    navText           : [ 'prev', 'next' ],
    slideBy           : 1,
    autoplay          : false,
    autoplayTimeout   : 2500,
    autoplayHoverPause: true,
    autoplaySpeed     : 800,
    startPosition     : 0,
    responsive        : {
      0:{
        items: 2
      },
      480:{
        items: 2
      },
      768:{
        items: 3
      },
      1024:{
        items: 4
      },
      1200:{
        items: 5
      }
    }
  });

  $(".releate-products .owl-nav .owl-prev").html('<i class="fa fa-chevron-left"></i>');
  $(".releate-products .owl-nav .owl-next").html('<i class="fa fa-chevron-right"></i>');
});
