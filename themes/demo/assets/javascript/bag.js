$(function(){
  function addRemoveEvent(){
    $('.shopping_cart .shoppingcart-box .btn-remove').on('click', function(e){
      e.preventDefault();
      var current = $(this);
      var hash    = current.attr('data-hash');

      $.request('onDeleteOrderItem', {
        data   : {
          hash: hash
        },
        success: function(res){
          if(res.result == "success"){
            updateCartItems(res.order, res.itemCount);
            removeCartItem(res.deletedHash);

            swal({
              title             : "Removed!",
              text              : "An item has been removed from your cart.",
              type              : "success",
              showCancelButton  : true,
              cancelButtonText  : "Continue Shopping",
              confirmButtonColor: "#f79e1d",
              confirmButtonText : "Checkout",
              closeOnConfirm    : true
            },
            function(){
              $.request('onCheckout', {
                success: function(response){
                  if(typeof response.X_OCTOBER_REDIRECT !== "undefined")
                    location.href = response.X_OCTOBER_REDIRECT;
                }
              });
            });
          }

          if(typeof res.X_OCTOBER_REDIRECT !== "undefined"){
            location.href = res.X_OCTOBER_REDIRECT;
          }
        }
      });
    });
  }

  addRemoveEvent();

  $('.btn-add-wishlist-to-cart').on('click', function(e){
    var options = [];
    $(this).parents('form').find('input[name="options[]"]').each(function(){
      options.push($(this).val());
    })

    var productId = $(this).parents('form').find('input[name="product_id"]').val();
    var quantity  = $(this).parents('form').find('input[name="quantity"]').val();

    $('form').request('onAddItem', {
      data: {
        product_id: productId,
        options   : options,
        quantity  : quantity,
      },
      success: function(data) {
        if (data.status == "failed"){
          swal({
            title           : "Error",
            text            : data.results.message,
            type            : "error",
            showCancelButton: true,
            cancelButtonText: "Close",
          });
        }
        else if (data.status == "success"){
          swal({
            title             : "Added to cart",
            text              : "An item has been added to your cart.",
            type              : "success",
            showCancelButton  : true,
            cancelButtonText  : "Continue Shopping",
            confirmButtonColor: "#f79e1d",
            confirmButtonText : "Checkout",
            closeOnConfirm    : true
          },
          function(){
            $.request('onCheckout', {
              success: function(response){
                if(typeof response.X_OCTOBER_REDIRECT !== "undefined")
                  location.href = response.X_OCTOBER_REDIRECT;
              }
            });
          });

          updateCartItems(data.results.order, data.results.itemCount);
        }
      }
    });
  });

  $('#button-cart').on('click', function(e){
    if($('#button-cart').val() == 'Out of Stock'){
      swal({
        title           : "Out of Stock",
        text            : "Item is out of stock, please try purchase other products.",
        type            : "warning",
        showCancelButton: true,
        cancelButtonText: "Close",
      });
      return;
    }

    var options = [];
    $(this).parents('form').find('input[name="options[]"]').each(function(){
      options.push($(this).val());
    })

    var productId = $('#product_id').val();
    var quantity  = $('#quantity').val();

    if(typeof productId == 'undefined' 
    || productId == null 
    || productId == ""){
      swal({
        title           : "Warning",
        text            : "Please refresh the page and try again.",
        type            : "warning",
        showCancelButton: true,
        cancelButtonText: "Close",
      });
      return;
    }

    if (options.length <= 0){
      swal({
        title           : "Warning",
        text            : "Please select an option to add to cart",
        type            : "warning",
        showCancelButton: true,
        cancelButtonText: "Close",
      });
      return;
    }

    if($('#product-type').val() == 'SET' 
    || $('#product-type').val() == 'BUNDLE' 
    || $('#product-type').val() == 'OPTION'){
      if(options.length <= 0){
        swal({
          title           : "Warning",
          text            : "Please select an option to add to cart",
          type            : "warning",
          showCancelButton: true,
          cancelButtonText: "Close",
        });
        return;
      }
    }

    if(typeof quantity == 'undefined' 
    || quantity == null
    || quantity <= 0){
      swal({
        title           : "Warning",
        text            : "Quantity must be equal or more than 1 to add to cart",
        type            : "warning",
        showCancelButton: true,
        cancelButtonText: "Close",
      });
      return;
    }

    $('form').request('onAddItem', {
      data: {
        product_id: productId,
        options   : options,
        quantity  : quantity,
      },
      success: function(data) {
        if (data.status == "failed"){
          swal({
            title           : "Error",
            text            : data.results.message,
            type            : "error",
            showCancelButton: true,
            cancelButtonText: "Close",
          });
        }
        else if (data.status == "success"){
          swal({
            title             : "Added to cart",
            text              : "An item has been added to your cart.",
            type              : "success",
            showCancelButton  : true,
            cancelButtonText  : "Continue Shopping",
            confirmButtonColor: "#f79e1d",
            confirmButtonText : "Checkout",
            closeOnConfirm    : true
          },
          function(){
            $.request('onCheckout', {
              success: function(response){
                if(typeof response.X_OCTOBER_REDIRECT !== "undefined")
                  location.href = response.X_OCTOBER_REDIRECT;
              }
            });
          });

          updateCartItems(data.results.order, data.results.itemCount);
        }
      }
    });
  });

  $('form').on('ajaxSuccess', function(event, context, res) {
    if (context.handler == 'onAddItem'){
      if(res.status == "success"){
        updateCartItems(res.order, res.itemCount);

        swal({
          title             : "Success!",
          text              : "An item has been added into your cart.",
          type              : "success",
          showCancelButton  : true,
          cancelButtonText  : "Continue Shopping",
          confirmButtonColor: "#f79e1d",
          confirmButtonText : "Checkout",
          closeOnConfirm    : true
        },
        function(){
          $.request('onCheckout', {
            success: function(response){
              if(typeof response.X_OCTOBER_REDIRECT !== "undefined")
                location.href = response.X_OCTOBER_REDIRECT;
            }
          });
        });
      } else if (res.status == "failed"){
        swal({
          title: "Failed!",
          text : res.error_message,
          type : "error",
        });
      }
    }
  });

  function updateCartItems(order, itemCount){
    // $('.shopping_cart .cart-count').html(itemCount);

    $.post('/cart-list', function (response) {
      var $result = $(response);
      $('.shopping_cart .cart-total-full .cart-count').text($result.find('#cart-list').data('count'));
      $('.shopping_cart .shoppingcart-box .cart-lists').empty().html($result.find('#cart-list').html());
      addRemoveEvent();
    });

  }

  function removeCartItem(deletedHash){
    $('tr[data-hash="' + deletedHash + '"]').remove();
  }
});