var inputResizeFactor = null;
function resizable(el, factor) {
  var int = Number(factor) || 7.7;

  function resize() {
    el.style.width = ((el.value.length + 1) * int + 1) + 'px'
  }
  var e = 'keyup,keypress,focus,blur,change'.split(',');
  for (var i in e) el.addEventListener(e[i], resize, false);
  resize();
}
resizable(document.getElementById('price_low'), inputResizeFactor);
resizable(document.getElementById('price_high'), inputResizeFactor);

function PriceSlider(value) {
  this.value = value;

  console.log(this.value);

  $(".price-slider").each(function () {
    $(this).bootstrapSlider('setValue', this.value);
  });
}

$(document).ready(function () {
  $('#category_banner').carousel({
    interval: 3000,
    keyboard: false,
  });

  $('#product-filter .keyword-input').on('change', function(){
    $(document).trigger('onProductListFiltered');
  });

  $('.btn-clear-all').click(function(e){
    e.preventDefault();

    $('#product-filter input[type="checkbox"]:checked').prop('checked', null);
    $('#product-filter .keyword-input').val(null);

    var defaultValue = JSON.parse($('#price_slider').attr('data-slider-value'));

    $('#price_slider').bootstrapSlider('setValue', defaultValue);
    $('#price_slider_modal').bootstrapSlider('setValue', defaultValue);

    $('.price-low').each(function(){
      $('.price-low').val(0);
    })

    $('.price-high').each(function(){
      $('.price-high').val(defaultValue[1]);
    })

    $(document).trigger('onProductListFiltered');
  });

  $(".price-slider").each(function () {
    $(this).bootstrapSlider({});
  });

  var triggerProductListingFilter = debounce(function () {
    $(document).trigger('onProductListFiltered');
  }, 350);

  $(".price-slider").on("change", function (slideEvt) {
    $(".price-slider").each(function () {
      $(this).bootstrapSlider('setValue', slideEvt.value['newValue']);
    });

    $(".price-low").each(function () {
      $(this).val(slideEvt.value['newValue'][0]);
    });
    $(".price-high").each(function () {
      $(this).val(slideEvt.value['newValue'][1]);
    });

    resizable(document.getElementById('price_low'), inputResizeFactor);
    resizable(document.getElementById('price_high'), inputResizeFactor);

    triggerProductListingFilter();
  });

  $(".price-low, .price-high").each(function () {
    $(this).on('keyup keypress change', function () {
      var lowPrice  = parseInt($('.price-low').val());
      var highPrice = parseInt($('.price-high').val());
      var newValue  = [lowPrice, highPrice];

      $('#price_slider').bootstrapSlider('setValue', newValue);
      $('#price_slider_modal').bootstrapSlider('setValue', newValue);

      triggerProductListingFilter();
    })
  });

  $('.checkbox label').each(function () {
    $(this).on('click', function (e) {
      if ($(e.target).attr('type') != "checkbox") {
        e.preventDefault();
        e.stopImmediatePropagation();
        $(this).find('input[type="checkbox"]').prop("checked", !$(this).find('input[type="checkbox"]').prop("checked"));
      }

      $(document).trigger('onProductListFiltered');
    });
  });

  $('.btn-more-categories').on('click', function (e) {
    e.preventDefault();

    if ($('.btn-more-categories').hasClass('open')) {
      $('.extra-hidden').addClass('hide');
      $('.btn-more-categories').removeClass('open');
      $('.btn-more-categories').html('More categories');
    } else {
      $('.extra-hidden').removeClass('hide');
      $('.btn-more-categories').addClass('open');
      $('.btn-more-categories').html('Hide categories');
    }
  })
});

