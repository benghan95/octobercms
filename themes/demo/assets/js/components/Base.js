define(["declare",
        "widget",
        "templatemixin",
        Fac.temp('components/Base')],
function(declare, _Widget, TemplatedMixin, template) {
    return declare("scripts.components.Base", [_Widget, TemplatedMixin], {
        templateString: template,
        destroy: function() {
        	var domNode = this.domNode;
            var context = this;

            if(domNode)
                topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));

            context.inherited(arguments);
        },suicide: function(){
            var context = this;
            context.destroy();
        },startListening: function(){
            var context = this;
            var domNode = this.domNode;
            var id = domAttr.get(domNode, "id");

            var suicideHandle = topic.subscribe("suicide."+id, function(){
                if(domNode)
                    context.suicide();
            });
            topic.publish("topic.handlers.add", suicideHandle, domAttr.get(domNode, "id"));
        },xhr: function(setting, callback, fail, always){
            //Format expected for -setting-
            /*
                {
                  url: api_url,
                  type: "GET",
                  cache: false,
                  dataType: "json"
                }
            */

            var request = $.ajax(setting);

            request.done(function( response ) {
                if(callback)
                {
                    callback(response);
                }else{
                    console.warn("No callback for XHR in scripts.compoents.Base");
                }
            });
             
            request.fail(function( jqXHR, textStatus ) {
                if(fail)
                    fail(jqXHR, textStatus);
            });
            
            request.always(function() {
                if(always)
                    always();
            });

            return request;
        }, regDeath: function(handler){
            var context = this;
            var domNode = this.domNode;

            topic.publish("topic.handlers.add", handler, domAttr.get(domNode, "id"));
        }
    });

});