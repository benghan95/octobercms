define(["declare",
        "widget",
        "templatemixin",
        Fac.path('Basepro')
    ],
    function(declare, _Widget, TemplatedMixin, basepro) {
        return declare("js.components.BaseModal", [basepro, TemplatedMixin], {
            templateString: null,
            construct: function(setting) {
                var context = this;
                var domNode = this.domNode;

            }, startup: function() {
                var domNode = this.domNode;
                var context = this;
            }, initModal:function(){
                var domNode = this.domNode;
                var context = this;

                $(domNode).modal({
                    backdrop : true,
                    show: true,
                    keyboard: false
                });

                $(".modal").on('hidden.bs.modal', function () {
                    var body = win.body();

                    if(domClass.contains(body, "modal-open")){
                        domClass.remove(body, "modal-open");
                    }

                    if(domClass.contains(body, "page-overflow")){
                        domClass.remove(body, "page-overflow");
                    }

                    topic.publish("nuke.this", domNode);
                    context.destroy();
                });

                var promodalHideHandle = topic.subscribe("promodal.animate.hide", function(){
                    $(".modal-backdrop").remove();
                    topic.publish("nuke.this", domNode);
                    context.destroy();
                });
                topic.publish("topic.handlers.add", promodalHideHandle, domAttr.get(domNode, "id"));
            }
        });

    });