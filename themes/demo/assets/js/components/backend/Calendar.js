define(["declare",
        "widget",
        "templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/backend/Calendar')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("js.components.backend.Calendar", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
                var context = this;
                var domNode = this.domNode;

                context.initInteraction(setting);
                context.startListening();
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;
            }, initInteraction:function(setting){
                var context = this;
                var domNode = this.domNode;

                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);


                var dataDate = nowTemp.getFullYear()+"-"+(nowTemp.getMonth()+ 1)+"-"+nowTemp.getDate();

                // console.log("now", now);
                // console.log("nowTemp.getMonth()", nowTemp.getMonth());

                domAttr.set(context.fromDateNode, "data-date", dataDate);

                $(context.fromDateNode).val(dataDate);
                $(context.toDateNode).val(dataDate);

                if(setting.df != "")
                    $(context.fromDateNode).val(setting.df);

                if(setting.dt != "")
                    $(context.toDateNode).val(setting.dt);

                var from = $(context.fromDateNode).datepicker({
                    format: 'yyyy-mm-dd',
                    onClose: function( selectedDate ) {
                        $(context.toDateNode).datepicker( "option", "minDate", selectedDate );
                    }
                });
                var to = $(context.toDateNode).datepicker({
                    format: 'yyyy-mm-dd',
                    onClose: function( selectedDate ) {
                        $(context.fromDateNode).datepicker( "option", "maxDate", selectedDate );
                    }
                });


                /**
                 *
                 * Setup Time Picker
                 *
                 */
                $(context.fromTimeNode).val(setting.tf);
                $(context.fromTimeNode).clockpicker({
                    placement: 'bottom',
                    align: 'left',
                    donetext: 'Done'
                });

                $(context.toTimeNode).val(setting.tt);
                $(context.toTimeNode).clockpicker({
                    placement: 'bottom',
                    align: 'left',
                    donetext: 'Done'
                });

                $(context.filterNode).click(function(){
                    window.open(setting.controllerURL+"?df=" +from.val()+"&dt="+to.val()+"&tf="+$(context.fromTimeNode).val()+"&tt="+$(context.toTimeNode).val(), "_self");
                });
            }, startListening: function(){
                var context = this;
                var domNode = this.domNode;

            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;
            }
        });

    });