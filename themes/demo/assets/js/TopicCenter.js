require([
	"dojo/ready",
	"dojo/topic",
	"dojo/_base/array",
	"dojo/_base/config"
	],

	function(ready, topic, array, config){

		var masterTopicHandlers = new Object();

		ready(function()
		{
			startListening();
			console.log("[Topic Center Running]");
		});

		function startListening(){
			topic.subscribe("topic.handlers.add", function(_topichandler, _identifier){
				if(typeof masterTopicHandlers[_identifier] === "undefined"){
					var topicHandlerArray = new Array();
					topicHandlerArray.push(_topichandler);

					masterTopicHandlers[_identifier] = topicHandlerArray;
					return;
				}

				var existingTopicHandlerArray = masterTopicHandlers[_identifier];
				existingTopicHandlerArray.push(_topichandler);

				masterTopicHandlers[_identifier] = existingTopicHandlerArray;
			});

			topic.subscribe("topic.handlers.destroy", function(_identifier){
				//console.log("GB Topic Hanlders", _identifier);
				if(typeof masterTopicHandlers[_identifier] !== "undefined"){
					var topicHandlerArray = masterTopicHandlers[_identifier];
					array.forEach(topicHandlerArray, function(singleTopic, index){
						try{
							if(singleTopic != null){
								singleTopic.remove();
							}
						}catch(err){
							console.warn("[Topic Center GB]", err);
						}
					});

					delete masterTopicHandlers[_identifier];
				}
			});
		}
});
