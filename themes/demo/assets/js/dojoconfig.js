var _baseURL = "//boozeat.lf";
var _assetUR = _baseURL + '/themes/demo/assets';

switch(location.hostname){
    case "103.233.1.74":
        _baseURL = "//103.233.1.74/~viennabooz/";
        _assetUR = _baseURL + '/themes/demo/assets';
        break;
	default:
		_baseURL = "//"+location.hostname;
		_assetUR = _baseURL + '/themes/demo/assets';
		break;
	break;
}

dojoConfig = {
	has : {"dojo-firebug":true, "dojo-debug-messages":true },
	parseOnLoad : false,
	async : true,
	packages : [{name:"Beacon", location:"Scripts"}],
	aliases : [
		["ready", "dojo/ready"],
		["declare", "dojo/_base/declare"],
		["widget", "dijit/_Widget"],
		["templatemixin", "dijit/_TemplatedMixin"],
		["topic", "dojo/topic"],
		["Factory", _assetUR + "/js/api/Factory.js"],
		["Element", _assetUR + "/js/api/Element.js"]
	],
	isExperimental: true,
	baseURL: _baseURL,
	imgURL: _assetUR + "/img/",
	baseJSURL: _assetUR + "/js/",
	baseTmptURL: _assetUR + "/template/"
};


String.prototype.insertAt = function(index, string) {
	return this.substr(0, index) + string + this.substr(index);
}

var html, domConstruct, topic, win, windows, on, dom, domStyle, query, domAttr, domClass, array, config, timer, Fac , Elem= null;

$(function($) {
});