define(["declare",
 		"widget"], 
        function(declare,_Widget) {

	var module =  declare("scripts.api.Element", [], {});

	/*
		Util function to inject select options
	*/
	module.select = function(key, value, name, selected){
		var _el= "";

		//<option value="">Salutation</option>
		_el += "<option ";
		_el += "name='"+name+"'";
		_el += " value='";
		_el += key;

		if(selected){
            _el += "'";
			_el += " selected='selected' >";
		}else{
            _el += "'>";
		}

		_el += value;
		_el += "</option>";

		return _el;
	};

	module.groupselect = function(labelName, options){
		var _el = "";

        _el += "<optgroup label='";
        _el += labelName;
        _el += "'>";
        _el += options;
        _el += "</optgroup>";

		return _el;
	}

	/*
		Util function to inject img!
	*/
	module.img = function(src, _class){
		var _el = "";

		el += "<img class='";
		el += _class;
		el += "'";
		el += "src='";
		el += src;
		el += "' />";

		return el;
	};

	return module;
});